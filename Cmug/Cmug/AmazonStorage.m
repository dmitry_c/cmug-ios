//
//  AmazonStorage.m
//  Crowd-iOS
//
//  Created by Skeeet on 3/31/11.
//  Copyright 2011 Munky Interactive / munkyinteractive.com. All rights reserved.
//	File created using Singleton XCode Template by Mugunth Kumar (http://mugunthkumar.com
//  Permission granted to do anything, commercial/non-commercial with this file apart from removing the line/URL above

#import "AmazonStorage.h"
#import "FileCache.h"

static AmazonStorage *_instance;
@implementation AmazonStorage

@synthesize uploadQueue;
@synthesize downloadQueue;
@synthesize uploadObjects;
@synthesize downloadObjects;
@synthesize delegate;
@synthesize requestCompletedSelector;
@synthesize downloadsInProgressRequests;
@synthesize uploadProgressSelector;
@synthesize requestFailedSelector;


+ (AmazonStorage*)sharedStorage
{
	@synchronized(self) 
	{	
        if (_instance == nil) {
			
            _instance = [[super allocWithZone:NULL] init];
        }
    }
    return _instance;
}


#pragma mark Singleton Methods

+ (id)allocWithZone:(NSZone *)zone
{	
	return [[self sharedStorage]retain];	
}


- (id)copyWithZone:(NSZone *)zone
{
    return self;	
}

- (id)retain
{	
    return self;	
}

- (unsigned)retainCount
{
    return NSUIntegerMax;  //denotes an object that cannot be released
}

- (void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;	
}
- (void) dealloc
{
	self.uploadQueue = nil;
	self.downloadQueue = nil;
	self.uploadObjects = nil;
	self.downloadObjects = nil;
	self.downloadsInProgressRequests = NULL;
	self.uploadProgressSelector = NULL;
	[super dealloc];
}

#pragma mark init
- (id) init
{
	self = [super init];
	if (self != nil) 
	{
		self.uploadQueue = [ASINetworkQueue queue];
		self.uploadQueue.showAccurateProgress  = YES;
		self.downloadQueue = [ASINetworkQueue queue];
		self.uploadObjects = [NSMutableDictionary dictionary];
		self.downloadObjects = [NSMutableDictionary dictionary];
		
		self.downloadsInProgressRequests = [NSMutableArray array];
		
		//[ASIS3Request setSharedAccessKey:ACCESSKEY];
		//[ASIS3Request setSharedSecretAccessKey:SECRETKEY];
		
		
	}
	return self;
}

#pragma mark -

-(ASIS3ObjectRequest*) beginUpload:(AmazonObject*) object progressIndicator:(id)progressIndicator mediaSettings:(CRMediaSettings*)mediaSettings
{	
	CRLog(@"Uploading to S3 : %@",object);
	
	[ASIS3Request setSharedAccessKey:mediaSettings.aws_accesskey];
	[ASIS3Request setSharedSecretAccessKey:mediaSettings.aws_secretkey];
	
	// calculate size
	NSFileManager* fileManager = [NSFileManager defaultManager];
	NSError* error;
	NSDictionary* attrs = [fileManager attributesOfItemAtPath:object.fileLocation error:&error];
	if(!error && !![attrs objectForKey:NSFileSize])
		object.size = -1;
	else
		object.size = [(NSNumber*)[attrs objectForKey:NSFileSize] intValue];
	
	//upload main object
	ASIS3ObjectRequest *request = [ASIS3ObjectRequest PUTRequestForFile:object.fileLocation
															 withBucket:mediaSettings.aws_sourcebucket
																	key:object.urlLocation];
	request.shouldCompressRequestBody = NO;
	request.shouldContinueWhenAppEntersBackground = YES;
	request.delegate = self;
	request.uploadProgressDelegate = progressIndicator;
	request.didFinishSelector = @selector(requestDone:);
	request.didFailSelector = @selector(requestFailed:);
	request.showAccurateProgress = NO;
	request.storageClass = ASIS3StorageClassReducedRedundancy;
	request.requestScheme = ASIS3RequestSchemeHTTPS;
	request.timeOutSeconds = 300;
	request.shouldStreamPostDataFromDisk = YES;
	[request setValidatesSecureCertificate:NO];
	object.status = kStatusUploading;
	object.request = request;
	[self.uploadQueue addOperation:request];
	
	[self.uploadObjects setObject:object forKey:object.urlLocation];
	CRLog(@"set obj for path: %@", object.urlLocation);
	[self.uploadQueue go];
	
	return request;
}

-(void) beginnDownloadFromCDN:(NSString*)object
{
	if([self.downloadsInProgressRequests containsObject:[self makeCDNUrl:object]]) return;
	[self.downloadsInProgressRequests addObject:[self makeCDNUrl:object]];
	
	CRLog(@"%@",[self makeCDNUrl:object]);
	
	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[self makeCDNUrl:object]];

	request.shouldCompressRequestBody = YES;
	request.shouldContinueWhenAppEntersBackground = YES;
	request.delegate = self;
	
	
	NSString * pathInCache = [[[FileCache sharedCache] cacheDirectory] stringByAppendingPathComponent:object];
	request.downloadDestinationPath = pathInCache;
	//CRLog(@"%@",pathInCache);
	
	request.didFinishSelector = @selector(requestDone:);
	request.didFailSelector = @selector(requestFailed:);
	
	[self.uploadQueue addOperation:request];
	[self.uploadQueue go];
}

-(ASIHTTPRequest*) beginnDownloadFromCDNWithLink:(NSString*)object delegate:(id)requestDelegate onSuccess:(SEL)onSuccessSelector onFail:(SEL)onFailSelector 
{
	NSURL * objectUrl = [NSURL URLWithString:object];
	
	if([self.downloadsInProgressRequests containsObject:objectUrl]) return nil;
	[self.downloadsInProgressRequests addObject:objectUrl];
	
	CRLog(@"downloading %@",objectUrl);
	
	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:objectUrl];
	
	request.shouldCompressRequestBody = NO;
	request.shouldContinueWhenAppEntersBackground = YES;
	request.delegate = requestDelegate;
	
	
	NSString * pathInCache = [[[FileCache sharedCache] cacheDirectory] stringByAppendingPathComponent:[object lastPathComponent]];
	request.downloadDestinationPath = pathInCache;
	CRLog(@"%@",pathInCache);
	
	request.didFinishSelector = onSuccessSelector;
	request.didFailSelector = onFailSelector;
	
	[self.uploadQueue addOperation:request];
	[self.uploadQueue go];
	return request;
}

-(NSURL*)makeCDNUrl:(NSString*)filepath
{
	if([[filepath pathExtension] isEqualToString:@"m3u8"])
	{
		return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",VIDEOCDNSERVER,filepath]];
	}
	else 
	{
		return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PHOTOCDNSERVER,filepath]];
	}
}

- (void)requestDone:(ASIS3Request *)request
{
	[self.downloadsInProgressRequests removeObject:[request url]];
	//todo use not absolute url here
	AmazonObject * object = [[self.uploadObjects objectForKey:[request.url path]] retain];
	[self.uploadObjects removeObjectForKey:[request.url path]];
	
	if (object) object.status = kStatusDone;
	else 
	{
		object = [self.downloadObjects objectForKey:[request.url path]];
		if (object) object.status = kStatusDone;
	}	
	
	
	if(self.delegate && [self.delegate respondsToSelector:requestCompletedSelector])
	{
		[self.delegate performSelector:requestCompletedSelector];
	}
	
	[object release];
}

- (void)requestFailed:(ASIS3Request *)request
{
	[self.downloadsInProgressRequests removeObject:[request url]];
	AmazonObject * object = [[self.uploadObjects objectForKey:[request.url path]] retain];
	[self.uploadObjects removeObjectForKey:[request.url path]];
	
	CRLog(@"get obj for path: %@", [request.url path]);
	
	if (object) object.status = kStatusInvalid;
	else 
	{
		object = [self.downloadObjects objectForKey:[request.url path]];
		if (object) object.status = kStatusInvalid;
	}	
	
	if(self.delegate && [self.delegate respondsToSelector:requestFailedSelector])
	{
		[self.delegate performSelector:requestFailedSelector withObject:object];
	}
	
	CRLog(@"Dammit, something went wrong: %@, %@",[[request error] localizedDescription],[request url]);
	CRLog(@"%@", [request error]);
	
	[object release];
}



@end







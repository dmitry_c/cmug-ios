//
//  AppDelegate.h
//  Cmug
//
//  Created by Dmitry C on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiClient.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

+ (AppDelegate*) sharedApplication;

@property (strong, nonatomic) IBOutlet UIWindow *window;

@property (strong, nonatomic) ApiClient* apiClient;
@property (strong, nonatomic) ApiObjectManager* apiObjectManager;

@end

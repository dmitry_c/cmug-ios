//
//  CustomSHKConfigurator.m
//  Cmug
//
//  Created by Dmitry C on 5/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomSHKConfigurator.h"
#import "Settings.h"

@implementation CustomSHKConfigurator

- (NSString*)facebookAppId {
	return FACEBOOK_APP_ID;
}

- (NSString*)facebookLocalAppId {
	return @"";
}

- (NSArray*)facebookListOfPermissions {  
    return [NSArray arrayWithObjects:
            @"email", 
            @"user_about_me", 
            @"friends_about_me",
            @"publish_stream", 
            @"offline_access", 
            nil];
}

- (NSString*)twitterConsumerKey {
	return TWITTER_kOAuthConsumerKey;
}

- (NSString*)twitterSecret {
	return TWITTER_kOAuthConsumerSecret;
}

- (NSString*)twitterCallbackUrl {
    return TWITTER_OAUTH_CALLBACK_URL;
}

// To use xAuth, set to 1
- (NSNumber*)twitterUseXAuth {
	return [NSNumber numberWithInt:0];
}

- (NSNumber*)forcePreIOS5TwitterAccess {
    // NOTE: for debug we can enable twitter auth through webview in ios5
//	return [NSNumber numberWithBool:YES];
    	return [NSNumber numberWithBool:NO];
}

// Enter your app's twitter account if you'd like to ask the user to follow it when logging in. (Only for xAuth)
//- (NSString*)twitterUsername {
//	return @"";
//}
@end

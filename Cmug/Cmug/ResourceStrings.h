//
//  ResourceStrings.h
//  Cmug
//
//  Created by Dmitry C on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef Cmug_ResourceStrings_h
#define Cmug_ResourceStrings_h

#define ALERT_NoTwitterAccountsTitle @"No Twitter Accounts"
#define ALERT_NoTwitterAccountsMessage @"Please setup you twitter account first"

#define ALERT_FacebookErrorTitle @"Facebook"
#define ALERT_FacebookErrorMessage @"Couldn't connect to your facebook account :("

#define ALERT_WarningTitle  @"Warning"
#define ALERT_PleaseEnterValidEmail @"Please enter valid email"
#define ALERT_PleaseEnterPassword @"Please enter your password"

#endif

//
//  AmazonObject.m
//  Crowd-iOS
//
//  Created by Skeeet on 3/31/11.
//  Copyright 2011 Munky Interactive / munkyinteractive.com. All rights reserved.
//

#import "AmazonObject.h"


@implementation AmazonObject

@synthesize urlLocation;
@synthesize fileLocation;
@synthesize status;
@synthesize isImage;
@synthesize progress = _progress;
@synthesize request;
@synthesize size=size_;

@synthesize progressDelegate;
@synthesize uploadProgressSelector;
@synthesize postedOnServer;

@synthesize thumbnailLocation;


+(AmazonObject*) objectWithFile:(NSString*)filePath url:(NSString*)url isImage:(BOOL)image
{
	return [[[self alloc] initWithFile:filePath url:url isImage:image] autorelease];
}

-(id) initWithFile:(NSString*)filePath url:(NSString*)url isImage:(BOOL)image
{
	self = [super init];
	if (self != nil) 
	{
		self.urlLocation = url;
		self.fileLocation = filePath;
		self.status = kStatusBeforeUploading;
		self.isImage = image;
		self.progress = 0;
		self.progressDelegate = nil;
		self.uploadProgressSelector = NULL;
		self.request = nil;
		self.thumbnailLocation = nil;
	}
	return self;
}


- (id) init
{
	self = [super init];
	if (self != nil) 
	{
		self.urlLocation = nil;
		self.fileLocation = nil;
		self.status = kStatusBeforeUploading;
	}
	return self;
}

- (void) dealloc
{
	self.urlLocation = nil;
	self.fileLocation = nil;
	self.status = kStatusInvalid;
	
	[super dealloc];
}


-(NSString *) description
{
	return [NSString stringWithFormat:@"<AmazonObject>:urlLocation = %@,fileLocation = %@, isImage = %d, status = %d",
			self.urlLocation,self.fileLocation,self.isImage,self.status];
}

-(void) setProgress:(float) newProgress
{
	if(fabs(_progress - _oldprogress)>0.05) _oldprogress = _progress;
	_progress = newProgress;
	//CRLog(@"progress: %f",_progress);
}

- (void)request:(ASIHTTPRequest *)request didSendBytes:(long long)bytes
{
	//small optimization. update each 5 percents
	if(fabs(_progress - _oldprogress)<0.05) return;
	if(self.progressDelegate && [self.progressDelegate respondsToSelector:uploadProgressSelector])
	{
		[self.progressDelegate performSelector:uploadProgressSelector];
	}
}


@end

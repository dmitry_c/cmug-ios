//
//  AmazonObject.h
//  Crowd-iOS
//
//  Created by Skeeet on 3/31/11.
//  Copyright 2011 Munky Interactive / munkyinteractive.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"

#define AMAZON_THUMBNAIL_WIDTH 230
#define AMAZON_THUMBNAIL_HEIGHT 175
#define AMAZON_VIDEO_THUMBNAIL_WIDTH 230
#define AMAZON_VIDEO_THUMBNAIL_HEIGHT 175
#define AMAZON_FULL_THUMBNAIL_WIDTH 640
#define AMAZON_FULL_THUMBNAIL_HEIGHT 960
#define AVA_MAX_SIZE 150

typedef enum
{
	kStatusInvalid = 0,
	kStatusUploading = 1,
	kStatusCancelled = 2,
	kStatusDownloading = 3,
	kStatusDone = 4,
	kStatusMAX = 5,
	kStatusBeforeUploading = 6
} AmazonObjectStatus;




@interface AmazonObject : NSObject <ASIProgressDelegate>
{
	float _progress;
	float _oldprogress;
	
	int size_;
}
//path on server, like /images/somename.jpg
@property(retain,nonatomic) NSString * urlLocation;

//path on file system
@property(retain,nonatomic) NSString * fileLocation;

//path on file system
@property(retain,nonatomic) NSString * thumbnailLocation;

//object status
@property(assign,nonatomic) AmazonObjectStatus status;

//if object is image
@property(assign,nonatomic) BOOL isImage;

@property (nonatomic) int size;

//upload/download progress
@property(assign,nonatomic) float progress;
@property(assign,nonatomic) id progressDelegate;
@property(assign,nonatomic) SEL uploadProgressSelector;

@property(retain,nonatomic) ASIHTTPRequest * request;

@property(assign,nonatomic) BOOL postedOnServer;



+(AmazonObject*) objectWithFile:(NSString*)filePath url:(NSString*)url isImage:(BOOL)image;

-(id) initWithFile:(NSString*)filePath url:(NSString*)url isImage:(BOOL)image;


@end

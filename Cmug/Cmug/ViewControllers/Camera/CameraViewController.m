//
//  CRCameraViewController.m
//

#import "CameraViewController.h"
#import "ImagePickerController.h"

#import <MobileCoreServices/UTCoreTypes.h>
#import "FileCache.h"
#import "AuthorizationManager.h"
#import "UIImage+Resize.h"
#import "MBProgressHUD.h"

//#define CAMERA_SCALAR 1.12412 // scalar = (480 / (2048 / 480))
#define CAMERA_SCALAR 1.33 // scalar = (480 / (2048 / 480))

#ifndef C_Log//(...)
#define C_Log(...) NSLog(__VA_ARGS__)
//#define C_Log(...) (void)0
#endif


@interface CameraViewController ()
- (void)layoutControlsDepentCaptureMode:(UIImagePickerControllerCameraCaptureMode)capMode inRecording:(BOOL)inRecording;
@end

@implementation CameraViewController
@synthesize photoVideoSwitch;
@synthesize bottomPanel;

@synthesize cameraView, sourceBtn, captureBtn, camSourceBtn, delegate, imagePicker, state;

static BOOL redidectToProfile = FALSE;

- (void)availCameraControls
{
	// enable controls
	[self.captureBtn setUserInteractionEnabled:YES];
}

+ (void)redirectToProfile:(BOOL)must
{
	redidectToProfile = must;
	
//	[CRCameraPhaseThreeViewController mustRedirectToProfile];
}

+ (BOOL)mustRedirectToProfile
{
	return redidectToProfile;
}

- (void)viewDidUnload
{
	self.cameraView = nil;
	self.captureBtn = nil;
	self.sourceBtn = nil;
	self.camSourceBtn = nil;
//	[self.imagePicker release];
	self.imagePicker = nil;
	
    [self setPhotoVideoSwitch:nil];
    [self setBottomPanel:nil];
    [super viewDidUnload];
}

- (void)dealloc
{
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
	self.cameraView = nil;
	self.captureBtn = nil;
	self.sourceBtn = nil;
	self.camSourceBtn = nil;
//	[self.imagePicker release];
	self.imagePicker = nil;
	
//	[super dealloc];
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    C_Log(@"%@::%@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    
	[super viewWillAppear:animated];
	
	[self.imagePicker viewWillAppear:animated];
	
	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
	[self.navigationController setNavigationBarHidden:YES];
	
	// disable controls
	[self.captureBtn setUserInteractionEnabled:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    C_Log(@"%@::%@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    
    [super viewDidAppear:animated];
	
	[self.imagePicker viewDidAppear:animated];
	
	[self performSelector:@selector(availCameraControls)
			   withObject:nil
			   afterDelay:(!notFirstAppearance_?4:1)];
	
	notFirstAppearance_ = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    C_Log(@"%@::%@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    
	[super viewWillDisappear:animated];
	
	[self.imagePicker viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    C_Log(@"%@::%@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    
	[super viewDidDisappear:animated];
	
	[self.imagePicker viewDidDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
#if !TARGET_IPHONE_SIMULATOR
	// prepare camera
	self.imagePicker = [[UIImagePickerController alloc] init];
	self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
	NSArray* mediaTypes = (mode == CRCameraViewControllerModeUpload
						   ? [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeImage, nil]
						   : [NSArray arrayWithObjects:(NSString *)kUTTypeImage, nil]);
	self.imagePicker.mediaTypes = mediaTypes;
	self.imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    self.imagePicker.showsCameraControls = NO;
    self.imagePicker.navigationBarHidden = NO;
    self.imagePicker.toolbarHidden = YES;
    self.imagePicker.wantsFullScreenLayout = YES;
	self.imagePicker.delegate = self;
	self.imagePicker.cameraViewTransform = CGAffineTransformScale(self.imagePicker.cameraViewTransform, CAMERA_SCALAR, CAMERA_SCALAR);
	self.wantsFullScreenLayout = YES;
	[self.cameraView addSubview:self.imagePicker.view];
	[self.imagePicker.view setFrame:CGRectMake(0, 0, self.cameraView.frame.size.width, self.cameraView.frame.size.height)];
#endif
	
	self.navigationController.wantsFullScreenLayout = YES;
	[self.navigationController setNavigationBarHidden:YES];
	
	[self layoutControlsDepentCaptureMode:self.imagePicker.cameraCaptureMode inRecording:FALSE];
}

#pragma mark Rotations

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Camera controls

- (void)layoutControlsDepentCaptureMode:(UIImagePickerControllerCameraCaptureMode)capMode inRecording:(BOOL)inRecording
{
	if(capMode == UIImagePickerControllerCameraCaptureModeVideo)
	{
		[self.sourceBtn setTitle:@"Photo" forState:UIControlStateNormal];
		[self.sourceBtn setEnabled:!inRecording];
//		[self.captureBtn setBackgroundImage:(inRecording ? [UIImage imageNamed:@"btn.video.pause.png"] : [UIImage imageNamed:@"btn.video.rec.big.png"])
//								   forState:UIControlStateNormal];
	}
	else
	{
		[self.sourceBtn setTitle:@"Video" forState:UIControlStateNormal];
//		[self.captureBtn setBackgroundImage:[UIImage imageNamed:@"btn.cam.big.png"] forState:UIControlStateNormal];
	}
	
	// controls
	self.camSourceBtn.hidden = ![UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
	self.sourceBtn.hidden = ![self.imagePicker.mediaTypes containsObject:(NSString*)kUTTypeMovie];
}

- (IBAction)changeCamera:(id)sender
{
	UIImagePickerControllerCameraDevice camDevice = [self.imagePicker cameraDevice];
	if(camDevice == UIImagePickerControllerCameraDeviceFront)
		camDevice = UIImagePickerControllerCameraDeviceRear;
	else
		camDevice = UIImagePickerControllerCameraDeviceFront;
	
	[self.imagePicker setCameraDevice:camDevice];
	
	[self layoutControlsDepentCaptureMode:self.imagePicker.cameraCaptureMode inRecording:recordingVideo_];
	
	[self.captureBtn setUserInteractionEnabled:NO];
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
	[self performSelector:@selector(availCameraControls)
			   withObject:nil
			   afterDelay:2];
}

- (IBAction)changeSource:(id)sender
{
	UIImagePickerControllerCameraCaptureMode capMode = [self.imagePicker cameraCaptureMode];
	if(capMode == UIImagePickerControllerCameraCaptureModePhoto)
		capMode = UIImagePickerControllerCameraCaptureModeVideo;
	else
		capMode = UIImagePickerControllerCameraCaptureModePhoto;
	[self.imagePicker setCameraCaptureMode:capMode];
	
	[self layoutControlsDepentCaptureMode:self.imagePicker.cameraCaptureMode inRecording:recordingVideo_];
	
	[self.captureBtn setUserInteractionEnabled:NO];
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
	[self performSelector:@selector(availCameraControls)
			   withObject:nil
			   afterDelay:3];
}

- (IBAction)takePhoto:(id)sender
{
	if([self.imagePicker cameraCaptureMode] == UIImagePickerControllerCameraCaptureModePhoto)
	{
		[self.imagePicker takePicture];
		[MBProgressHUD showHUDAddedTo:self.view animated:YES];
	}
	else
	{
		if(recordingVideo_)
			[self.imagePicker stopVideoCapture];
		else
			[self.imagePicker startVideoCapture];
		
		recordingVideo_ = !recordingVideo_;
	}
	
	[self layoutControlsDepentCaptureMode:self.imagePicker.cameraCaptureMode inRecording:recordingVideo_];
}

- (IBAction)useLibrary:(id)sender
{
	UIImagePickerController* imPickerController = [[ImagePickerController alloc] init];
	[imPickerController setWantsFullScreenLayout:YES];
	[imPickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
	[imPickerController setMediaTypes:[NSArray arrayWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeImage, nil]];
	[imPickerController setDelegate:self];
	
	[self presentModalViewController:imPickerController animated:YES];
}

- (IBAction)cancel:(id)sender
{
	if(redidectToProfile)
	{
		UINavigationController* navC = [[self.tabBarController viewControllers] objectAtIndex:4];
		[navC popToRootViewControllerAnimated:NO];
		redidectToProfile = FALSE;
		return;
	}
    [self.delegate cameraDidCancel:self];
}

#pragma Mark -
#pragma UIImagePickerControllerDelegate

- (void)finishPickingImage:(NSDictionary*)dic
{
	UIImagePickerController* picker = [dic objectForKey:@"picker"];
	MediaObject* doubleObj = [dic objectForKey:@"dObj"];
	[MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)storeToDisk:(NSDictionary*)params
{
	UIImage* img = [params objectForKey:@"image"];
	NSString* path = [params objectForKey:@"path"];
	
	if([[FileCache sharedCache] putToCache:img name:path])
		[self finishPickingImage:params];
	else
		[self performSelector:@selector(storeToDisk:)
				   withObject:params
				   afterDelay:0.3];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	CRLog(@"INFO = %@", info);
	
	NSString *prefix = @"2";
	MediaObject* doubleObj = nil;
	
	[MBProgressHUD hideHUDForView:self.view animated:NO];
	[MBProgressHUD showHUDAddedTo:self.view animated:NO];
	
	//for randomSequenceWithPrefix:
	srandom(time(NULL));
	
	UIImage* image = nil;
	NSString* path = nil;
	
	if([[info objectForKey:UIImagePickerControllerMediaType] isEqual:(NSString*)kUTTypeMovie]) // we have movie
	{
		NSURL * videoUrl = [info objectForKey:UIImagePickerControllerMediaURL];
//		NSString* rnd = [CRPlacesAddMediaViewController randomSequenceWithPrefix:prefix];
        NSString *rnd = @"TODO:random string id";
		NSString *path = [NSString stringWithFormat:@"/videos/%@.mov", rnd];
		
		[[FileCache sharedCache] putToCache:[videoUrl absoluteString] name:path];
//		MediaObject *object = [MediaObject objectWithFile:[[FileCache sharedCache] getFromCache:path]
//														 url:path
//													 isImage:NO];
//		object.thumbnailLocation = path;
//		object.progressDelegate = self;
//		object.uploadProgressSelector = @selector(uploadProgressChanged);
		
		NSString* objPath = [[FileCache sharedCache] getFullPathFromCache:path];
		NSString * thumbPath = [NSString stringWithFormat:@"/photos/%@.jpg", rnd];
		MPMoviePlayerController* movie = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:objPath]];
		[movie setInitialPlaybackTime:-1];
		UIImage* image = [movie thumbnailImageAtTime:movie.duration / 2.0
										  timeOption:MPMovieTimeOptionNearestKeyFrame];
		float duration = movie.duration;
		[movie pause];
		[movie stop];
//		[movie release];
		MediaObject* aObjectThumb = nil;
		if(image)
		{
			image = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFill
												bounds:CGSizeMake(AMAZON_VIDEO_THUMBNAIL_WIDTH, AMAZON_VIDEO_THUMBNAIL_HEIGHT)
								  interpolationQuality:kCGInterpolationHigh];
			
			[[FileCache sharedCache] putToCache:image name:thumbPath];
//			aObjectThumb = [MediaObject objectWithFile:[[FileCache sharedCache] getFullPathFromCache:thumbPath]
//													url:thumbPath
//												isImage:YES];
//			aObjectThumb.thumbnailLocation = thumbPath;
		}
		
//		doubleObj = [MediaObject objectWithCameraObject:object
//                                            andThumbnail:aObjectThumb];
////		doubleObj.isFresh = YES;
//		doubleObj.duration = duration;
//		doubleObj.ownerId = [[AuthorizationManager sharedManager] userId];
////		doubleObj.ownerName = [[AuthorizationManager sharedManager] shortname];
	}
	else //we have photo
	{
		image = [info objectForKey:UIImagePickerControllerOriginalImage];
//		NSString* rnsSequence = [CRPlacesAddMediaViewController randomSequenceWithPrefix:prefix];
        NSString* rnsSequence = @"TODO:random file name";
		path = [NSString stringWithFormat:@"/photos/%@.jpg", rnsSequence];
		
		//resize image to new full size
//		if(mode == CRCameraViewControllerModeTakePhoto)
//		{
//			image = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFit
//												bounds:CGSizeMake(AVA_MAX_SIZE, AVA_MAX_SIZE)
//								  interpolationQuality:kCGInterpolationHigh];
//		}
//		else if(mode == CRCameraViewControllerModeUpload)
//		{
//			image = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFit
//												bounds:CGSizeMake(AMAZON_FULL_THUMBNAIL_WIDTH, AMAZON_FULL_THUMBNAIL_HEIGHT)
//								  interpolationQuality:kCGInterpolationHigh];
//		}
		
//		MediaObject * object = [MediaObject objectWithFile:[[FileCache sharedCache] getFullPathFromCache:path]
//														 url:path
//													 isImage:YES];
//		object.thumbnailLocation = path;
//		object.progressDelegate = self;
//		object.uploadProgressSelector = @selector(uploadProgressChanged);
//		
//		doubleObj = [MediaObject objectWithCameraObject:object
//                                            andThumbnail:nil];
//		doubleObj.isFresh = YES;
	}
	
	NSMutableDictionary* dic = [NSMutableDictionary dictionary];
	[dic setObject:image forKey:@"image"];
	[dic setObject:path forKey:@"path"];
	[dic setObject:picker forKey:@"picker"];
	[dic setObject:doubleObj forKey:@"dObj"];
	[self storeToDisk:dic];
	
    [self.delegate camera:self didFinishWithImage: doubleObj];
    
	if(self.modalViewController)
		[self dismissModalViewControllerAnimated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[self dismissModalViewControllerAnimated:YES];
}

@end

//
//  MediaObject.h
//  Cmug
//
//  Created by Dmitry C on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

// TODO: review this
#define AMAZON_THUMBNAIL_WIDTH 230
#define AMAZON_THUMBNAIL_HEIGHT 175
#define AMAZON_VIDEO_THUMBNAIL_WIDTH 230
#define AMAZON_VIDEO_THUMBNAIL_HEIGHT 175
#define AMAZON_FULL_THUMBNAIL_WIDTH 640
#define AMAZON_FULL_THUMBNAIL_HEIGHT 960
#define AVA_MAX_SIZE 150

typedef enum
{
	kStatusInvalid = 0,
	kStatusUploading = 1,
	kStatusCancelled = 2,
	kStatusDownloading = 3,
	kStatusDone = 4,
	kStatusMAX = 5,
	kStatusBeforeUploading = 6
} MediaObjectStatus;


typedef enum 
{
    kMediaTypePhoto = 0,
    kMediaTypeVideo
} MediaType;


@interface MediaObject : NSObject
{
	float _progress;
	float _oldprogress;
	
	int size_;
}

@property (retain, nonatomic) NSString *urlLocation;
@property (retain, nonatomic) NSString *fileLocation;

@property(assign,nonatomic) MediaObjectStatus status;
@property (assign,nonatomic) MediaType mediaType;

@property (nonatomic, readonly) NSUInteger size;
// video specific
@property (nonatomic, readonly) NSUInteger duration;
@property (nonatomic, assign) NSUInteger thumbnailFrame; // miliseconds
@property (retain, nonatomic) NSString *thumbnailLocalPath;
@property (retain, nonatomic) NSString *thumbnailUrl;


+ (MediaObject*)objectWithFile:(NSString*)filePath url:(NSString*)url;

- (id)initWithFile:(NSString*)filePath url:(NSString*)url;

@end
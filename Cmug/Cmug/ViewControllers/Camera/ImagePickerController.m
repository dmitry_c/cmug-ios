//
//  ImagePickerController.m
//

#import "ImagePickerController.h"

@implementation ImagePickerController
- (void)hideStatusBar
{
	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	timer_ = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(hideStatusBar) userInfo:nil repeats:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[timer_ invalidate];
	timer_ = nil;
}
@end

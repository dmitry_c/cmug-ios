//
//  CameraViewController.h
//

#import <UIKit/UIKit.h>
#import "MediaObject.h"
#import <MediaPlayer/MediaPlayer.h>
#import "PhotoVideoSwitch.h"

typedef enum {
    kCameraPhoto = 1 << 0,
    kCameraVideo = 1 << 1,
    kCameraFront = 1 << 2,
    kCameraBack = 1 << 3
} CameraViewControllerState;

@protocol CameraViewControllerDelegate;

@interface CameraViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    BOOL notFirstAppearance_;
  	BOOL recordingVideo_;
}

@property (nonatomic, retain) IBOutlet UIView* cameraView;
@property (nonatomic, retain) IBOutlet UIButton* sourceBtn;
@property (nonatomic, retain) IBOutlet UIButton* camSourceBtn;
@property (nonatomic, retain) IBOutlet UIButton* captureBtn;
@property (unsafe_unretained, nonatomic) IBOutlet PhotoVideoSwitch *photoVideoSwitch;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *bottomPanel;

@property (nonatomic, retain) UIImagePickerController* imagePicker;
@property (nonatomic) CameraViewControllerState state;
@property (nonatomic, assign) id<CameraViewControllerDelegate> delegate;

- (IBAction)changeCamera:(id)sender;
- (IBAction)changeSource:(id)sender;
- (IBAction)takePhoto:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)useLibrary:(id)sender;

@end


@protocol CameraViewControllerDelegate

- (void)camera:(id)cameraViewController didFinishWithImage:(MediaObject*)cameraObject;
- (void)cameraDidCancel:(id)cameraViewController;

@end

//
//  CameraObject.m
//  Cmug
//
//  Created by Dmitry C on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MediaObject.h"

@implementation MediaObject

@synthesize urlLocation;
@synthesize thumbnailUrl = _thumbnailUrl;
@synthesize mediaType = _mediaType;
@synthesize thumbnailFrame;
@synthesize thumbnailLocalPath;
@synthesize fileLocation;
@synthesize status;
@synthesize size;

@synthesize duration = _duration;

+ (MediaObject*)objectWithFile:(NSString*)filePath url:(NSString*)url
{
	return [[self alloc] initWithFile:filePath url:url];
}

- (id)initWithFile:(NSString*)filePath url:(NSString*)url
{
	self = [super init];
	if (self != nil) 
	{
		self.urlLocation = url;
		self.fileLocation = filePath;
		self.status = kStatusBeforeUploading;
	}
	return self;
}


- (id)init
{
	self = [super init];
	if (self != nil) 
	{
		self.urlLocation = nil;
		self.fileLocation = nil;
		self.status = kStatusBeforeUploading;
	}
	return self;
}

- (void)dealloc
{
	self.urlLocation = nil;
	self.fileLocation = nil;
	self.status = kStatusInvalid;
}


- (NSString *)description
{
	return [NSString stringWithFormat:@"<%@>:urlLocation = %@,fileLocation = %@, isImage = %d, status = %d", NSStringFromClass(self.class), self.urlLocation, self.fileLocation, self.status];
}

@end

//
//  LabelImageCell.m
//  Cmug
//
//  Created by Dmitry C on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LabelImageCell.h"

@implementation LabelImageCell

static NSString *CellIdentifier = @"IDLabelImageCell";

@synthesize label, image;
@synthesize imageObject=_imageObject;

- (NSString *) reuseIdentifier {
    return CellIdentifier;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        // initialization
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

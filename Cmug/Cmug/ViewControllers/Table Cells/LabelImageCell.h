//
//  LabelImageCell.h
//  Cmug
//
//  Created by Dmitry C on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaObject.h"
#import "UITableViewCell+LoadCell.h"

@interface LabelImageCell : UITableViewCell

@property (nonatomic, unsafe_unretained) IBOutlet UILabel* label;
@property (nonatomic, unsafe_unretained) IBOutlet UIImageView* image;

@property (nonatomic, retain) MediaObject* imageObject;

@end

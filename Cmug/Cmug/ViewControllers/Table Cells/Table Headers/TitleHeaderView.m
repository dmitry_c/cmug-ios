//
//  TitleHeaderView.m
//  Cmug
//
//  Created by Dmitry C on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TitleHeaderView.h"
#define LABEL_TAG 10282

@interface TitleHeaderView()
@end

@implementation TitleHeaderView
@synthesize textLabel = _textLabel;

- (void)setTextLabel:(NSString *)textLabel
{
    if (_textLabel != textLabel) {
        _textLabel = textLabel;
        UILabel *label = (UILabel*)[self viewWithTag:LABEL_TAG];
        [label setText:_textLabel];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        float fontSize = 18.0f;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, (frame.size.height - fontSize) * 0.5, frame.size.width - 20, frame.size.height - fontSize)];
        label.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        label.font = [UIFont systemFontOfSize:fontSize];
        label.text = self.textLabel;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.tag = LABEL_TAG;
        label.shadowColor = [UIColor colorWithRed:59.0f/255.0f green:95.0f/255.0f blue:117.0f/255.0f alpha:0.8];
        label.shadowOffset = CGSizeMake(1.0, 1.0);
        [self addSubview:label];
        
        // add navy background gradient
        self.layer.masksToBounds = NO;
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.bounds;
        gradient.colors = [NSArray arrayWithObjects:
                           (id)[[UIColor colorWithRed:73.0f/255.0f green:119.0f/255.0f blue:141.0f/255.0f alpha:1.0] CGColor],
                           (id)[[UIColor colorWithRed:72.0f/255.0f green:117.0f/255.0f blue:139.0f/255.0f alpha:1.0] CGColor],
                           (id)[[UIColor colorWithRed:60.0f/255.0f green:98.0f/255.0f blue:116.0f/255.0f alpha:1.0] CGColor],
                           nil];
        gradient.locations = [NSArray arrayWithObjects:
                              [NSNumber numberWithFloat:0.0], 
                              [NSNumber numberWithFloat:0.32], 
                              [NSNumber numberWithFloat:1.0], 
                              nil];
        [self.layer insertSublayer:gradient atIndex:0];
    }
    return self;
}

- (id)initForTableView:(UITableView*)tableView inSection:(NSInteger)section
{
    NSString* title = [tableView.dataSource tableView:tableView titleForHeaderInSection:section];
    float height = [tableView.delegate tableView:tableView heightForHeaderInSection:section];
    _textLabel = title;
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, height);
    return [self initWithFrame:frame];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

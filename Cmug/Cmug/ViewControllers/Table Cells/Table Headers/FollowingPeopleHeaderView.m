//
//  FollowingPeopleHeaderView.m
//  Cmug
//
//  Created by Dmitry C on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FollowingPeopleHeaderView.h"
#import <QuartzCore/QuartzCore.h>

@implementation FollowingPeopleHeaderView
@synthesize actionButton;
@synthesize followingLabel;
@synthesize delegate;
@synthesize dataSource;

- (void) setup
{
    self.layer.masksToBounds = NO;
    self.layer.backgroundColor = [[UIColor colorWithRed:57.0/255.0 green:73.0/255.0 blue:79.0/255.0 alpha:1.0] CGColor];
    self.followingLabel.textColor = [UIColor whiteColor];
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

+ (id)loadFromNib
{
    FollowingPeopleHeaderView *header = [super loadFromNib];
    [header setup];
    return header;
}

- (IBAction)editButtonClick:(UIButton*)sender {
    [sender setSelected:!sender.selected];
    [self.delegate followingPeopleHeaderView:self didButtonAction:sender];
}

- (void) reloadData
{
    NSInteger peopleCount = [self.dataSource followingPeopleHeaderViewPeopleCount];
    if (peopleCount == 0) {
        self.followingLabel.text = @"Following nobody.";
    } else if (peopleCount == 1) {
        self.followingLabel.text = @"Following one person.";
    } else {
        self.followingLabel.text = [NSString stringWithFormat: @"Following %d people", peopleCount];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

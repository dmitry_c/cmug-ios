//
//  FollowingPeopleHeaderView.h
//  Cmug
//
//  Created by Dmitry C on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DarkActionButton.h"
#import "UIView+LoadFromNib.h"

@class FollowingPeopleHeaderView;

@protocol FollowingPeopleHeaderViewDelegate
- (void) followingPeopleHeaderView: (FollowingPeopleHeaderView*) headerView didButtonAction:(id)sender;
@end

@protocol FollowingPeopleHeaderViewDataSource
- (NSInteger) followingPeopleHeaderViewPeopleCount;
@end

@interface FollowingPeopleHeaderView : UIView
@property (unsafe_unretained, nonatomic) IBOutlet DarkActionButton *actionButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *followingLabel;

- (void) reloadData;

@property (nonatomic, retain) id<FollowingPeopleHeaderViewDelegate> delegate;
@property (nonatomic, retain) id<FollowingPeopleHeaderViewDataSource> dataSource;
@end

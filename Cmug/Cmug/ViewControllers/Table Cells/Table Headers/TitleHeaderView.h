//
//  TitleHeaderView.h
//  Cmug
//
//  Created by Dmitry C on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface TitleHeaderView : UIView
@property (nonatomic, copy) NSString* textLabel;

- (id)initForTableView:(UITableView*)tableView inSection:(NSInteger)section;
@end

//
//  CellSeparator.m
//  Cmug
//
//  Created by Dmitry C on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CellSeparator.h"
#import <QuartzCore/QuartzCore.h>

@implementation CellSeparator

+ (id) decorateView:(UIView*)view withSeparatorStyle: (CellSeparatorStyle) separatorStyle withFrame:(CGRect)frame
{
    NSArray *strokeColors=nil;
    
    switch (separatorStyle) {
        case CellSeparatorStyleDark:
            strokeColors = [NSArray arrayWithObjects:
                            [UIColor colorWithRed:36.0f/255.0f green:36.0f/255.0f blue:35.0f/255.0f alpha:1.0], 
                            [UIColor colorWithRed:62.0f/255.0f green:62.0f/255.0f blue:62.0f/255.0f alpha:1.0], 
                            nil];

            break;
        case CellSeparatorStyleLight:
            strokeColors = [NSArray arrayWithObjects:
                            [UIColor colorWithRed:201.0f/255.0f green:201.0f/255.0f blue:201.0f/255.0f alpha:1.0], 
                            [UIColor colorWithRed:251.0f/255.0f green:252.0f/255.0f blue:252.0f/255.0f alpha:1.0], 
                            nil];

            break;
        default:
            break;
    }
    
    CALayer *layer = view.layer;
    NSArray *sublayers = [layer.sublayers copy];
    for (CALayer *l in sublayers) {
        if ([l isKindOfClass:CAShapeLayer.class]) {
            [l removeFromSuperlayer];
        }
    }
    
    for (NSUInteger colorNumber=0; colorNumber<strokeColors.count; colorNumber++) {
        CAShapeLayer *lineShape = nil;
        CGMutablePathRef linePath = nil;
        linePath = CGPathCreateMutable();
        lineShape = [CAShapeLayer layer];

        float lineWidth = 0.15f;
        lineShape.lineWidth = lineWidth;
        lineShape.lineCap = kCALineCapSquare;
        lineShape.strokeColor = [[strokeColors objectAtIndex:0] CGColor];

        CGFloat y = frame.origin.y + frame.size.height + (colorNumber - 2.0f) * lineWidth;
        CGFloat endX = frame.size.width; 

        CGPathMoveToPoint(linePath, NULL, 0, y);
        CGPathAddLineToPoint(linePath, NULL, endX, y);

        lineShape.path = linePath;
        CGPathRelease(linePath);
        [layer addSublayer:lineShape];
    }
    
    return view;
}

+ (id) decorateView:(UIView*)view withSeparatorStyle: (CellSeparatorStyle) separatorStyle
{
    float separatorHeight = 2.0f;
    return [self decorateView:view withSeparatorStyle:separatorStyle withFrame: CGRectMake(0, view.frame.size.height - separatorHeight, view.frame.size.width, separatorHeight)];
}

+ (id) decorateCell:(UITableViewCell*)cell withSeparatorStyle: (CellSeparatorStyle) separatorStyle
{
    float separatorHeight = 2.0f;
    [self decorateView:cell.contentView withSeparatorStyle:separatorStyle withFrame: CGRectMake(0, cell.frame.size.height - separatorHeight, cell.frame.size.width, separatorHeight)];
    return cell;
}

+ (id) decorateCell:(UITableViewCell*)cell withSeparatorStyle: (CellSeparatorStyle) separatorStyle withHeight: (float) height
{
    float separatorHeight = 2.0f;
    [self decorateView:cell.contentView withSeparatorStyle:separatorStyle withFrame: CGRectMake(0, height - separatorHeight, cell.frame.size.width, separatorHeight)];
    return cell;
}

@end

//
//  CellSeparator.h
//  Cmug
//
//  Created by Dmitry C on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    CellSeparatorStyleDark = 0,  
    CellSeparatorStyleLight
} CellSeparatorStyle;

@interface CellSeparator : NSObject
+ (id) decorateView:(UIView*)view withSeparatorStyle: (CellSeparatorStyle) separatorStyle;
+ (id) decorateCell:(UITableViewCell*)cell withSeparatorStyle: (CellSeparatorStyle) separatorStyle;
+ (id) decorateCell:(UITableViewCell*)cell withSeparatorStyle: (CellSeparatorStyle) separatorStyle withHeight: (float) height;
@end

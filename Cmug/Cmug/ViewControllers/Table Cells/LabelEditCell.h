//
//  LabelEditCell.h
//  Cmug
//
//  Created by Dmitry C on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+LoadCell.h"

@interface LabelEditCell : UITableViewCell

@property (nonatomic, unsafe_unretained) IBOutlet UILabel* label;
@property (nonatomic, unsafe_unretained) IBOutlet UITextField* input;

@end

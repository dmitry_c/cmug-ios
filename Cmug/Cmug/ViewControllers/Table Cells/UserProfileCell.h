//
//  FollowedProfileCell.h
//  Cmug
//
//  Created by Dmitry C on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileShort.h"

typedef enum UserProfileCellAction {
    UserProfileCellAction_None = 0,
    UserProfileCellAction_Invite = 1,
    UserProfileCellAction_Follow,
    UserProfileCellAction_Unfollow,
    UserProfileCellAction_Select
} UserProfileCellAction;

@protocol UserProfileActionProtocol <NSObject>
- (void) actionPerformed:(UserProfileCellAction)action inCell:(UITableViewCell*)cell;
@end


static NSString *CellIdentifier = @"IDUserProfileCell";

@interface UserProfileCellDataAdapter:NSObject
+ (ProfileShort*) profileWithData:(id)data;
+ (NSDictionary*) adoptData:(id)data;
@end

@interface UserProfileCell : UITableViewCell
@property (nonatomic, retain) id profileData;

@property (nonatomic, assign) UserProfileCellAction cellAction;
@property (nonatomic, readonly) UIButton* actionButton;

@property (nonatomic, retain) id<UserProfileActionProtocol> delegate;
@property (nonatomic, readonly) NSDictionary* adoptedProfileData;

- (void) updateAccesoriesIfNeeded;
@end

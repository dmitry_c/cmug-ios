//
//  FollowedProfileCell.m
//  Cmug
//
//  Created by Dmitry C on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserProfileCell.h"
#import "FollowingService.h"
#import "LightActionButton.h"
#import "CustomButton.h"
#import "TableCellAccessories.h"
#import "ProfileShort.h"
#import "ProfileMatch.h"
#import "AddressBookWrapper.h"
#import "FacebookWrapper.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "NSNull+Additions.h"
#import "UIImage+RoundedCorner.h"

@implementation UserProfileCellDataAdapter

+ (ProfileShort *)profileWithData:(id)data
{
    if ([data isMemberOfClass:ProfileShort.class]) {
        return data;        
    } else if ([data respondsToSelector:@selector(profile)]) {
        id profile = [data performSelector:@selector(profile)];
        if ([profile isMemberOfClass:ProfileShort.class]) {
            return profile;
        }
    }
    return nil;
}

+ (NSDictionary *)adoptData:(id)data
{
    ProfileShort* profile = [self profileWithData:data];
    NSString* name = nil;
    NSString* picture = nil;
    UIImage* pictureData = nil;
    
    if (profile) {
        name = [NSString stringWithFormat: @"%@ %@", 
                profile.firstName,
                profile.lastName];
        picture = profile.picture;

    } else if ([data isMemberOfClass:AddressBookProfileMatch.class]) {
        AddressBookProfileMatch *profile = (AddressBookProfileMatch*)data;
        name = [NSString stringWithFormat: @"%@ %@", 
                profile.firstName,
                profile.lastName];

        pictureData = [AddressBookWrapper imageForContactWithRecordID:[profile.recordId intValue]];
       
    } else if ([data isMemberOfClass:FacebookProfileMatch.class]) {
        FacebookProfileMatch *profile = (FacebookProfileMatch*) data;
        name = profile.name;
        picture = [FacebookWrapper pictureURLForFacebookUserWithId:profile.facebookId];
    } else if ([data isMemberOfClass:TwitterProfileMatch.class]) {
        TwitterProfileMatch *profile = (TwitterProfileMatch*) data;
        name = profile.twitterName;
        picture = profile.twitterImageUrl;
    } else {
        name = @"erroneous";
    }

    return [NSDictionary dictionaryWithKeysAndObjects:
            @"name", [NSNull emptyStringOrValue:name],
            @"picture", [NSNull nullOrValue:picture],
            @"pictureData", [NSNull nullOrValue:pictureData],
            nil];
}
@end


@interface UserProfileCell()
{
    BOOL needControlsUpdate;
}
@property (nonatomic, retain) UIView* defaultAccessoryView;
@property (nonatomic, retain) UIView* customEditingAccessoryView1;

@property (nonatomic, retain) UIImage* defaultProfileImage;
@end

@implementation UserProfileCell
@synthesize profileData=_profileData;
@synthesize delegate;

@synthesize actionButton = _actionButton;
@synthesize defaultAccessoryView = _defaultAccessoryView;
@synthesize customEditingAccessoryView1 = _customEditingAccessoryView1;
@synthesize cellAction = _cellAction;
@synthesize defaultProfileImage = _defaultProfileImage;

@synthesize adoptedProfileData = _adoptedProfileData;


- (void) updateAccesoriesIfNeeded
{
    if (needControlsUpdate) {
        self.accessoryView = self.defaultAccessoryView;
        self.editingAccessoryView = self.customEditingAccessoryView1;
    }
    needControlsUpdate = NO;
}

- (void)setCellAction:(UserProfileCellAction)cellAction
{
    if (_cellAction != cellAction) {
        _cellAction = cellAction;
        needControlsUpdate = YES;
    }
}

- (UIImage *)defaultProfileImage
{
    if (!_defaultProfileImage) {
        _defaultProfileImage = [UIImage imageNamed:@"avatar"];
    }
    return _defaultProfileImage;
}

- (UIButton *)actionButton
{
    NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSArray arrayWithObjects:@"Unfollow", @"Unfollowing...", nil],[NSNumber numberWithInt:UserProfileCellAction_Unfollow],
                              [NSArray arrayWithObjects:@"Follow", @"Following...", nil],[NSNumber numberWithInt:UserProfileCellAction_Follow],
                              [NSArray arrayWithObjects:@"Invite", @"Inviting...", nil],[NSNumber numberWithInt:UserProfileCellAction_Invite],
                              nil];

    NSArray *currentSettings = [settings objectForKey:[NSNumber numberWithInt:self.cellAction]];
    NSString* titleNormal = [currentSettings objectAtIndex:0];
    NSString* titleSelected = [currentSettings objectAtIndex:1];
    
    if (!_actionButton || needControlsUpdate) {
        _actionButton = [[LightActionButton alloc] initWithFrame:CGRectMake(0, 0, 60.0f, 28.0f)];
        [_actionButton setTitle: titleNormal forState:UIControlStateNormal];
        [_actionButton setTitle: titleSelected forState:UIControlStateSelected];
        [_actionButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _actionButton;
}

- (UIView *)customEditingAccessoryView1
{
    // "unfollow" button
    if (!_customEditingAccessoryView1 || needControlsUpdate) {
        // display action button
        _customEditingAccessoryView1 = [[UIView alloc] initWithFrame: self.actionButton.frame];
        [_customEditingAccessoryView1 addSubview:self.actionButton];
    }
    return _customEditingAccessoryView1;
}

- (UIView *)defaultAccessoryView
{
    if (!_defaultAccessoryView || needControlsUpdate) {
        if (self.cellAction == UserProfileCellAction_Select) {
            // accessories handled by caller
        } else {
            _defaultAccessoryView = [TableCellAccessories greyArrowAccessory];
        }
    }
    return _defaultAccessoryView;
}

- (void)setProfileData:(ProfileShort *)profileData
{
    if (_profileData!=profileData) {
        _profileData = profileData;
        _adoptedProfileData = [UserProfileCellDataAdapter adoptData:(id)_profileData];
        [self setNeedsLayout];
    }
}

- (IBAction)buttonAction:(UIButton*)sender
{
    [self.delegate actionPerformed:self.cellAction inCell:self];
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // this works only fro "grouped" style tables
        self.shouldIndentWhileEditing = NO;
        self.indentationWidth = 0.0f;
        self.indentationLevel = 0;
        // fix indentation for "plain" style tables
        [self.contentView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionOld context:nil];
        // image corner radius
//        self.imageView.layer.cornerRadius = 6.0f;
//        self.imageView.layer.masksToBounds = YES;
//        self.imageView.clipsToBounds = YES;

        needControlsUpdate = YES;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)willTransitionToState:(UITableViewCellStateMask)state
{
    needControlsUpdate = YES;
    [self updateAccesoriesIfNeeded];
    
    switch (state) {
        case UITableViewCellStateDefaultMask:
            // default - show disclosure
            self.accessoryView = self.defaultAccessoryView;
            break;
        case UITableViewCellStateEditingMask:
            // edit - show "unfollow"
            self.editingAccessoryView = self.customEditingAccessoryView1;
            break;
        case UITableViewCellStateShowingDeleteConfirmationMask:
            NSLog(@"Showing Delete Confirmation");
            break;
        default:
            break;
    }
}

- (NSString *)reuseIdentifier
{
    return CellIdentifier;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    float margin = 10.0f;
    float imageWidth = 40.0f;
    float imageHeight = 40.0f;
    float labelHeight = 40.0f;
    self.imageView.frame = CGRectMake(margin, margin, imageWidth, imageHeight);
    self.textLabel.text = [self.adoptedProfileData valueForKey:@"name"];
    self.textLabel.frame = CGRectMake(2*margin+imageWidth, margin, self.frame.size.width - 4*margin - imageWidth - self.accessoryView.frame.size.width, labelHeight);
}

#pragma mark - fix messy indentation while table editing
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
//    NSLog(@"observed value for kp %@ changed: %@",keyPath,change);
    if ( [keyPath isEqual:@"frame"] && object == self.contentView )
    {
        CGRect newFrame = self.contentView.frame;
        CGRect oldFrame = [[change objectForKey:NSKeyValueChangeOldKey] CGRectValue];
//        NSLog(@"frame old: %@  new: %@",NSStringFromCGRect(oldFrame),NSStringFromCGRect(newFrame));
        
        if ( newFrame.origin.x != 0 ) self.contentView.frame = oldFrame; //CGRectMake(oldFrame.origin.x, oldFrame.origin.y, newFrame.size.width, newFrame.size.height);
    }
}

- (void)dealloc
{
    [self.contentView removeObserver:self forKeyPath:@"frame"];
}

@end

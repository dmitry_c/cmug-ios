//
//  TableCellAccessories.m
//  Cmug
//
//  Created by Dmitry C on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TableCellAccessories.h"
#import "CheckmarkButton.h"

@implementation TableCellAccessories

+ (UIView*)greyArrowAccessory
{
    UIImage *accessoryImage = [UIImage imageNamed:@"table_accessory_details_arrow_grey"];
    UIView *cellAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, accessoryImage.size.width, accessoryImage.size.height)];
    [cellAccessoryView setClipsToBounds:YES];
    [cellAccessoryView addSubview: [[UIImageView alloc] initWithImage:accessoryImage]];
    return cellAccessoryView;
}

+ (UIView*)greyArrowAccessoryWithFrame:(CGRect)frame
{
    UIImage *accessoryImage = [UIImage imageNamed:@"table_accessory_details_arrow_grey"];
    UIView *cellAccessoryView = [[UIView alloc] initWithFrame:frame];
    [cellAccessoryView setClipsToBounds:YES];
    [cellAccessoryView addSubview: [[UIImageView alloc] initWithImage:accessoryImage]];
    return cellAccessoryView;
}

+ (UIView*)blueCheckmark
{
    UIImage *checkmarkImage = [UIImage imageNamed:@"checkmark_blue.png"];
    return [[UIImageView alloc] initWithImage:checkmarkImage];
}
@end

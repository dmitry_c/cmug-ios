//
//  TableCellAccessories.h
//  Cmug
//
//  Created by Dmitry C on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellAccessories : NSObject
+ (UIView*)greyArrowAccessory;
+ (UIView*)greyArrowAccessoryWithFrame:(CGRect)frame;
+ (UIView*)blueCheckmark;
@end

//
//  IntroViewController.h
//

#import <UIKit/UIKit.h>


@interface IntroViewController : UIViewController 
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *startButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *loginButton;


@property (nonatomic, assign) BOOL forwardToLogin;
@property (nonatomic, assign) BOOL forwardToSignup;

@end


@interface UINavigationController(IntroViewController)
-(IntroViewController*) presentModalIntroViewController;
-(IntroViewController*) presentModalIntroViewControllerAndForwardToLogin:(BOOL)forwardToLogin andForwardToSignup:(BOOL)forwardToSignup;
@end

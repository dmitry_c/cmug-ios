//
//  IntroViewController.m
//

#import "IntroViewController.h"
#import "LoginViewController.h"
#import "NavigationManager.h"


@implementation IntroViewController
@synthesize startButton;
@synthesize loginButton;

@synthesize forwardToLogin, forwardToSignup;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.forwardToLogin) {
        [self.navigationController pushViewController:[[NavigationManager sharedManager] viewControllerByKey:kViewControllerName_Login] animated:YES];
    } else if (self.forwardToSignup) {
        [self.navigationController pushViewController:[[NavigationManager sharedManager] viewControllerByKey:kViewControllerName_Signup] animated:YES];
    }
}

- (void)viewDidUnload
{
    [self setStartButton:nil];
    [self setLoginButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction) onStartBtn:(id)sender
{
    [self.navigationController pushViewController:[[NavigationManager sharedManager] viewControllerByKey:kViewControllerName_Signup] animated:YES];
}


-(IBAction) onLoginBtn:(id)sender
{
    [self.navigationController pushViewController:[[NavigationManager sharedManager] viewControllerByKey:kViewControllerName_Login] animated:YES];
}

@end


@implementation UINavigationController(IntroViewController)

-(IntroViewController*) presentModalIntroViewController
{
    return [self presentModalIntroViewControllerAndForwardToLogin:NO andForwardToSignup:NO];
}

-(IntroViewController*) presentModalIntroViewControllerAndForwardToLogin:(BOOL)forwardToLogin andForwardToSignup:(BOOL)forwardToSignup
{
    IntroViewController* ivc = [[NavigationManager sharedManager] viewControllerByKey:kViewControllerName_Intro];
    ivc.forwardToLogin = forwardToLogin;
    ivc.forwardToSignup = forwardToSignup;
    
    UINavigationController *navigationViewController = [[NavigationManager sharedManager] customizedNavigationController];
    
    [navigationViewController setViewControllers:[NSArray arrayWithObject:ivc]];
    
    [self presentModalViewController:navigationViewController animated:NO];
    return ivc;
}

@end


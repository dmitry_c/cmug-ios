//
//  MainMenuTableViewController.m
//  Cmug
//
//  Created by Dmitry C on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainMenuTableViewController.h"
#import "IIViewDeckController.h"
#import "NavigationManager.h"
#import "AuthorizationManager.h"
#import "IntroViewController.h"

@interface MainMenuTableViewController () <IIViewDeckControllerDelegate>
@property (nonatomic, strong) NSArray* menuItems;
@property (nonatomic, strong) NSDictionary* menuItemsDict;
@end


@implementation MainMenuTableViewController
@synthesize menuItems = _menuItems;
@synthesize menuItemsDict = _menuItemsDict;

- (NSArray*) menuItems
{
    if (!_menuItems || _menuItems.count == 0) {
        _menuItems = [NSArray arrayWithObjects:
                      @"Posts", 
                      @"Request Something", 
                      @"Open Offers",
                      @"Upload Cool Stuff",
                      @"My Activity",
                      @"",
                      @"Profile",
                      @"Settings",
                      @"Logout",
                      @"Following", // menu item added for debug
                      nil];
    }
    return _menuItems;
}


- (NSDictionary*) menuItemsDict
{
    if (!_menuItemsDict || _menuItemsDict.count == 0) {
        _menuItemsDict = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSNumber numberWithInt: kViewControllerName_Posts], @"Posts", 
                          [NSNumber numberWithInt: kViewControllerName_CreateOffer], @"Request Something",
                          [NSNumber numberWithInt: kViewControllerName_Following], @"Following",
                          nil];
    }
    return _menuItemsDict;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.viewDeckController.delegate = self;
    self.tableView.scrollsToTop = NO;
    self.tableView.scrollEnabled = NO;

    // Adjust table width to accomodate right ledge
    self.tableView.frame = (CGRect) { self.viewDeckController.leftLedge, self.tableView.frame.origin.y, 
        self.view.frame.size.width - self.viewDeckController.rightLedge, self.tableView.frame.size.height };

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.menuItems = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MainMenuCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:12];
//    NSLog(@"%d", indexPath.row);
//    NSLog(@"%d", [self.menuItems count]);
    cell.textLabel.text = [self.menuItems objectAtIndex:indexPath.row];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
        NSString* selectedMenuItem = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
        
        if (selectedMenuItem == @"Logout") {
            [[AuthorizationManager sharedManager] logout];
            // force redirect to login page
//            [self.viewDeckController.centerController.navigationController presentModalIntroViewControllerAndForwardToLogin:YES
//                                                                     andForwardToSignup:NO];
        }
        
        if ([controller.centerController isKindOfClass:[UINavigationController class]]) {
            UITableViewController* cc = (UITableViewController*)((UINavigationController*)controller.centerController).topViewController;

            cc.navigationItem.title = selectedMenuItem;
//            [cc.tableView deselectRowAtIndexPath:[cc.tableView indexPathForSelectedRow] animated:NO];
        }

        if ([self.menuItemsDict objectForKey:selectedMenuItem]) {
            [[NavigationManager sharedManager] setDeckCenterViewController:[[NavigationManager sharedManager] viewControllerByKey:((NSNumber*)[self.menuItemsDict objectForKey:selectedMenuItem]).intValue]];

//            controller.centerController = [[NavigationManager sharedManager] viewControllerByKey:((NSNumber*)[self.menuItemsDict objectForKey:selectedMenuItem]).intValue];
        }
        
//        [NSThread sleepForTimeInterval:(300+arc4random()%700)/1000000.0]; // mimic delay... not really necessary
    }];

}

- (BOOL)viewDeckControllerWillOpenLeftView:(IIViewDeckController*)viewDeckController animated:(BOOL)animated {
//    CGRect bounds = self.viewDeckController.leftController.view.bounds;
//    CGFloat ledge = self.viewDeckController.rightLedge;
//    [self.viewDeckController.leftController.view setBounds:CGRectMake(0, 0, bounds.size.width - ledge, bounds.size.height)];
    return YES;
}

@end

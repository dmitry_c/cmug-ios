//
//  FacebookFriendsViewController.m
//  Cmug
//
//  Created by Dmitry C on 6/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FacebookFriendsViewController.h"
#import "ProfileMatch.h"
#import "UserProfileCell.h"
#import "UITableView+LoadCell.h"

#import "Facebook.h"
#import "FacebookWrapper.h"


@interface FacebookFriendsViewController () <UserProfileActionProtocol, FBDialogDelegate>
@property (nonatomic, retain) FacebookWrapper* facebookWrapper;
@end

@implementation FacebookFriendsViewController
@synthesize facebookWrapper = _facebookWrapper;

- (FacebookWrapper *)facebookWrapper
{
    if (!_facebookWrapper) {
        _facebookWrapper = [[FacebookWrapper alloc] init];
    }
    return _facebookWrapper;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView setEditing:YES animated:YES];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - TableView dataSource & delegate

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Facebook Friends on Cmug.";
    } else if (section == 1) {
        return @"Invite Facebook Friends";
    }
    return nil;
}

# pragma mark - UserProfile Cell Actions
- (void) apiDialogInviteSend:(NSString*)facebookId {
    NSLog(@"Before Dialog Open");
    [self.facebookWrapper sendInvitationTo:facebookId delegate:self];
}

- (void)dialogDidNotCompleteWithUrl:(NSURL *)url
{
    NSLog(@"Complete %@", url);
}

- (void)dialogDidNotComplete:(FBDialog *)dialog
{
    NSLog(@"Dialog Did Not Complete");
}

- (void)dialog:(FBDialog *)dialog didFailWithError:(NSError *)error
{
    NSLog(@"Error %@", error);
}

- (void)actionPerformed:(UserProfileCellAction)action inCell:(UserProfileCell *)cell
{
    if (action == UserProfileCellAction_Invite) {
        NSLog(@"Facebook Invitation");
        if (action == UserProfileCellAction_Invite) {
            NSString *facebookId = ((FacebookProfileMatch*)cell.profileData).facebookId;
            [self performSelector:@selector(apiDialogInviteSend:) 
                       withObject:facebookId afterDelay:0.5];
            
        }
    } else {
        [super actionPerformed:action inCell:cell];
    }
}

@end

//
//  ContactsFromAddressBookViewController.m
//  Cmug
//
//  Created by Dmitry C on 6/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ContactsFromAddressBookViewController.h"
#import "ProfileMatch.h"
#import "UITableView+LoadCell.h"
#import "UserProfileCell.h"
#import <MessageUI/MFMessageComposeViewController.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface ContactsFromAddressBookViewController () <UserProfileActionProtocol,MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
@property (strong, nonatomic) NSCache *imageCache;
@end

@implementation ContactsFromAddressBookViewController
@synthesize imageCache = _imageCache;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView setEditing:YES animated:YES];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - TableView dataSource & delegate

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Contacts on Cmug.";
    } else if (section == 1) {
        return @"Invite Contacts";
    }
    return nil;
}

#pragma mark - UserProfile Cell Actions
- (void)actionPerformed:(UserProfileCellAction)action inCell:(UserProfileCell *)cell
{
    if (action == UserProfileCellAction_Invite) {
        NSString *smsBody = @"TODO: Invitation sms body";
        NSString *emailMessageBody = @"TODO: Invitation email body";
        
        AddressBookProfileMatch *profile = ((AddressBookProfileMatch*)cell.profileData);
        
        if (profile.phones && 
            (profile.phones.count > 0) && 
            [MFMessageComposeViewController canSendText]) 
        {
            MFMessageComposeViewController* mcc = [[MFMessageComposeViewController alloc] init];
            mcc.body = smsBody;
            mcc.recipients = [NSArray arrayWithArray:profile.phones];
            [mcc setMessageComposeDelegate:self];
            [self.navigationController presentModalViewController:mcc animated:YES];
        } else 
            if (profile.emails &&
                (profile.emails.count > 0) &&
                [MFMailComposeViewController canSendMail]) 
            {
                MFMailComposeViewController *mailvc = [[MFMailComposeViewController alloc] init];
                [mailvc setSubject:@"Invitation"];
                [mailvc setToRecipients:profile.emails];
                [mailvc setTitle:@"Compose invitation"];
                [mailvc setMessageBody:emailMessageBody isHTML:YES];
                [mailvc setMailComposeDelegate:self];
                [self.navigationController presentModalViewController:mailvc animated:YES];
            } else {
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"Can't send an invitation to this contact" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [av show];
            }
    } else {
        [super actionPerformed:action inCell:cell];
    }
}

#pragma mark - MFMessage dialogs delegates
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    NSLog(@"sms send finished");
    [controller dismissModalViewControllerAnimated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSLog(@"email send finished");    
    [controller dismissModalViewControllerAnimated:YES];
}
@end

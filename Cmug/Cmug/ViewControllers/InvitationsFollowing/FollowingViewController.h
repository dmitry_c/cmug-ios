//
//  FollowingViewController.h
//  Cmug
//
//  Created by Dmitry C on 6/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FollowingPeopleHeaderView.h"
#import "AutoFetchTableView.h"

@interface FollowingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, FollowingPeopleHeaderViewDelegate, FollowingPeopleHeaderViewDataSource, AutoFetchTableViewDelegate>

@property (unsafe_unretained, nonatomic) IBOutlet AutoFetchTableView *tableView;

// A list of followed users.
@property (nonatomic, retain) NSMutableArray *usersList;
// Total number of followed users. May be greater then size of usersList.
@property (nonatomic, assign) NSInteger usersTotal;
@end

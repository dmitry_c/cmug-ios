//
//  BasePeopleViewController.m
//  Cmug
//
//  Created by Dmitry C on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BasePeopleViewController.h"
#import "TitleHeaderView.h"
#import "UserProfileCell.h"
#import "UITableView+LoadCell.h"
#import "UITableView+QuickScrollAdditions.h"
#import "FollowingService.h"

@interface BasePeopleViewController ()
@property (nonatomic, retain) NSMutableDictionary* titleHeaderViews;
- (UIView*)headerViewForSection:(NSInteger)section;
@end

@implementation BasePeopleViewController
@synthesize usersList = _usersList;
@synthesize matchedUsersList = _matchedUsersList;
@synthesize unmatchedUsersList = _unmatchedUsersList;
@synthesize titleHeaderViews = _titleHeaderViews;


- (NSMutableDictionary *)titleHeaderViews
{
    if (!_titleHeaderViews) {
        _titleHeaderViews = [NSMutableDictionary dictionaryWithCapacity:2];
    }
    return _titleHeaderViews;
}

- (UIView*)headerViewForSection:(NSInteger)section
{
    if ([self.titleHeaderViews objectForKey:[NSNumber numberWithInt:section]] != nil) {
        return [self.titleHeaderViews objectForKey:[NSNumber numberWithInt:section]];
    } else {
        UIView* titleView = [[TitleHeaderView alloc] initForTableView:self.tableView inSection:section];
        [CellSeparator decorateView:titleView withSeparatorStyle:CellSeparatorStyleLight];

        [self.titleHeaderViews setObject:titleView forKey:[NSNumber numberWithInt:section]];
        return titleView;
    }
}

- (void)setUsersList:(NSArray *)usersList
{
    if (_usersList != usersList) {
        _usersList = usersList;
        NSMutableArray *matchedProfiles = [[NSMutableArray alloc] initWithCapacity:_usersList.count];
        NSMutableArray *unmatchedProfiles = [[NSMutableArray alloc] initWithCapacity:_usersList.count];
        
        for (id contact in _usersList) {
            if ([contact respondsToSelector:@selector(profile)] && [contact valueForKey:@"profile"] != nil) {
                [matchedProfiles addObject:contact];
            } else {
                [unmatchedProfiles addObject:contact];
            }
        }
        _matchedUsersList = matchedProfiles;
        _unmatchedUsersList = unmatchedProfiles;
//        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
//        });
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:kBackgroundImageName]]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView setEditing:YES animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.tableView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) { // already at cmug
        return self.matchedUsersList.count;
    } else if (section == 1) {
        return self.unmatchedUsersList.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeaderHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CellHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [self headerViewForSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfileCell *profileCell = [tableView loadCellWithClass:UserProfileCell.class];
    
    if (indexPath.section == 0) { // already at cmug - "follow"
        id contact = [self.matchedUsersList objectAtIndex:indexPath.row];
        profileCell.cellAction = UserProfileCellAction_Follow;
        profileCell.delegate = self;
        ProfileShort *profileShort = (ProfileShort*)[contact valueForKey:@"profile"];
        profileCell.profileData = profileShort;
        
        [tableView setImageURL:profileShort.picture placeholderImage:[UIImage imageNamed:@"avatar"] forCellAtIndexPath:indexPath];
    } else if (indexPath.section == 1) {
        id contact = [self.unmatchedUsersList objectAtIndex:indexPath.row];
        profileCell.cellAction = UserProfileCellAction_Invite;
        profileCell.delegate = self;
        profileCell.profileData = contact;
        NSDictionary *profileData = profileCell.adoptedProfileData;
        
        if (![[profileData objectForKey:@"pictureData"] isMemberOfClass:NSNull.class]) {
            [tableView setImage:[profileData objectForKey:@"pictureData"] placeholderImage:[UIImage imageNamed:@"avatar"] forCellAtIndexPath:indexPath];
            
        } else if ([profileData objectForKey:@"picture"]) {
            [tableView setImageURL:[profileData objectForKey:@"picture"] placeholderImage:[UIImage imageNamed:@"avatar"] forCellAtIndexPath:indexPath];
        }
    }

    profileCell.textLabel.textColor = [UIColor blackColor];
    profileCell.selectionStyle = UITableViewCellSelectionStyleGray;
    [profileCell updateAccesoriesIfNeeded];
    return [CellSeparator decorateCell:profileCell withSeparatorStyle:CellSeparatorStyleLight withHeight:CellHeight];
}

# pragma mark - UserProfile Cell Actions
- (void)actionPerformed:(UserProfileCellAction)action inCell:(UserProfileCell*)cell
{
    if ((action == UserProfileCellAction_Follow) ||
        (action == UserProfileCellAction_Unfollow))
    {
        NSString* userId = [cell.profileData valueForKeyPath:@"userId"];
        NSLog(@"Action for user: %@", userId);

        cell.actionButton.selected = YES;
        cell.actionButton.enabled = NO;

        if (action == UserProfileCellAction_Follow) {
            [[FollowingService new] followUserWithId: userId
                                           onSuccess:^(id responseData) {
                                               cell.actionButton.selected = NO;
                                               cell.actionButton.enabled = YES;

                                               // TODO: mark profile as invited?
                                               [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject: [self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationFade];
                                           } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                                               NSLog(@"Follow failed with error response: %@", errorResponse);
                                               cell.actionButton.selected = NO;
                                               cell.actionButton.enabled = YES;
                                           }];


        }
        
        if (action == UserProfileCellAction_Unfollow) {
            [[FollowingService new] unfollowUserWithId:userId
                                             onSuccess:^(id responseData) {
                                                 cell.actionButton.selected = NO;
                                                 cell.actionButton.enabled = YES;
                                             } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                                                 NSLog(@"Unfollow failed with error response: %@", errorResponse);
                                                 cell.actionButton.selected = NO;
                                                 cell.actionButton.enabled = YES;
                                             }];
            
        }
    }

}

@end

//
//  InviteOptionsViewController.h
//  Cmug
//
//  Created by Dmitry C on 6/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlexibleViewController.h"
#import "UITableView+LoadCell.h"

@interface InviteOptionsViewController : FlexibleViewController <UITableViewDelegate,  UITableViewDataSource>

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *selectionTableView;

@end

//
//  ContactsFromAddressBookViewController.h
//  Cmug
//
//  Created by Dmitry C on 6/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePeopleViewController.h"

@interface ContactsFromAddressBookViewController : BasePeopleViewController <UITableViewDataSource, UITableViewDelegate>

@end

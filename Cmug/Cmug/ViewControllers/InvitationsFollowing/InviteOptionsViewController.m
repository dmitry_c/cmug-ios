//
//  InviteOptionsViewController.m
//  Cmug
//
//  Created by Dmitry C on 6/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "InviteOptionsViewController.h"
#import "AddressBookWrapper.h"
#import "FacebookWrapper.h"
#import "TwitterWrapper.h"
#import "FollowingService.h"
#import "TableCellAccessories.h"

#import "ContactsFromAddressBookViewController.h"
#import "FacebookFriendsViewController.h"
#import "TwitterFriendsViewController.h"

#define TAG_CONTACTS 1
#define TAG_FACEBOOK 2
#define TAG_TWITTER 3

@interface InviteOptionsViewController ()
@property (nonatomic, retain) NSArray* usersList;

@property (nonatomic, retain) FacebookWrapper* fb;
@property (nonatomic, retain) TwitterWrapper* tw;
@end

@implementation InviteOptionsViewController
@synthesize selectionTableView = _selectionTableView;
@synthesize usersList=_usersList;
@synthesize fb=_fb, tw=_tw;

- (FacebookWrapper *)fb
{
    if (!_fb) {
        _fb = [FacebookWrapper new];
    }
   return _fb;
}

- (TwitterWrapper *)tw 
{
    if (!_tw) {
        _tw = [TwitterWrapper new];
    }
    return _tw;
}

- (void) doOptionSelected:(NSInteger) index
{
    switch (index) {
        case TAG_CONTACTS: {
            NSArray *contacts = [NSArray arrayWithArray: [AddressBookWrapper contacts]];
            if ([contacts count] == 0) {
                NSLog(@"TODO: message: no contacts");
                return;
            }
            [[FollowingService new] matchProfiles:contacts fromSource:(ProfileMatchSource)TAG_CONTACTS onSuccess:^(id responseData) {
                
                self.usersList = ((MatchResponse*)responseData).matches;
                // push Contacts list view with self.usersList data
                ContactsFromAddressBookViewController *cabViewController = [[ContactsFromAddressBookViewController alloc] init];
                cabViewController.usersList = self.usersList;
                [self.navigationController pushViewController:cabViewController animated:YES];
                
            } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                NSLog(@"TODO: show error");
            }];
            NSLog(@"%@", contacts);
            break;
        }
            
        case TAG_FACEBOOK: {
            // Fetch facebook Friends
            [self.fb friends:^(NSArray *friendsList) {
                if ([friendsList count] == 0) {
                    NSLog(@"TODO: message: no friends");
                    return;
                }
                
                // on success post friends array to the server
                [[FollowingService new] matchProfiles:friendsList fromSource:(ProfileMatchSource)TAG_FACEBOOK onSuccess:^(id responseData) {
                    
                    self.usersList = ((MatchResponse*)responseData).matches;

                    FacebookFriendsViewController *fbViewController = [[FacebookFriendsViewController alloc] init];
                    fbViewController.usersList = self.usersList;
                    [self.navigationController pushViewController:fbViewController animated:YES];
                    
                } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                    NSLog(@"TODO: show error");
                }];
                
            } onFailure:^(NSError *error) {
                NSLog(@"Can't fetch friends: error %@", error);
                
            }];
            
            
            break;
        }
            
        case TAG_TWITTER: {
            [self.tw friendsWithInfo:YES andDelegate:nil onSuccess:^(NSArray *usersList) {
                if ([usersList count] == 0) {
                    NSLog(@"TODO: message: no followers");
                    return;
                }

                NSArray *userIdsList = [usersList valueForKey:@"id_str"];
                NSLog(@"Users: %@\n\n Ids: %@", usersList, userIdsList);
                
                [[FollowingService new] matchProfiles:userIdsList fromSource:(ProfileMatchSource)TAG_TWITTER onSuccess:^(id responseData) {
                    
                    self.usersList = ((MatchResponse*)responseData).matches;
                    NSDictionary *usersDict = [NSDictionary dictionaryWithObjects:usersList forKeys: userIdsList];
                    for (TwitterProfileMatch* profile in self.usersList) {
                        NSDictionary *twitterData = [usersDict objectForKey:profile.twitterId];
                        profile.twitterName = [twitterData valueForKey:@"name"];
                        profile.twitterScreenName = [twitterData valueForKey:@"screen_name"];
                        profile.twitterImageUrl = [twitterData valueForKey:@"profile_image_url"];
                    }
                    
                    TwitterFriendsViewController *twViewController = [[TwitterFriendsViewController alloc] init];
                    twViewController.usersList = self.usersList;
                    [self.navigationController pushViewController:twViewController animated:YES];
                    
                } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                    NSLog(@"TODO: show error");
                }];
            } onFailure:^(NSError *error) {
                NSLog(@"Can't fetch friends: error %@", error); 
                
            }];
            break;
        }
            
        default:
            break;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectionTableView.backgroundColor = [UIColor clearColor];
    self.navigationItem.title = @"Follow";
}

- (void)viewDidUnload
{
    [self setSelectionTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67.0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return nil;
    } else {
        return @"Suggestions";
    };
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger tag = indexPath.row + 1;
    [self doOptionSelected:tag];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of users for the second section
    if (section == 0)
        return 3;
    else
        return 0; // no suggestions
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"IDInviteOptionCell";
//    UITableViewCell* cell = [tableView loadCellNamed:@"InviteOptionCell"];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.accessoryView = [TableCellAccessories greyArrowAccessory];

    NSInteger tag = indexPath.row + 1;
    switch (tag) {
        case TAG_CONTACTS:
            cell.imageView.image = [UIImage imageNamed:@"invite_address_book"];
            cell.textLabel.text = @"Contacts";
            cell.detailTextLabel.text = @"Follow people from your contacts.";
            break;
        case TAG_FACEBOOK:
            cell.imageView.image = [UIImage imageNamed:@"invite_facebook"];
            cell.textLabel.text = @"Facebook";
            cell.detailTextLabel.text = @"Follow your friends from Facebook.";
            break;
        case TAG_TWITTER:
            cell.imageView.image = [UIImage imageNamed:@"invite_twitter"];
            cell.textLabel.text = @"Twitter";
            cell.detailTextLabel.text = @"Follow your friends from Twitter.";
            break;
        default:
            break;
    }
    return cell;
}


@end

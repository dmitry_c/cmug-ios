//
//  TwitterFriendsViewController.m
//  Cmug
//
//  Created by Dmitry C on 6/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TwitterFriendsViewController.h"
#import "ProfileMatch.h"
#import "UserProfileCell.h"
#import "UITableView+LoadCell.h"
#import "TwitterWrapper.h"

@interface TwitterFriendsViewController () <UserProfileActionProtocol>
@property (nonatomic, retain) TwitterWrapper* twitterWrapper;
@end

@implementation TwitterFriendsViewController

@synthesize twitterWrapper = _twitterWrapper;

- (TwitterWrapper *)twitterWrapper
{
    if (!_twitterWrapper) {
        _twitterWrapper = [[TwitterWrapper alloc] init];
    }
    return _twitterWrapper;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setEditing:YES animated:YES];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - TableView dataSource & delegate

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Twitter friends on Cmug.";
    } else if (section == 1) {
        return @"Invite Twitter Friends";
    }
    return nil;
}

# pragma mark - UserProfile Cell Actions
- (void)actionPerformed:(UserProfileCellAction)action inCell:(UserProfileCell *)cell
{
    if (action == UserProfileCellAction_Invite) {
        NSString *screenName = ((TwitterProfileMatch*)cell.profileData).twitterScreenName;
        [self.twitterWrapper sendDirectMessage:@"TODO: twitter account invitation" to:screenName];
    } else {
        [super actionPerformed:action inCell:cell];
    }

}

@end

//
//  BasePeopleViewController.h
//  Cmug
//
//  Created by Dmitry C on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellSeparator.h"
#import "UserProfileCell.h"

#define kBackgroundImageName @"bg_textured_light_blue.png"

#define HeaderHeight 40.0f
#define CellHeight 60.0f

@interface BasePeopleViewController : UITableViewController <UserProfileActionProtocol>
//@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;
// table data source
@property (nonatomic, retain) NSArray* usersList;  // all users
@property (nonatomic, retain) NSArray* matchedUsersList;  // already registered - to be show in the top table with 'follow' option
@property (nonatomic, retain) NSArray* unmatchedUsersList;  // users not found in the system - to be shown with 'invite' option
@end

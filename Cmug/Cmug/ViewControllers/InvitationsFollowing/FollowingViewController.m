//
//  FollowingViewController.m
//  Cmug
//
//  Created by Dmitry C on 6/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FollowingViewController.h"
#import "FollowingService.h"
#import "UserProfileCell.h"
#import "InviteOptionsViewController.h"
#import "TableCellAccessories.h"
#import "CellSeparator.h"
#import "UITableView+QuickScrollAdditions.h"

@interface FollowingViewController () <UserProfileActionProtocol>
@property (nonatomic, retain) FollowingResponse* lastResponse;
@property (nonatomic, retain) FollowingPeopleHeaderView* headerView;
@end

@implementation FollowingViewController
@synthesize tableView = _tableView;

@synthesize usersList=_usersList;
@synthesize usersTotal;
@synthesize headerView=_headerView;
@synthesize lastResponse;

- (NSMutableArray *)usersList
{
    if (!_usersList) {
        _usersList = [NSMutableArray arrayWithCapacity:10];
    }
    return _usersList;
}

- (UIView *)headerView
{
    if (!_headerView) {
        _headerView = [FollowingPeopleHeaderView loadFromNib];
        _headerView.delegate = self;
        _headerView.dataSource = self;
        // place button
        float width = _headerView.frame.size.width;
        float height = _headerView.frame.size.height;
        float buttonWidth = 60.0f;
        float buttonHeight = 30.0f;
        float rightMargin = 10.0f;
        
        [_headerView.actionButton setFrame:CGRectMake(width - rightMargin - buttonWidth, (height - buttonHeight) * 0.5f, buttonWidth, buttonHeight)];

        _headerView = [CellSeparator decorateView:_headerView withSeparatorStyle:CellSeparatorStyleDark];
    }
    return _headerView;
}

#pragma mark - Header Delegate & Data
- (NSInteger)followingPeopleHeaderViewPeopleCount
{
//    return self.usersList.count;
    return self.usersTotal;
}

-(void)followingPeopleHeaderView:(FollowingPeopleHeaderView *)headerView didButtonAction:(UIButton*)sender
{
    [self.tableView setEditing: sender.selected animated: YES];
}

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadFollowedUsersList
{
    [self.tableView setState:AutoFetchTableViewStateScrolledToBottom];
    [self.tableView setState:AutoFetchTableViewStateLoading];
    NSInteger limit = 20;
    NSInteger offset = 0;
    
    if (self.lastResponse) {
        offset = self.lastResponse.offset + limit;
    }
    
    [[FollowingService new] loadFollowedUsersWithLimit:limit andOffset:offset
                                             onSuccess:^(FollowingResponse* responseData) {
                                                 NSLog(@"%@", responseData);
                                                 //reload table
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [_usersList addObjectsFromArray:responseData.usersList];
                                                     usersTotal = responseData.total;
                                                     self.lastResponse = responseData;
                                                     [self.tableView reloadData];
                                                     [self.headerView reloadData];
                                                     [self.tableView performSelector:@selector(autoFetchTriggerEndLoading:) withObject:self.lastResponse];
                                                 });
                                             } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                                                 NSLog(@"%@", error);
                                                 [self.tableView performSelector:@selector(autoFetchTriggerEndLoading:) withObject:nil];
                                             }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self loadFollowedUsersList];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_textured_dark"]]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.autoFetchDelegate = self;
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of users for the second section
    if (section==0) { //Follow button
        return 1;
    }
    return self.usersList.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return self.headerView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0.0f;
    } else {
        return 40.0f;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 48.0f; // "follow" cell
    } else {
        return 60.0f; // "profile" cell
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"IDFollowSelectCell";
    UITableViewCell *cell = nil;
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            
            cell.textLabel.text = @"Follow";
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor clearColor];
            cell.accessoryView = [TableCellAccessories greyArrowAccessory];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
    } else if (indexPath.section == 1) {
        UserProfileCell *profileCell = [tableView loadCellWithClass:UserProfileCell.class];
        profileCell.cellAction = UserProfileCellAction_Unfollow;
        
        ProfileShort* profile = [self.usersList objectAtIndex:indexPath.row];
        profileCell.delegate = self;
        profileCell.profileData = profile;
        profileCell.textLabel.textColor = [UIColor whiteColor];
        profileCell.selectionStyle = UITableViewCellSelectionStyleGray;
        [profileCell updateAccesoriesIfNeeded];
        [tableView setImageURL:profile.picture forCellAtIndexPath:indexPath];
        
        return [CellSeparator decorateCell:profileCell withSeparatorStyle:CellSeparatorStyleDark withHeight:[self tableView:tableView heightForRowAtIndexPath:indexPath]]; 
    }
    return [CellSeparator decorateCell:cell withSeparatorStyle:CellSeparatorStyleDark withHeight:[self tableView:tableView heightForRowAtIndexPath:indexPath]];
}

#pragma mark - UserProfile Cell Action Protocol
- (void)actionPerformed:(UserProfileCellAction)action inCell:(UserProfileCell *)cell
{
    if (action == UserProfileCellAction_Unfollow) {
        NSString* userId = ((ProfileShort*)cell.profileData).userId;
        [[FollowingService new] unfollowUserWithId:userId
                                         onSuccess:^(id responseData) {
                                             cell.actionButton.selected = NO;
                                             cell.actionButton.enabled = YES;
                                             NSIndexSet* indexes = [self.usersList indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                                                 if (((ProfileShort*)obj).userId == userId) 
                                                 {
                                                     *stop = YES;
                                                     return YES;
                                                 }
                                                 return NO;
                                             }];
                                             
                                             NSMutableArray* mutableUsersList = [self.usersList mutableCopy];
                                             [mutableUsersList removeObjectsAtIndexes:indexes];
                                             _usersList = [NSArray arrayWithArray:mutableUsersList];
                                             self.usersTotal -= 1;
                                             [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[self.tableView indexPathForCell:cell]]
                                                                   withRowAnimation:UITableViewScrollPositionMiddle];
                                             [self.headerView reloadData];
                                             
                                         } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                                             NSLog(@"Unfollow failed with error response: %@", errorResponse);
                                             cell.actionButton.selected = NO;
                                             cell.actionButton.enabled = YES;
                                         }];
    }
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    if (indexPath.section == 1) {
        return YES;
    }
    return NO;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        // Cancel the delete button if we are in swipe to edit mode
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell.editing && !self.editing)
        {
            [cell setEditing:NO animated:YES];
            return;
        }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        InviteOptionsViewController *inviteOptionsViewController = [[InviteOptionsViewController alloc] initWithNibName:@"InviteOptionsViewController" bundle:nil];
        
        [self.navigationController pushViewController:inviteOptionsViewController animated:YES];
    } else {
        ProfileShort *profile = [self.usersList objectAtIndex:indexPath.row];
        NSLog(@"Show profile details view for user: %@", profile);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    } else {
        return 40.0f;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [self.tableView tableView:tableView viewForFooterInSection:section];
}

#pragma mark - Auto Fetch
- (void)autoFetchTableViewDidStart:(AutoFetchTableView *)tableView
{
    [self loadFollowedUsersList];
}

- (BOOL)autoFetchTableViewShouldStart:(AutoFetchTableView *)tableView
{
    return (self.usersList.count < self.usersTotal);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tableView scrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self.tableView scrollViewDidEndDecelerating:scrollView];    
}

@end

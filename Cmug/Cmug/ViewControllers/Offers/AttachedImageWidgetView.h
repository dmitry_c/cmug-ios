//
//  AttachedImageWidgetView.h
//  Cmug
//
//  Created by Dmitry C on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreateOfferRequest.h"

@interface AttachedImageWidgetView : UIView
@property (nonatomic, retain) UIImageView* imageView;
@property (nonatomic, retain) UIButton* removeButton;

@property (unsafe_unretained, nonatomic) CreateOfferRequest* offerData;
@end

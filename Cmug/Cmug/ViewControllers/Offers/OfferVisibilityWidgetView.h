//
//  OfferVisibilityWidgetView.h
//  Cmug
//
//  Created by Dmitry C on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Offer.h"
#import "CreateOfferRequest.h"

@interface OfferVisibilityWidgetView : UIView
@property (nonatomic, assign) OfferVisibilityType offerVisibility;
@property (nonatomic, retain) NSArray* usersList;

@property (nonatomic, assign) BOOL showUsersList;

@property (nonatomic, unsafe_unretained) CreateOfferRequest *offerData;
- (void)setupButtonHandlerForTarget:(id)target action:(SEL)action;
@end

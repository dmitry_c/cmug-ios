//
//  BeAnonymousWidgetView.h
//  Cmug
//
//  Created by Dmitry C on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreateOfferRequest.h"

@interface BeAnonymousWidgetView : UIView
@property (nonatomic, retain) UIButton* anonymousCheckButton;

@property (nonatomic, unsafe_unretained) CreateOfferRequest* offerData;
@end

//
//  HashTagCell.h
//  Cmug
//
//  Created by Dmitry C on 7/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HashTagCell : UITableViewCell
{
    UILabel *label;
}
@property (nonatomic, copy) NSString* tagText;
@end

//
//  CreateOfferViewController.h
//  Cmug
//
//  Created by Dmitry C on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlexibleViewController.h"
#import "CreateOfferRequest.h"

typedef enum {
    kDefaultView = 0,
    kShowAttachment = 1 << 1,
    kShowAttachmentMenu = 1 << 2
} CreateOfferViewMode;

@interface CreateOfferViewController : FlexibleViewController <UITableViewDataSource, UITableViewDelegate>
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, retain) CreateOfferRequest *offerData;
@end

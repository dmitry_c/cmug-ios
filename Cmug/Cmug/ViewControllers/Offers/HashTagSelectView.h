//
//  HashTagSelectView.h
//  Cmug
//
//  Created by Dmitry C on 7/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSelectProtocol.h"

@interface HashTagSelectView : UIView <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (nonatomic, retain) UITextField* textField;
@property (nonatomic, retain) UITableView* tableView;

@property (nonatomic, retain) id<DataSelectProtocol> dataSelectDelegate;

- (void)showAnimated;
- (void)hideAnimated;
- (void)doHashTagSelect;
@end

//
//  CreateOfferViewController.m
//  Cmug
//
//  Created by Dmitry C on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CreateOfferViewController.h"
#import "UIView+RoundedBorderWithShadow.h"
#import "VisibilitySelectViewController.h"
#import "HashTagSelectView.h"
#import "NSString+Additions.h"
#import "IIViewDeckController.h"
#import "NavigationManager.h"
#import "OfferDetailsWidgetView.h"
#import "OfferTypeSelectWidgetView.h"
#import "OfferVisibilityWidgetView.h"
#import "OfferService.h"
#import "AttachedImageWidgetView.h"
#import "AttachOptionsView.h"
#import "CellSeparator.h"
#import "FacebookShareWidgetView.h"
#import "TwitterShareWidgetView.h"
#import "BeAnonymousWidgetView.h"
#import "CameraViewController.h"

typedef enum {
    kAttachedImageRow = 0,
    kDetailsRow = 1,
    kTypeRow = 2,
    kVisibilityRow = 3,
    kFacebookShareRow = 4,
    kTwitterShareRow = 5,
    kAnonymousRow = 6,
    kAttachmentOptionsRow = 7
} TableRowIndexes;

@interface CreateOfferViewController () <DataSelectProtocol>
{
    NSIndexPath *visibilityIndexPath;
}
@property (nonatomic, retain) HashTagSelectView* hashTagSelectView;
@property (nonatomic, retain) OfferDetailsWidgetView *topView;
@property (nonatomic, retain) OfferTypeSelectWidgetView *offerTypeSelectView;
@property (nonatomic, retain) OfferVisibilityWidgetView *offerVisibilityView;
@property (nonatomic, retain) AttachedImageWidgetView *attachedImageView;
@property (nonatomic, retain) AttachOptionsView *attachOptionsView;
@property (nonatomic, retain) FacebookShareWidgetView *facebookShareView;
@property (nonatomic, retain) TwitterShareWidgetView *twitterShareView;
@property (nonatomic, retain) BeAnonymousWidgetView *beAnonymousWidgetView;


@property (nonatomic, assign) NSUInteger viewMode;
@end


@implementation CreateOfferViewController
@synthesize tableView = _tableView;
@synthesize hashTagSelectView = _hashTagSelectView;
@synthesize topView = _topView;
@synthesize offerTypeSelectView = _offerTypeSelectView;
@synthesize offerVisibilityView = _offerVisibilityView;
@synthesize attachedImageView = _attachedImageView;
@synthesize attachOptionsView = _attachOptionsView;
@synthesize facebookShareView = _facebookShareView;
@synthesize twitterShareView = _twitterShareView;
@synthesize beAnonymousWidgetView = _beAnonymousWidgetView;
@synthesize viewMode = _viewMode;
// Data
@synthesize offerData=_offerData;


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqual:@"frame"]) {
        // reload visibility cell to recalculate its size
        [self.tableView reloadData];
//        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:visibilityIndexPath] withRowAnimation:UITableViewRowAnimationMiddle];
    }
    
    if ([keyPath isEqual:@"sharing"]) {
        NSLog(@"%d", self.offerData.sharing);
    }
}

- (AttachedImageWidgetView *)attachedImageView
{
    if (!_attachedImageView) {
        _attachedImageView = [[AttachedImageWidgetView alloc] initWithFrame:CGRectMake(0, 0, 320, 60.0f)];
        _attachedImageView.offerData = self.offerData;
    }    
    return _attachedImageView;
}

- (OfferDetailsWidgetView *)topView
{
    if (!_topView) {
        _topView = [[OfferDetailsWidgetView alloc] initWithFrame: CGRectMake(10, 10, 300, 170)];
        _topView.offerTitle.delegate = self;
        _topView.offerDetails.delegate = self;
        
        _topView.offerData = self.offerData;

        [_topView.hashButton addTarget:self action:@selector(showHashTagSelectViewAction:) forControlEvents:UIControlEventTouchUpInside];
        [_topView.attachButton addTarget:self action:@selector(showAttachOptionsAction:) forControlEvents:UIControlEventTouchUpInside];

    }
    return _topView;
}

- (OfferTypeSelectWidgetView *)offerTypeSelectView
{
    if (!_offerTypeSelectView) {
        _offerTypeSelectView = [[OfferTypeSelectWidgetView alloc] initWithFrame:CGRectMake(0, 0, 320, 41)];
        
        _offerTypeSelectView.offerData = self.offerData;
    }
    return _offerTypeSelectView;
}

- (AttachOptionsView *)attachOptionsView
{
    if (!_attachOptionsView) {
        _attachOptionsView = [[AttachOptionsView alloc] initWithFrame:CGRectMake(0, 0, 320, 100.0f)];
        [_attachOptionsView.fromCameraButton addTarget:self action:@selector(showCameraView:) forControlEvents:UIControlEventTouchUpInside];
        [_attachOptionsView.fromLibraryButton addTarget:self action:@selector(showCameraView:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _attachOptionsView;
}

- (OfferVisibilityWidgetView *)offerVisibilityView
{
    if (!_offerVisibilityView) {
        _offerVisibilityView = [[OfferVisibilityWidgetView alloc] initWithFrame:CGRectMake(0, 10, 320, 105)];
        _offerVisibilityView.offerData = self.offerData;
        // Setup Visibility widget
        [_offerVisibilityView setupButtonHandlerForTarget:self action:@selector(showVisibilityDialogAction:)];
        // Setup observer for frame size change
        [_offerVisibilityView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
    }    
    return _offerVisibilityView;
}

- (FacebookShareWidgetView *)facebookShareView
{
    if(!_facebookShareView) {
        _facebookShareView = [[FacebookShareWidgetView alloc] initWithFrame:CGRectMake(0, 0, 320, 60.0f)];
        _facebookShareView.offerData = self.offerData;
    }
    return _facebookShareView;
}

- (TwitterShareWidgetView *)twitterShareView
{
    if (!_twitterShareView) {
        _twitterShareView = [[TwitterShareWidgetView alloc] initWithFrame:CGRectMake(0, 0, 320, 60.0f)];
        _twitterShareView.offerData = self.offerData;
    }
    return _twitterShareView;
}

- (BeAnonymousWidgetView *)beAnonymousWidgetView
{
    if (!_beAnonymousWidgetView) {
        _beAnonymousWidgetView = [[BeAnonymousWidgetView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        _beAnonymousWidgetView.offerData = self.offerData;
    }
    return _beAnonymousWidgetView;
}

- (HashTagSelectView *)hashTagSelectView
{
    if (!_hashTagSelectView) {
        CGRect hashViewFrame = self.topView.frame;
        hashViewFrame.size.height = self.view.frame.size.height - keyboardSize.size.height - self.topView.frame.origin.y;
        _hashTagSelectView = [[HashTagSelectView alloc] initWithFrame:hashViewFrame];
        _hashTagSelectView.dataSelectDelegate = self;
        _hashTagSelectView.hidden = YES; // do not show this when created
    }
    return _hashTagSelectView;
}

- (void)setViewMode:(NSUInteger)viewMode
{
    _viewMode = viewMode;
    [self.tableView reloadData];
}

#pragma mark - Data Select Protocol Implementation
- (void)selectDataSource:(id)source returnedData:(id)data
{
    if ([source isMemberOfClass:HashTagSelectView.class]) {
        HashTagSelectView *view = (HashTagSelectView*)source;
        NSString *tag = (NSString*)data;
        [view hideAnimated];
        // show the offer deails view and focus offer description field
        self.topView.hidden = NO;
        [self.topView.offerDetails becomeFirstResponder];
        
        // update offer details with selected tag
        // if it's not empty
        if (tag != nil) {
            NSString *trimmedTag = [tag stringFromCleanedTag];
            if (!([trimmedTag isEqual:@""])) {
                self.topView.offerDetails.text = [NSString stringWithFormat: @"%@ #%@", self.topView.offerDetails.text, trimmedTag];
            }
        }
    } else if ([source isMemberOfClass:VisibilitySelectViewController.class]) {
        OfferVisibilityType visibilityType = (OfferVisibilityType)[(NSNumber*)[((NSDictionary*)data) objectForKey:kOfferVisibilityType] intValue];
        NSArray *usersList = (NSArray*)[((NSDictionary*)data) objectForKey:kUsersList];
        self.offerData.visibility = visibilityType;
        self.offerData.usersList = usersList;
    }
}


- (CreateOfferRequest*)offerData
{
    if (!_offerData) {
        _offerData = [CreateOfferRequest new];
        _offerData.visibility = kOfferVisibilityType_Public;
        _offerData.usersList = nil;
        _offerData.requestType = kOfferRequestType_Text;
        _offerData.sharing = kOfferShareType_None;
        _offerData.title = nil;
        _offerData.description = nil;
        _offerData.isAnonymous = NO;
        [_offerData addObserver:self forKeyPath:@"sharing" options:NSKeyValueObservingOptionNew context:nil];
    }
    return _offerData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.requiresAuthorization = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.navigationItem.rightBarButtonItem) {
        
        UIBarButtonItem * doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Post" 
                                                                        style:UIBarButtonItemStylePlain 
                                                                       target:self 
                                                                       action:@selector(postCreateOffer)];
        self.navigationItem.rightBarButtonItem = doneButton;
    }
    

    [self.view addSubview:self.hashTagSelectView];
    [self.hashTagSelectView hideAnimated];

    self.viewMode = kDefaultView;
    self.tableView.scrollsToTop = NO;
    self.tableView.scrollEnabled = YES;
    
    self.allowFreeScrolling = YES;
    self.allowEditingScrolling = YES;
}

- (void)viewDidUnload
{
    [self.offerVisibilityView removeObserver:self forKeyPath:@"frame"];
    [self.offerData removeObserver:self forKeyPath:@"sharing"];
    
    [self setHashTagSelectView:nil];
    [self setTableView:nil];
    [self setTopView:nil];
    [self setOfferTypeSelectView:nil];
    [self setOfferVisibilityView:nil];
    [self setBeAnonymousWidgetView:nil];
    [self setAttachedImageView:nil];
    [self setAttachOptionsView:nil];
    [super viewDidUnload];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Create Request";    
}

#pragma mark - Table View DataSource & Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *cellIdentifiers = [NSArray arrayWithObjects:
                                @"IDAttachedImage", 
                                @"IDOfferDetails",
                                @"IDOfferTypeSelect",
                                @"IDOfferVisibility",
                                @"IDFacebookShare",
                                @"IDTwitterShare",
                                @"IDAnonymous",
                                @"IDAttachOptions",
                                nil];
    
    NSString* reuseIdentifier = [cellIdentifiers objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    CGFloat cellHeight = [self tableView:tableView heightForRowAtIndexPath:indexPath];
    
    if (!cell) {
        UITableViewCellStyle cellStyle = UITableViewCellStyleDefault;
//        if (indexPath.row == 4 || indexPath.row == 5) {
//            cellStyle = UITableViewCellStyleSubtitle;
//        }
        cell = [[UITableViewCell alloc] initWithStyle:cellStyle reuseIdentifier:reuseIdentifier];
        switch (indexPath.row) {
            case kAttachedImageRow:
                [cell.contentView addSubview:self.attachedImageView];
                break;
            case kDetailsRow:
                [cell.contentView addSubview:self.topView];
                break;
            case kTypeRow:
                [cell.contentView addSubview:self.offerTypeSelectView];
                break;
            case kVisibilityRow:
                [cell.contentView addSubview:self.offerVisibilityView];
                break;
            case kFacebookShareRow:
                // facebook
                [cell.contentView addSubview:self.facebookShareView];
//                cell.imageView.image = [UIImage imageNamed:@"share_facebook"];
//                cell.detailTextLabel.text = @"Share with Facebook";
//                cell.textLabel.text = @"Facebook";
                [CellSeparator decorateCell:cell withSeparatorStyle:CellSeparatorStyleLight withHeight:60.0f];
                break;
            case kTwitterShareRow:
                [cell.contentView addSubview:self.twitterShareView];
                [CellSeparator decorateCell:cell withSeparatorStyle:CellSeparatorStyleLight withHeight:60.0f];
                break;
            case kAnonymousRow:
                //anonymous
                [cell.contentView addSubview:self.beAnonymousWidgetView];
                break;
            case kAttachmentOptionsRow:
                [cell.contentView addSubview:self.attachOptionsView];
            default:
                break;
        }

    }
    
    if (cellHeight == 0.0f) {
        cell.contentView.hidden = YES;
    } else {
        cell.contentView.hidden = NO;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat detailsCellHeight = self.topView.frame.size.height + self.topView.frame.origin.y + 6.0f; // 6.0f for shadow
    CGFloat offerTypeHeight = self.offerTypeSelectView.frame.size.height + self.offerTypeSelectView.frame.origin.y;
    CGFloat offerVisibilityHeight = self.offerVisibilityView.frame.size.height + self.offerVisibilityView.frame.origin.y;
    CGFloat attachmentMenuOptionsHeight = self.attachOptionsView.frame.size.height + self.attachOptionsView.frame.origin.y;
;
    
    NSArray *cellHeights = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat: (_viewMode & kShowAttachment) ? 60.0f : 0.0f],  // attachment cell
                            [NSNumber numberWithFloat: detailsCellHeight],  // top view
                            [NSNumber numberWithFloat: (_viewMode & kShowAttachmentMenu) ? 0.0f : offerTypeHeight],  // offer type widget
                            [NSNumber numberWithFloat: (_viewMode & kShowAttachmentMenu) ? 0.0f : offerVisibilityHeight],  // visibility widget
                            [NSNumber numberWithFloat: (_viewMode & kShowAttachmentMenu) ? 0.0f : 60.0f],  // facebook
                            [NSNumber numberWithFloat: (_viewMode & kShowAttachmentMenu) ? 0.0f : 60.0f],  // twitter
                            [NSNumber numberWithFloat: (_viewMode & kShowAttachmentMenu) ? 0.0f : 60.0f],  // is anonymous
                            [NSNumber numberWithFloat: (_viewMode & kShowAttachmentMenu) ? attachmentMenuOptionsHeight : 0.0f],  // attachment menu
                            nil];
    CGFloat result = [[cellHeights objectAtIndex:indexPath.row] floatValue];

    return result;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.row == kFacebookShareRow) {
        // Toggle Facebook share option if offer is not anonymous
        if (!self.offerData.isAnonymous)
            self.offerData.sharing ^= kOfferShareType_Facebook;
    } else if (indexPath.row == kTwitterShareRow) {
        // Toggle Twitter share option if offer is not anonymous
        if (!self.offerData.isAnonymous)
            self.offerData.sharing ^= kOfferShareType_Twitter;        
    } else if (indexPath.row == kAnonymousRow) {
        self.offerData.isAnonymous = !self.offerData.isAnonymous;
        // Update social share buttons
        self.facebookShareView.disabled = self.offerData.isAnonymous;
        self.twitterShareView.disabled = self.offerData.isAnonymous;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - "Done" right navigation button
- (BOOL)shouldAddTextFieldDoneButton
{
    return YES;    
}

- (void)resignActiveViewElement
{
	[activeViewElement resignFirstResponder];
    if (self.hashTagSelectView.hidden == NO) {
        [self.hashTagSelectView doHashTagSelect];
    }
}


#pragma mark - Actions

- (IBAction)showHashTagSelectViewAction:(UIButton *)sender 
{
    [self.hashTagSelectView showAnimated];
    self.topView.hidden = YES;
}

- (IBAction)showAttachOptionsAction:(UIButton *)sender 
{
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.viewMode = self.viewMode | kShowAttachmentMenu;
    } else {
        self.viewMode = self.viewMode & ~kShowAttachmentMenu;
    }

}

- (IBAction)showCameraView:(UIButton *)sender 
{
    NSLog(@"TODO:%d", sender.tag);
    CameraViewController *vc = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
    if (sender.tag == kFromLibraryButtonTag) {
        NSLog(@"TODO: open library");
    } else if (sender.tag == kFromCameraButtonTag) {
        NSLog(@"TODO: start camera");
    }
}

- (IBAction)showVisibilityDialogAction:(UIButton *)sender 
{
    VisibilitySelectViewController *vc = [[VisibilitySelectViewController alloc] init];
    vc.dataSelectDelegate = self;
    vc.offerVisibilityType = self.offerData.visibility;
    if (self.offerData.usersList) {
        vc.selectedUsersList = [NSMutableArray arrayWithArray:self.offerData.usersList];
    }
    
    UINavigationController *nc = [[NavigationManager sharedManager] customizedNavigationController];
    nc.viewControllers = [NSArray arrayWithObject:vc];
    [self.navigationController presentModalViewController:nc animated:YES];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - 
- (void)collectData
{
    self.offerData.title = self.topView.offerTitle.text;
    self.offerData.description = self.topView.offerDetails.text;
    self.offerData.visibility = self.offerVisibilityView.offerVisibility;
    self.offerData.usersList = self.offerVisibilityView.usersList;
}

- (BOOL)validate
{
    if (!(self.offerData.title && self.offerData.title.length)) {
        // show alert
        return NO;
    }
    return YES;
}

- (void)postCreateOffer
{
    [activeViewElement resignFirstResponder];
    [self collectData];
    if (![self validate]) {
        NSLog(@"not valid");
        return;
    }
    
        [[OfferService new] createOffer:self.offerData onSuccess:^(id responseData) {
            
            NSLog(@"Offer Created");
            
        } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
            
            NSLog(@"Offer Creation Error %@", error);
        }];
}

@end

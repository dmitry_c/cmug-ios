//
//  TwitterShareWidgetView.m
//  Cmug
//
//  Created by Dmitry C on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TwitterShareWidgetView.h"
#import "Offer.h"
#import "TwitterWrapper.h"


@implementation TwitterShareWidgetView

- (void)updateShareCheckButton
{
    NSUInteger sharingOptions;
    [[self.offerData valueForKey:@"sharing"] getValue:&sharingOptions];
    
    // show share check button selected if Twitter is in share options
    self.shareCheckButton.selected = [[NSNumber numberWithInt:(sharingOptions & kOfferShareType_Twitter)] boolValue];
}

- (BOOL)isConfigured
{
    return [[TwitterWrapper get] isConfigured];
}

- (void)configureShare
{
    [[TwitterWrapper get] configureShare:^(NSDictionary *result) {
        if (result != nil) {
            NSString* screenName = [result objectForKey:@"screen_name"];
            self.descriptionLabel.text = [NSString stringWithFormat:@"@%@", screenName];
           [self updateButtons];
        } else {
            // Show configure error?
            NSLog(@"Twitter Error");
        }
    }];
}

- (void)updateOfferShareOption:(BOOL)selected
{
    // update offer data
    NSUInteger sharingOptions;
    [[self.offerData valueForKey:@"sharing"] getValue:&sharingOptions];
    if (selected) {
        sharingOptions |= kOfferShareType_Twitter;
    } else {
        sharingOptions &= ~kOfferShareType_Twitter;
    }
    [self.offerData setValue:[NSNumber numberWithInteger:sharingOptions] forKey:@"sharing"];
}

- (UIImage *)enabledImage
{
    return [UIImage imageNamed:kTwitterShareImageName];    
}

- (UIImage *)disabledImage
{
    return [UIImage imageNamed:kTwitterShareDisabledImageName];
}

- (void)setup
{
    [super setup];
    self.titleLabel.text = @"Twitter";

    if ([self isConfigured]) {
        [[TwitterWrapper get] screenName:^(NSString *result) {
            self.descriptionLabel.text = [NSString stringWithFormat:@"@%@", result];
        }];
    }
}
@end

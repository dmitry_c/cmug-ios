//
//  HashTagSelectView.m
//  Cmug
//
//  Created by Dmitry C on 7/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HashTagSelectView.h"
#import "UIView+RoundedBorderWithShadow.h"
#import "HashTagCell.h"

#define kHashImage @"hash"
#define CellHeight 30.0;


@interface HashTagSelectView()
@property (nonatomic, retain) NSArray* tagsList;
@property (nonatomic, assign) CGRect initialFrame;

- (void)loadTagsList;
@end

@implementation HashTagSelectView
@synthesize textField = _textField;
@synthesize tableView = _tableView;
@synthesize tagsList = _tagsList;
@synthesize initialFrame;
@synthesize dataSelectDelegate;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.initialFrame = frame;
        
        UIView *inputWrapper = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300.0f, 40.0f)];
        inputWrapper.backgroundColor = [UIColor whiteColor];
        [inputWrapper decorateWithRoundedBordersAndShadow];

        UIButton *hashButton = [[UIButton alloc] initWithFrame: CGRectMake(5, 5, 20, 20)];
        [hashButton setImage: [UIImage imageNamed:kHashImage] forState:UIControlStateNormal];
        [hashButton addTarget:self action:@selector(hashTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        self.textField = [[UITextField alloc] initWithFrame:CGRectMake(25, 5.0f, 280.0f, 28.0f)];
        
        [inputWrapper addSubview:hashButton];
        [inputWrapper addSubview:self.textField];
        
        [self addSubview:inputWrapper];

        CGFloat tableHeight = self.frame.size.height - 40.0f - 10.0f - self.frame.origin.y;
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40.0f, 300.0f, tableHeight)];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:self.tableView];
        
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_textured_light_blue"]];

        // setup delegates
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.textField.delegate = self;
        
        // load
        [self loadTagsList];
        [self.tableView reloadData];
    }
    return self;
}

- (void)showAnimated
{
    NSTimeInterval duration = 0.3f;
    self.hidden = NO;    
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationCurveEaseOut animations:^{
        [self setAlpha:1.0];
        self.frame = self.initialFrame;
    } completion:^(BOOL finished) {
        [self.textField becomeFirstResponder];
    }];
}

- (void)hideAnimated
{
    NSTimeInterval duration = 0.3f;
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationCurveEaseOut animations:^{
        self.frame = CGRectMake(self.initialFrame.origin.x, - self.initialFrame.origin.y- self.initialFrame.size.height, self.initialFrame.size.width, self.initialFrame.size.height);
        [self setAlpha:0.0];
    } completion:^(BOOL finished) {
        [self.textField resignFirstResponder];
        self.hidden = YES;
    }];
}

- (void)doHashTagSelect
{
    NSString* tag = self.textField.text;
    [self.textField resignFirstResponder];    
    [self.dataSelectDelegate selectDataSource:self returnedData:tag];
}

- (IBAction)hashTapped:(id)sender
{
    [self.textField resignFirstResponder];
    [self.dataSelectDelegate selectDataSource:self returnedData:nil]; // nil tag = cancelled
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tagsList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"IDHashTagTableCell";
    HashTagCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[HashTagCell alloc] initWithFrame: CGRectMake(10, 0, 300, 20)];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.tagText = [self.tagsList objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // tag selected
    NSString *tag = [self.tagsList objectAtIndex:indexPath.row];
    [self.textField setText:tag];
    [self.dataSelectDelegate selectDataSource:self returnedData:tag];
}

- (void)loadTagsList
{
    // TODO: load from server
    _tagsList = [NSArray arrayWithObjects:
                 @"smaple", 
                 @"entertainment", 
                 @"fun",
                 @"contest",
                 @"ddddd",
                 nil];
                 
//    [[TagsService new] getTags:^(NSArray* tags) {
//        _tagsList = tags;        
//        [self.tableView reloadData];
//    }];
}
@end

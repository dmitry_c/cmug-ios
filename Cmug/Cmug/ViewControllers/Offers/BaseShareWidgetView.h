//
//  BaseShareWidgetView.h
//  Cmug
//
//  Created by Dmitry C on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIImage+CappedImage.h"
#import "FacebookWrapper.h"
#import "Offer.h"

#define kFacebookShareImageName @"share_facebook"
#define kFacebookShareDisabledImageName @"facebook_share_gs"
#define kTwitterShareImageName @"share_twitter"
#define kTwitterShareDisabledImageName @"twitter_share_gs"

#define kShareCheckButtonImage @"share_green_accessory"
#define kShareConfigureButtonImage @"btn_share_configure_blue"

#define kShareCheckButtonDisabledImage @"share_accessory_gs"
#define kShareConfigureButtonDisabledImage @"btn_share_configure_gs"

@interface BaseShareWidgetView : UIView
@property (nonatomic, retain) UIButton* shareCheckButton;
@property (nonatomic, retain) UIButton* configureButton;

@property (nonatomic, retain) UIImageView* imageView;
@property (nonatomic, retain) UILabel* titleLabel;
@property (nonatomic, retain) UILabel* descriptionLabel;

@property (nonatomic, assign) BOOL disabled;

// Generic instance of either CreatOfferRequest CreateResponseRequest Offer.
// This view will be listening to `sharing` property.
@property (unsafe_unretained, nonatomic) id offerData;

- (void)updateButtons;
- (void)setup;

// To be defined in derived class
- (void)updateShareCheckButton;
- (void)updateOfferShareOption:(BOOL)selected;
- (void)configureShare;
- (BOOL)isConfigured;
- (UIImage *)enabledImage;
- (UIImage *)disabledImage;
@end

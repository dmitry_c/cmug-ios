//
//  BaseShareWidgetView.m
//  Cmug
//
//  Created by Dmitry C on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseShareWidgetView.h"

@implementation BaseShareWidgetView

@synthesize shareCheckButton = _shareCheckButton;
@synthesize configureButton = _configureButton;
@synthesize imageView = _imageView;
@synthesize titleLabel = _titleLabel;
@synthesize descriptionLabel = _descriptionLabel;
@synthesize disabled = _disabled;
@synthesize offerData = _offerData;

- (void)setOfferData:(id)offerData
{
    if (_offerData != offerData) {
        _offerData = offerData;
        [self setupDataObservers];
        [self updateButtons];
    }    
}

#pragma mark - Class specific
- (void)updateShareCheckButton
{
    [self doesNotRecognizeSelector:_cmd];
}

- (BOOL)isConfigured
{
    [self doesNotRecognizeSelector:_cmd];
    return YES;
}

- (void)configureShare
{
    [self doesNotRecognizeSelector:_cmd];
}

- (void)updateOfferShareOption:(BOOL)selected
{
    [self doesNotRecognizeSelector:_cmd];
}

- (UIImage *)enabledImage
{
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (UIImage *)disabledImage
{
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (void)setup
{
    [self addSubview:self.imageView];
    [self addSubview:self.titleLabel];
    [self addSubview:self.descriptionLabel];
    [self addSubview:self.configureButton];
    [self addSubview:self.shareCheckButton];
}

#pragma mark - Observers
- (void)setupDataObservers
{
    @try {
        [self.offerData removeObserver:self forKeyPath:@"sharing"];
    }
    @catch (NSException *exception) {}
    
    [self.offerData addObserver:self forKeyPath:@"sharing" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self updateShareCheckButton];
}

#pragma mark - Controls
- (UIImageView *)imageView
{
    if (!_imageView) {
        CGFloat topMargin = (self.frame.size.height - 30.0f) * 0.5f;
        
        // Image View
        _imageView = [[UIImageView alloc] initWithImage:[self enabledImage]];
        CGRect frame = CGRectMake(14.0f, topMargin, _imageView.frame.size.width, _imageView.frame.size.height);
        _imageView.frame = frame;
    }
    return _imageView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        CGFloat topMargin = (self.frame.size.height - 30.0f) * 0.5f;
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(43.0f, topMargin, 200.0, 20.0f)];
        _titleLabel.text = @"";
        CGFloat grayLevel = 60.0f / 255.0f;
        _titleLabel.textColor = [UIColor colorWithRed:grayLevel green:grayLevel blue:grayLevel alpha:1.0f];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    }
    return _titleLabel;
}    

- (UILabel *)descriptionLabel
{
    if (!_descriptionLabel) {
        CGFloat topMargin = (self.frame.size.height - 30.0f) * 0.5f;
        // Description
        _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(43.0f, topMargin + 18.0f + 4.0f, 200.0, 12.0f)];
        _descriptionLabel.text = @"";
        CGFloat grayLevel = 113.0f / 255.0f;
        _descriptionLabel.textColor = [UIColor colorWithRed:grayLevel green:grayLevel blue:grayLevel alpha:1.0f];
        _descriptionLabel.backgroundColor = [UIColor clearColor];
        _descriptionLabel.font = [UIFont systemFontOfSize:12.0f];
    }
    return _descriptionLabel;
}

- (UIButton *)shareCheckButton
{
    if (!_shareCheckButton) {
        UIImage *buttonImageSelected = [UIImage imageNamed:kShareCheckButtonImage];
        UIImage *buttonImageNormal = [UIImage imageNamed:kShareCheckButtonDisabledImage];
        CGFloat buttonWidth = MAX(buttonImageSelected.size.width, buttonImageNormal.size.width);
        CGFloat margin = 14.0f;
        CGFloat left = self.frame.size.width - buttonWidth - margin;
        _shareCheckButton = [[UIButton alloc] initWithFrame:CGRectMake(left, margin, buttonWidth, 25.0f)];
        [_shareCheckButton setImage:buttonImageNormal forState:UIControlStateNormal];
        [_shareCheckButton setImage:buttonImageSelected forState:UIControlStateSelected];
        
        [_shareCheckButton addTarget:self action:@selector(shareCheckAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareCheckButton;
}

- (UIButton *)configureButton
{
    if (!_configureButton) {
        UIEdgeInsets insets = UIEdgeInsetsMake(0, 8.0f, 0, 8.0f);
        UIImage *buttonImageNormal = [[UIImage imageNamed:kShareConfigureButtonImage] cappedImageWithCapInsets:insets];
        UIImage *buttonImageDisabled = [UIImage imageNamed:kShareConfigureButtonDisabledImage];
        CGFloat buttonWidth = 85.0f;
        CGFloat buttonHeight = buttonImageNormal.size.height;
        
        CGFloat margin = 14.0f;
        CGFloat left = self.frame.size.width - buttonWidth - margin;
        _configureButton = [[UIButton alloc] initWithFrame:CGRectMake(left, margin, buttonWidth, buttonHeight)];
        [_configureButton setBackgroundImage:buttonImageNormal forState:UIControlStateNormal];
        [_configureButton setBackgroundImage:buttonImageDisabled forState:UIControlStateDisabled];
        [_configureButton setTitle:@"Configure" forState:UIControlStateNormal];
        [_configureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _configureButton.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        
        [_configureButton addTarget:self action:@selector(configureAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _configureButton;
}

- (void)setDisabled:(BOOL)disabled
{
    if (disabled) {
        // set grayscale colors and disable controls
        self.imageView.image = [self disabledImage];
    } else {
        // set normal colors
        self.imageView.image = [self enabledImage];
    }

    [self.configureButton setEnabled:!disabled];
    [self.shareCheckButton setEnabled:!disabled];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)dealloc
{
    @try {
        [self.offerData removeObserver:self forKeyPath:@"sharing"];
    }
    @catch (NSException *exception) {}
}

- (void)updateButtons
{
    BOOL isConfigured = [self isConfigured];
    self.configureButton.hidden = isConfigured;
    self.shareCheckButton.hidden = !isConfigured;
    [self updateShareCheckButton];
}

- (IBAction)configureAction:(UIButton *)sender
{
    // Facebook wrapper configure    
    NSLog(@"Configure");
    sender.enabled = NO;  // set enabled on configure callback
    [self configureShare];
}

- (IBAction)shareCheckAction:(UIButton *)sender
{
    NSLog(@"Share Check");
    sender.selected = !sender.selected;
    [self updateOfferShareOption:sender.selected];
}

@end

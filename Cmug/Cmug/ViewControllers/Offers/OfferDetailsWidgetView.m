//
//  OfferDetailsWidgetView.m
//  Cmug
//
//  Created by Dmitry C on 7/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OfferDetailsWidgetView.h"
#import "UIView+RoundedBorderWithShadow.h"

#define kHashButtonImage @"hash.png"
#define kAttachButtonImage @"clip.png"
#define kAttachButtonImageSelected @"clip_blue"

@implementation OfferDetailsWidgetView
@synthesize offerTitle = _offerTitle;
@synthesize offerDetails = _offerDetails;
@synthesize userPicture = _userPicture;
@synthesize hashButton = _hashButton;
@synthesize attachButton = _attachButton;
@synthesize offerData = _offerData;

#pragma mark - Observers
- (void)setupDataObservers
{
    @try {
        [self.offerData removeObserver:self forKeyPath:@"title"];
        [self.offerData removeObserver:self forKeyPath:@"description"];
    }
    @catch (NSException *exception) {}
    
    [self.offerData addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
    [self.offerData addObserver:self forKeyPath:@"description" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    self.offerTitle.text = _offerData.title;
    self.offerDetails.text = _offerData.description;
}

#pragma mark - 
- (void)setOfferData:(CreateOfferRequest *)offerData
{
    if (_offerData != offerData) {
        _offerData = offerData;
        self.offerTitle.text = offerData.title;
        self.offerDetails.text = offerData.description;
    }    
}

- (UITextField *)offerTitle
{
    if (!_offerTitle) {
        CGRect frame = CGRectMake(55.0f, 10.0f, 235.0f, 31.0f);
        _offerTitle = [[UITextField alloc] initWithFrame:frame];
        _offerTitle.placeholder = @"Ask anything...";
    }
    return _offerTitle;
}

- (UIPlaceHolderTextView *)offerDetails
{
    if (!_offerDetails) {
        CGRect frame = CGRectMake(8.0f, 47.0f, 272.0f, 76.0f);
        _offerDetails = [[UIPlaceHolderTextView alloc] initWithFrame:frame];
        _offerDetails.placeholder = @"...add a detailed description (optional)";
    }
    return _offerDetails;
}

- (UIImageView *)userPicture
{
    if (!_userPicture) {
        CGRect frame = CGRectMake(8.0f, 8.0f, 38.0f, 38.0f);
        _userPicture = [[UIImageView alloc] initWithFrame:frame];
        [_userPicture setImage:[UIImage imageNamed:@"avatar"]];
    }
    return _userPicture;
}

- (UIButton *)hashButton
{
    if (!_hashButton) {
        CGRect frame = CGRectMake(8.0f, 145.0f, 14.0f, 17.0f);
        _hashButton = [[UIButton alloc] initWithFrame:frame];
        [_hashButton setBackgroundImage:[UIImage imageNamed:kHashButtonImage] forState:UIControlStateNormal];
    }
    return _hashButton;
}

- (UIButton *)attachButton
{
    if (!_attachButton) {
        CGRect frame = CGRectMake(48.0f, 139.0f, 11.0f, 23.0f);
        _attachButton = [[UIButton alloc] initWithFrame:frame];
        [_attachButton setBackgroundImage:[UIImage imageNamed:kAttachButtonImage] forState:UIControlStateNormal];
        [_attachButton setBackgroundImage:[UIImage imageNamed:kAttachButtonImageSelected] forState:UIControlStateSelected];
    }
    return _attachButton;
}

- (void)setup
{
    [self addSubview:self.userPicture];
    [self addSubview:self.offerTitle];
    [self addSubview:self.offerDetails];
    [self addSubview:self.hashButton];
    [self addSubview:self.attachButton];
    self.backgroundColor = [UIColor whiteColor];
    [self decorateWithRoundedBordersAndShadow];
    
    // draw line in offer descriptions block
    CAShapeLayer *lineShape = [CAShapeLayer layer];
    //    
    //    NSArray *sublayers = [self.topView.layer.sublayers copy];
    //    for (CALayer *l in sublayers) {
    //        if ([l isKindOfClass:CAShapeLayer.class]) {
    //            [l removeFromSuperlayer];
    //        }
    //    }
    
    CGMutablePathRef linePath = nil;
    linePath = CGPathCreateMutable();
    
    lineShape.lineWidth = 1.0f;
    lineShape.lineCap = kCALineCapRound;
    lineShape.strokeColor = [[UIColor colorWithRed:17.0f/255.0f green:84.0f/255.0f blue:103.0f/255.0f alpha:1.0] CGColor];
    
    CGFloat y = self.frame.size.height - 35.0f ;
    CGFloat margin = 5.0f;
    CGFloat startX = margin;
    CGFloat endX = self.frame.size.width - margin; 
    
    CGPathMoveToPoint(linePath, NULL, startX, y);
    CGPathAddLineToPoint(linePath, NULL, endX, y);
    
    lineShape.path = linePath;
    CGPathRelease(linePath);
    
    [self.layer addSublayer:lineShape];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)dealloc
{
    [self setUserPicture:nil];
    [self setOfferDetails:nil];
    [self setOfferTitle:nil];
    @try {
        [self.offerData removeObserver:self forKeyPath:@"title"];
        [self.offerData removeObserver:self forKeyPath:@"description"];
    }
    @catch (NSException *exception) {}
}

@end

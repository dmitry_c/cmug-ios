//
//  FacebookShareWidgetView.m
//  Cmug
//
//  Created by Dmitry C on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FacebookShareWidgetView.h"

@implementation FacebookShareWidgetView
- (void)updateShareCheckButton
{
    NSUInteger sharingOptions;
    [[self.offerData valueForKey:@"sharing"] getValue:&sharingOptions];
    
    // show share check button selected if Facebook in share options
    self.shareCheckButton.selected = [[NSNumber numberWithInt:(sharingOptions & kOfferShareType_Facebook)] boolValue];
}

- (BOOL)isConfigured
{
    return [[FacebookWrapper get] isConfigured];
}

- (void)configureShare
{
    [[FacebookWrapper get] configureShare:^(NSDictionary *result) {
        NSUInteger code = [[result objectForKey:@"code"] integerValue];
        if (code == kSharerSuccess) {
            [self updateButtons];
        } else {
            // Show configure error?
            NSLog(@"%@", [result objectForKey:@"description"]);
        }
    }];
}

- (void)updateOfferShareOption:(BOOL)selected
{
    // update offer data
    NSUInteger sharingOptions;
    [[self.offerData valueForKey:@"sharing"] getValue:&sharingOptions];
    if (selected) {
        sharingOptions |= kOfferShareType_Facebook;
    } else {
        sharingOptions &= ~kOfferShareType_Facebook;
    }
    [self.offerData setValue:[NSNumber numberWithInteger:sharingOptions] forKey:@"sharing"];
}

- (UIImage *)enabledImage
{
    return [UIImage imageNamed:kFacebookShareImageName];
}

- (UIImage *)disabledImage
{
    return [UIImage imageNamed:kFacebookShareDisabledImageName];
}

- (void)setup
{
    [super setup];
    self.titleLabel.text = @"Facebook";
    self.descriptionLabel.text = @"Share on Facebook";
}

@end

//
//  AttachedImageWidgetView.m
//  Cmug
//
//  Created by Dmitry C on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AttachedImageWidgetView.h"
#import "UIView+RoundedBorderWithShadow.h"
#import "SDWebImage/UIImageView+WebCache.h"

@implementation AttachedImageWidgetView
@synthesize offerData = _offerData;
@synthesize imageView = _imageView;
@synthesize removeButton = _removeButton;

#pragma mark - Observers
- (void)setupDataObservers
{
    @try {
        [self.offerData removeObserver:self forKeyPath:@"attachment"];
    }
    @catch (NSException *exception) {}
    
    [self.offerData addObserver:self forKeyPath:@"attachment" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self.imageView setImageWithURL:[NSURL URLWithString:self.offerData.attachment]];
}

#pragma mark - 
- (UIImageView *)imageView
{
    if (!_imageView) {
        CGFloat height = 40.0f;
        CGRect frame = CGRectMake(10.0f, (self.frame.size.height - height) * 0.5f, 56.0f, height);
        _imageView = [[UIImageView alloc] initWithFrame:frame];
        // add border decoration
        [_imageView decorateWithVignette];
    }
    return _imageView;
}

- (UIButton *)removeButton
{
    if (!_removeButton) {
        CGFloat width = 70.0f;
        CGFloat margin = 10.0f;
        _removeButton = [[UIButton alloc] initWithFrame: CGRectMake(self.frame.size.width - width - margin, (self.frame.size.height - 24.0f) * 0.5f, width, 24.0f)];
        [_removeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_removeButton setTitle:@"Remove" forState:UIControlStateNormal];
    }
    return _removeButton;
}

- (void)setup
{
    self.backgroundColor = [UIColor whiteColor];
    [self decorateWithRoundedBordersAndShadow];
    [self addSubview:self.imageView];
    // label
    UILabel *attachedImageLabel = [[UILabel alloc] initWithFrame: CGRectMake(76.0f, (self.frame.size.height - 24.0f) * 0.5f, 150.0f, 24.0f)];
    attachedImageLabel.text = @"attached image";
    attachedImageLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:attachedImageLabel];
    // remove button
    [self addSubview:self.removeButton];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)dealloc
{
    @try {
        [self.offerData removeObserver:self forKeyPath:@"attachment"];
    }
    @catch (NSException *exception) {}
}
@end

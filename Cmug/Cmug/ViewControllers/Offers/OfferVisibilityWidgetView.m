//
//  OfferVisibilityWidgetView.m
//  Cmug
//
//  Created by Dmitry C on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OfferVisibilityWidgetView.h"
#import "UserPicturesList.h"
#import "UIView+RoundedBorderWithShadow.h"
#import "CellSeparator.h"

#define kEyeImage @"visibility_eye_blue.png"

@interface OfferVisibilityWidgetView()
{
    CGRect fullFrame;
    CGRect collapsedFrame;
}

@property (nonatomic, retain) UIView* captionView;
@property (nonatomic, retain) UIButton* visibilityButton;
@property (nonatomic, retain) UILabel* visibilityLabel;

@property (nonatomic, retain) UserPicturesList* usersListView;
@end

@implementation OfferVisibilityWidgetView
@synthesize offerVisibility = _offerVisibility;
@synthesize usersList = _usersList;
@synthesize offerData = _offerData;
@synthesize showUsersList = _showUsersList;

@synthesize captionView = _captionView;
@synthesize visibilityButton = _visibilityButton;
@synthesize visibilityLabel = _visibilityLabel;
@synthesize usersListView = _usersListView;

#pragma mark - Observers
- (void)setupDataObservers
{
    @try {
        [self.offerData removeObserver:self forKeyPath:@"visibility"];
        [self.offerData removeObserver:self forKeyPath:@"usersList"];
    }
    @catch (NSException *exception) {}
    
    [self.offerData addObserver:self forKeyPath:@"visibility" options:NSKeyValueObservingOptionNew context:nil];
    [self.offerData addObserver:self forKeyPath:@"usersList" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    self.offerVisibility = _offerData.visibility;
    self.usersList = _offerData.usersList;
}

#pragma mark -

- (void)setOfferData:(CreateOfferRequest *)offerData
{
    if (_offerData != offerData) {
        _offerData = offerData;
        [self setupDataObservers];
        self.offerVisibility = _offerData.visibility;
        self.usersList = _offerData.usersList;
    }    
}

- (void)setOfferVisibility:(OfferVisibilityType)offerVisibility
{
    if(_offerVisibility != offerVisibility) {
        _offerVisibility = offerVisibility;
        [self setNeedsLayout];
    }
}

- (void)setUsersList:(NSArray *)usersList
{
    if (usersList != _usersList) {
        _usersList = usersList;
        self.usersListView.usersList = _usersList;
        [self setNeedsLayout];
    }
}

- (void)setShowUsersList:(BOOL)showUsersList
{
    if (_showUsersList != showUsersList) {
        _showUsersList = showUsersList;        
        [self setNeedsLayout];
    }
}

- (void)setupButtonHandlerForTarget:(id)target action:(SEL)action
{
    [self.visibilityButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];    
    // Set the same handler for users list "more" button
    [self.usersListView setupButtonHandlerForTarget:target action:action];
}

- (UIView *)captionView
{
    if (!_captionView) {
        CGFloat height = 40.0f;
        CGRect frame = CGRectMake(10.0f, 0, self.frame.size.width - 10 - 10, height);
        _captionView = [[UIView alloc] initWithFrame:frame];
        UILabel *visibleToLabel = [[UILabel alloc] initWithFrame:CGRectMake(45.0f, 5.0f, 100.0f, 30.0f)];
        visibleToLabel.text = @"Visible to:";
        
        [_captionView addSubview:self.visibilityButton];
        [_captionView addSubview:visibleToLabel];
        [_captionView addSubview:self.visibilityLabel];
        _captionView.backgroundColor = [UIColor whiteColor];
        [_captionView decorateWithRoundedBordersAndShadow];
    }
    return _captionView;
}

- (UserPicturesList *)usersListView
{
    if (!_usersListView) {
        CGFloat height = 50.0f;
    
        _usersListView = [[UserPicturesList alloc] initWithFrame:CGRectMake(10.0f, 50.0f, self.frame.size.width - 10 - 10, height)];
    }
    return _usersListView;
}

- (UIButton *)visibilityButton
{
    if (!_visibilityButton) {
        UIImage *buttonBackground = [UIImage imageNamed:kEyeImage];
        CGRect frame = CGRectMake(10.0f, 12.0f, buttonBackground.size.width, buttonBackground.size.height);
        _visibilityButton = [[UIButton alloc] initWithFrame:frame];
        [_visibilityButton setBackgroundImage:[UIImage imageNamed:kEyeImage] forState:UIControlStateNormal];
    }
    return _visibilityButton;
}

- (UILabel *)visibilityLabel
{
    if (!_visibilityLabel) {
        CGRect frame = CGRectMake(190.0f, 5.0f, 100.0f, 30.0f);
        _visibilityLabel = [[UILabel alloc] initWithFrame:frame];
        _visibilityLabel.text = @"Public";
        _visibilityLabel.textAlignment = UITextAlignmentRight;
    }
    return _visibilityLabel;
}

- (void)setup
{
    fullFrame = self.frame;
    collapsedFrame = CGRectMake(fullFrame.origin.x, fullFrame.origin.y, fullFrame.size.width, fullFrame.size.height - 50.0f);
    self.showUsersList = YES;
    
    // Top view
    [self addSubview:self.captionView];
    // Users pictures
    [self addSubview:self.usersListView];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (self.offerVisibility == kOfferVisibilityType_Public || !self.showUsersList) {
        // hide users list
        [self.usersListView setHidden:YES];
        self.frame = collapsedFrame;
        self.visibilityLabel.text = @"Public";
    } else {
        [self.usersListView setHidden:NO];
        self.frame = fullFrame;
        if ([_usersList count] > 1) {
            self.visibilityLabel.text = [NSString stringWithFormat:@"%d friends", [_usersList count]];            
        } else if ([_usersList count] == 1) {
            self.visibilityLabel.text = @"one friend";
        } else {
            // no users - set visibility to public
            self.offerVisibility = kOfferVisibilityType_Public;
        }
    }
 
    [CellSeparator decorateView:self withSeparatorStyle:CellSeparatorStyleLight];
}

- (void)dealloc
{
    @try {
        [self.offerData removeObserver:self forKeyPath:@"visibility"];
        [self.offerData removeObserver:self forKeyPath:@"usersList"];
    }
    @catch (NSException *exception) {}
}

@end

//
//  OfferDetailsWidgetView.h
//  Cmug
//
//  Created by Dmitry C on 7/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+LoadFromNib.h"
#import "UIPlaceHolderTextView.h"
#import "CreateOfferRequest.h"

@interface OfferDetailsWidgetView : UIView
@property (retain, nonatomic) IBOutlet UITextField *offerTitle;
@property (retain, nonatomic) IBOutlet UIPlaceHolderTextView *offerDetails;

@property (retain, nonatomic) IBOutlet UIImageView *userPicture;
@property (retain, nonatomic) IBOutlet UIButton *hashButton;
@property (retain, nonatomic) IBOutlet UIButton *attachButton;

@property (unsafe_unretained, nonatomic) CreateOfferRequest *offerData;
@end

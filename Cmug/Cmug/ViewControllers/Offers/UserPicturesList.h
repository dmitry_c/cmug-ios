//
//  UserPicturesList.h
//  Cmug
//
//  Created by Dmitry C on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImage/UIImageView+WebCache.h"

@interface UserPicturesList : UIView
// data
@property (nonatomic, retain) NSArray* usersList;

- (void)setupButtonHandlerForTarget:(id)target action:(SEL)action;
@end

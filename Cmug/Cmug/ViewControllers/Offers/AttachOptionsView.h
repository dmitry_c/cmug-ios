//
//  AttachOptionsView.h
//  Cmug
//
//  Created by Dmitry C on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kFromCameraButtonTag 1
#define kFromLibraryButtonTag 2

@interface AttachOptionsView : UIView
@property (nonatomic, retain) UIButton *fromCameraButton;
@property (nonatomic, retain) UIButton *fromLibraryButton;
@end

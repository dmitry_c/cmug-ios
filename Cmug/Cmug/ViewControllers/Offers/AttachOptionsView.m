//
//  AttachOptionsView.m
//  Cmug
//
//  Created by Dmitry C on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AttachOptionsView.h"
#import "UIImage+CappedImage.h"

#define kFromCameraButtonCappedImage @"btn_from_camera"
#define kFromLibraryButtonCappedImage @"btn_from_library"

@implementation AttachOptionsView
@synthesize fromCameraButton = _fromCameraButton;
@synthesize fromLibraryButton = _fromLibraryButton;

- (UIButton *)makeButtonWithTitle:(NSString*)title backgroundImage:(NSString*)imageName frame:(CGRect)frame
{
    UIEdgeInsets insets = UIEdgeInsetsMake(0.0f, 6.0f, 0.0f, 55.0f);
    UIImage *buttonImage = [[UIImage imageNamed:imageName] cappedImageWithCapInsets:insets];
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    button.titleLabel.textAlignment = UITextAlignmentLeft;
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    button.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    [button setTitle:title forState:UIControlStateNormal];
    CGFloat grayLevel = 68.0f/255.0f;
    UIColor *labelColor = [UIColor colorWithRed:grayLevel green:grayLevel blue:grayLevel alpha:1.0f];
    [button setTitleColor:labelColor forState:UIControlStateNormal];
    [button setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.shadowColor = [UIColor whiteColor];
    button.titleLabel.shadowOffset = CGSizeMake(0, 1);
//    [button setTitleShadowOffset:CGSizeMake(0, 1)];
    return button;
}

- (UIButton *)fromCameraButton
{
    if (!_fromCameraButton) {
        _fromCameraButton = [self makeButtonWithTitle:@"From camera" backgroundImage:kFromCameraButtonCappedImage frame:CGRectMake(10, 10, 300, 40)];
        _fromCameraButton.tag = kFromCameraButtonTag;
    }
    return _fromCameraButton;
}

- (UIButton *)fromLibraryButton
{
    if (!_fromLibraryButton) {
        _fromLibraryButton = [self makeButtonWithTitle:@"From library" backgroundImage:kFromLibraryButtonCappedImage frame:CGRectMake(10, 10 + 42 + 10, 300, 40)];
        _fromLibraryButton.tag = kFromLibraryButtonTag;
    }
    return _fromLibraryButton;
}

- (void)setup
{
    [self addSubview:self.fromCameraButton];
    [self addSubview:self.fromLibraryButton];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

@end

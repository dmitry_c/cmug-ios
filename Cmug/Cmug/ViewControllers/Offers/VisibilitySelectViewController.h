//
//  VisibilitySelectViewController.h
//  Cmug
//
//  Created by Dmitry C on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Offer.h"
#import "AutoFetchTableView.h"
#import "DataSelectProtocol.h"

#define kOfferVisibilityType @"visibilityType"
#define kUsersList @"usersList"

@interface VisibilitySelectViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (unsafe_unretained, nonatomic) IBOutlet AutoFetchTableView *tableView;

// data
@property (nonatomic, assign) OfferVisibilityType offerVisibilityType;
// The array of selected ProfileShort objects
@property (nonatomic, retain) NSMutableArray* selectedUsersList;

@property (nonatomic, unsafe_unretained) id<DataSelectProtocol>dataSelectDelegate;
@end

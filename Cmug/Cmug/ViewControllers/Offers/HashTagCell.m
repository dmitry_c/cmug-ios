//
//  HashTagCell.m
//  
//
//  Created by Dmitry C on 7/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HashTagCell.h"

@implementation HashTagCell
@synthesize tagText = _tagText;

static NSString* CellIdentifier = @"IDHashTagTableCell";

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        label = [[UILabel alloc] initWithFrame:self.bounds];
        label.adjustsFontSizeToFitWidth = YES;
        label.textAlignment = UITextAlignmentLeft;
        label.opaque = YES;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor blackColor];
        label.font = [UIFont systemFontOfSize:18.0];

        [self addSubview:label];
    }
    return self;
}

- (void)setTagText:(NSString *)tagText
{
    if (tagText != _tagText) {
        _tagText = tagText;
        [label setText:_tagText];
    }
}

- (NSString *)reuseIdentifier
{
    return CellIdentifier;
}

@end

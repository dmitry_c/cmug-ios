//
//  UserPicturesList.m
//  Cmug
//
//  Created by Dmitry C on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserPicturesList.h"
#import <QuartzCore/QuartzCore.h>

#define MARGIN 9.0f
#define PADDING 13.0f
#define IMAGE_WIDTH 38.0f
#define IMAGE_HEIGHT 38.0f
#define IMAGE_CORNER_RADIUS 6.0f
#define kRightArrowImage @"right_arrow_gray"

@interface UserPicturesList()
@property (nonatomic, retain) NSMutableArray* imageViews;

@property (nonatomic, retain) UIButton* rightArrowButton;
@end

@implementation UserPicturesList
@synthesize usersList = _usersList;
@synthesize imageViews = _imageViews;
@synthesize rightArrowButton = _rightArrowButton;

- (NSMutableArray *)imageViews
{
    // Create array with max 5 UIImageView elements with user images.
    if (!_imageViews) {
        _imageViews = [NSMutableArray arrayWithCapacity:5];
    }
    
    NSInteger count = [self.usersList count];
    NSInteger imagesCount = MIN(count, 5);
    UIImage *defaultImage = [UIImage imageNamed:@"avatar"];
    
    if (_imageViews.count == 0) {
        NSArray *pictures = [self.usersList valueForKey:@"picture"];
        
        for (NSInteger imageIndex=0; imageIndex<imagesCount; imageIndex++) {
            NSString *pictureUrl = [pictures objectAtIndex:imageIndex];
            CGFloat imageLeftOffset = MARGIN + imageIndex * (IMAGE_WIDTH + PADDING);
            CGFloat imageTopOffset = 0.5f * (self.frame.size.height - IMAGE_HEIGHT);  // Center image vertically
            UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(imageLeftOffset, imageTopOffset, IMAGE_WIDTH, IMAGE_HEIGHT)];
            [iv setImageWithURL:[NSURL URLWithString:pictureUrl] placeholderImage:defaultImage];

            // Add corner radius
            iv.clipsToBounds = YES;
            iv.layer.cornerRadius = IMAGE_CORNER_RADIUS;
            iv.layer.masksToBounds = YES;
            
            [_imageViews addObject:iv];
        }

    }
    return _imageViews;
}

- (void)setUsersList:(NSArray *)usersList
{
    if (_usersList != usersList) {
        _usersList = usersList;
        [_imageViews removeAllObjects];  // purge images array
        [self setNeedsLayout];
    }
}

- (void)setupButtonHandlerForTarget:(id)target action:(SEL)action
{
    [self.rightArrowButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

- (UIButton*)rightArrowButton
{
    if (!_rightArrowButton) {
        // Right arrow button
        UIImage *bgImage = [UIImage imageNamed:kRightArrowImage];
        _rightArrowButton = [[UIButton alloc] initWithFrame:
                                      CGRectMake(self.frame.size.width - bgImage.size.width - MARGIN, 
                                                 0.5f * (self.frame.size.height - bgImage.size.height), 
                                                 bgImage.size.width, 
                                                 bgImage.size.height)];
        [_rightArrowButton setBackgroundImage:bgImage forState:UIControlStateNormal];
    }
    
    return _rightArrowButton;
}

- (void)setup
{
    [self rightArrowButton];
    [self addSubview:self.rightArrowButton];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)layoutSubviews
{
    for (UIImageView* iv in self.imageViews) 
    {
        [iv removeFromSuperview];
        [self addSubview: iv];
    }
}

- (void)awakeFromNib
{
    [self setup];
}
@end

//
//  OfferTypeSelectWidgetView.m
//  Cmug
//
//  Created by Dmitry C on 7/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OfferTypeSelectWidgetView.h"

@implementation OfferTypeSelectWidgetView
@synthesize offerTypeDisplayLabel = _offerTypeDisplayLabel;
@synthesize offerTypesSlider = _offerTypesSlider;
@synthesize offerData = _offerData;

#pragma mark - Observers
- (void)setupDataObservers
{
    @try {
        [self.offerData removeObserver:self forKeyPath:@"requestType"];
    }
    @catch (NSException *exception) {}
    
    [self.offerData addObserver:self forKeyPath:@"requestType" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    self.offerTypesSlider.selectedOfferType = _offerData.requestType;
}

#pragma mark - 
- (void)setOfferData:(CreateOfferRequest *)offerData
{
    if (_offerData != offerData) {
        _offerData = offerData;
        self.offerTypesSlider.selectedOfferType = _offerData.requestType;
    }
}

- (OfferTypesSlider *)offerTypesSlider
{
    if (!_offerTypesSlider) {
        _offerTypesSlider = [[OfferTypesSlider alloc] initWithFrame:CGRectMake(self.frame.size.width - 120 - 10, 0, 120, 40)];
        _offerTypesSlider.backgroundColor = [UIColor clearColor];
        [_offerTypesSlider addTarget:self action:@selector(offerTypeSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    }
    return _offerTypesSlider;
}

- (IBAction)offerTypeSliderValueChanged:(OfferTypesSlider*)sender {
    self.offerData.requestType = sender.selectedOfferType;
    self.offerTypeDisplayLabel.text = self.offerData.requestTypeDisplay;
}

- (UILabel *)offerTypeDisplayLabel
{
    if (!_offerTypeDisplayLabel) {
        _offerTypeDisplayLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 112, 21)];
        _offerTypeDisplayLabel.font = [UIFont systemFontOfSize:14.0f];
        _offerTypeDisplayLabel.textColor = [UIColor colorWithRed:22.0f/255.0f green:126.0f/255.0f blue:59.0f/255.0f alpha:1.0f];
        _offerTypeDisplayLabel.backgroundColor = [UIColor clearColor];
        _offerTypeDisplayLabel.text = @"Text";
    }
    return _offerTypeDisplayLabel;
}

- (void)setup
{
    UILabel *requestingLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 112.0f, 21.0f)];
    requestingLabel.text = @"Requesting";
    requestingLabel.font = [UIFont systemFontOfSize:16.0f];
    requestingLabel.textColor = [UIColor colorWithRed:78.0f/255.0f green:122.0f/255.0f blue:141.0f/255.0f alpha:1.0f];
    requestingLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:requestingLabel];
    [self addSubview:self.offerTypesSlider];
    [self addSubview:self.offerTypeDisplayLabel];
    self.offerTypesSlider.selectedOfferType = self.offerData.requestType;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)dealloc
{
    @try {
        [self.offerData removeObserver:self forKeyPath:@"requestType"];
    }
    @catch (NSException *exception) {}
}
@end

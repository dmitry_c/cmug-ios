//
//  VisibilitySelectViewController.m
//  Cmug
//
//  Created by Dmitry C on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VisibilitySelectViewController.h"
#import "FollowingService.h"
#import "FollowingPeopleHeaderView.h"
#import "UserProfileCell.h"
#import "UITableView+LoadCell.h"
#import "TableCellAccessories.h"
#import "CellSeparator.h"
#import "UITableView+QuickScrollAdditions.h"

@interface VisibilitySelectViewController () <FollowingPeopleHeaderViewDelegate, FollowingPeopleHeaderViewDataSource, AutoFetchTableViewDelegate, UIScrollViewDelegate>
@property (nonatomic, retain) FollowingResponse* lastResponse;

// views
@property (nonatomic, retain) FollowingPeopleHeaderView* headerView;
@property (nonatomic, retain) UIView* checkmark;

// dataSource
@property (nonatomic, strong) NSMutableArray* usersList;  // all friends (followed users) from server
@property (nonatomic, assign) NSInteger usersTotal;

- (void) loadFollowedUsersList;
- (BOOL) profileInSelectedProfiles:(ProfileShort*)profile;
@end

@implementation VisibilitySelectViewController
// public properties
@synthesize tableView = _tableView;
@synthesize selectedUsersList = _selectedUsersList;
@synthesize offerVisibilityType = _offerVisibilityType;
// private properties
@synthesize usersList = _usersList;
@synthesize usersTotal;
@synthesize headerView = _headerView;
@synthesize checkmark = _checkmark;
@synthesize lastResponse;
@synthesize dataSelectDelegate;

- (NSMutableArray *)usersList
{
    if (!_usersList) {
        _usersList = [NSMutableArray arrayWithCapacity:10];
    }  
    return _usersList;
}

- (NSMutableArray *)selectedUsersList
{
    if (!_selectedUsersList) {
        _selectedUsersList = [NSMutableArray arrayWithCapacity:self.usersTotal];
    }   
    return _selectedUsersList;
}

- (UIView *)checkmark
{
//    if (!_checkmark) {
//        _checkmark = [TableCellAccessories blueCheckmark];
//    }
//    return _checkmark;
    return [TableCellAccessories blueCheckmark];
}

#pragma mark - Header View
-(FollowingPeopleHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = [FollowingPeopleHeaderView loadFromNib];
        // place button
        float width = _headerView.frame.size.width;
        float height = _headerView.frame.size.height;
        float buttonWidth = 80.0f;
        float buttonHeight = 30.0f;
        float rightMargin = 10.0f;

        [_headerView.actionButton setFrame:CGRectMake(width - rightMargin - buttonWidth, (height - buttonHeight) * 0.5f, buttonWidth, buttonHeight)];
        [_headerView.actionButton setTitle:@"Select All" forState:UIControlStateNormal];
        [_headerView.actionButton setTitle:@"Deselect All" forState:UIControlStateSelected];
        _headerView.delegate = self;
        _headerView.dataSource = self;
        _headerView = [CellSeparator decorateView:_headerView withSeparatorStyle:CellSeparatorStyleDark];
    }
    return _headerView;
}

-(NSInteger)followingPeopleHeaderViewPeopleCount
{
    return self.usersTotal;
}

- (void)followingPeopleHeaderView:(FollowingPeopleHeaderView *)headerView didButtonAction:(UIButton*)sender
{
    if (sender.selected) {
        self.offerVisibilityType = kOfferVisibilityType_Users;
        // add all loaded users array to selected users array
        for (ProfileShort* profile in self.usersList) {
            if (![self profileInSelectedProfiles:profile]) {
                [self.selectedUsersList addObject:profile];
            }
        }
    } else {
        // deselect all - set public visibility
        self.offerVisibilityType = kOfferVisibilityType_Public;
        [self.selectedUsersList removeAllObjects];
    }
    [self.tableView reloadData];
}

#pragma mark -

- (void)loadFollowedUsersList
{
    [self.tableView setState:AutoFetchTableViewStateScrolledToBottom];
    [self.tableView setState:AutoFetchTableViewStateLoading];
    
    NSInteger limit = 20;
    NSInteger offset = 0;
    
    if (self.lastResponse) {
        offset = self.lastResponse.offset + limit;
    } else {
        [_usersList removeAllObjects];
    }
    
    [[FollowingService new] loadFollowedUsersWithLimit:limit andOffset:offset
                                             onSuccess:^(FollowingResponse* responseData) {
                                                 NSLog(@"%@", responseData);
                                                 //reload table
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [_usersList addObjectsFromArray:responseData.usersList];
                                                     usersTotal = responseData.total;
                                                     self.lastResponse = responseData;
                                                     [self.tableView reloadData];
                                                     [self.headerView reloadData];
                                                     [self.tableView performSelector:@selector(autoFetchTriggerEndLoading:) withObject:self.lastResponse];
                                                 });
                                             } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                                                 NSLog(@"%@", error);
                                                 [self.tableView performSelector:@selector(autoFetchTriggerEndLoading:) withObject:nil];
                                             }];
}

- (BOOL) profileInSelectedProfiles:(ProfileShort*)profile
{
    if (self.offerVisibilityType == kOfferVisibilityType_All) {
        return YES;  // If visibility set for "all friends"
    }
    
    if (self.selectedUsersList)
        return ([[self.selectedUsersList valueForKey:@"userId"] containsObject: profile.userId]);
    return NO;
}

- (IBAction)doneButtonAction:(id)sender
{
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSNumber numberWithInt:self.offerVisibilityType], kOfferVisibilityType,
                          self.selectedUsersList, kUsersList, 
                          nil];
    [self.dataSelectDelegate selectDataSource:self returnedData:data];
    [self dismissModalViewControllerAnimated:YES];
}

- (UIBarButtonItem*)doneButtonItem
{
    UIBarButtonItem* doneButton =  [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(doneButtonAction:)];
    return doneButton;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = self.doneButtonItem;
    
    [self loadFollowedUsersList];

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_textured_dark"]]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.autoFetchDelegate = self;
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source & delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;  // make public cell
    } else if (section == 1) {
        return self.usersList.count;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"IDMakeOfferVisibilityCheckableCell";
    UITableViewCell *cell = nil;
    
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }

        if (indexPath.row == 0) {
            cell.textLabel.text = @"Make Public";
        
            if (self.offerVisibilityType == kOfferVisibilityType_Public) {
                cell.accessoryView = self.checkmark;
            } else {
                cell.accessoryView = nil;
            }
        }
//        else if (indexPath.row == 1) {
//            cell.textLabel.text = @"Visible to Friends";
//            
//            if (self.offerVisibilityType == kOfferVisibilityType_All) {
//                cell.accessoryView = self.checkmark;
//            } else {
//                cell.accessoryView = nil;
//            }
//        }
        
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
    } else if (indexPath.section == 1) {
        UserProfileCell *profileCell = [tableView loadCellWithClass:UserProfileCell.class];
        ProfileShort* profile = [self.usersList objectAtIndex:indexPath.row];
        profileCell.cellAction = UserProfileCellAction_Select;
        profileCell.profileData = profile;

        if ([self profileInSelectedProfiles:profile]) {
            profileCell.accessoryView = self.checkmark;         
        } else {
            profileCell.accessoryView = nil;
        }

        [tableView setImageURL:profile.picture placeholderImage:[UIImage imageNamed:@"avatar"] forCellAtIndexPath:indexPath];
        profileCell.textLabel.textColor = [UIColor whiteColor];
        
        return [CellSeparator decorateCell:profileCell withSeparatorStyle:CellSeparatorStyleDark withHeight:[self tableView:tableView heightForRowAtIndexPath:indexPath]];
    }

    return [CellSeparator decorateCell:cell withSeparatorStyle:CellSeparatorStyleDark withHeight:[self tableView:tableView heightForRowAtIndexPath:indexPath]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0.0f;
    } else {
        return 40.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 50.0f;
    } else {
        return 60.0f;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return nil;
    } else {
        return self.headerView;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            self.offerVisibilityType = kOfferVisibilityType_Public;
        } else if (indexPath.row == 1) {
            self.offerVisibilityType = kOfferVisibilityType_All;
        }
        [self.selectedUsersList removeAllObjects];
        [self.tableView reloadData];
        return;
    } else if (indexPath.section == 1) {
        self.offerVisibilityType = kOfferVisibilityType_Users;
        
        ProfileShort* profile = [self.usersList objectAtIndex:indexPath.row];
        if ([self profileInSelectedProfiles:profile]) {
            NSUInteger index = [[self.selectedUsersList valueForKey:@"userId"] indexOfObject:profile.userId];
            [self.selectedUsersList removeObjectAtIndex:(NSUInteger)index];
        } else {
            [self.selectedUsersList addObject:profile];
        }
    }
    NSIndexPath *cell1 = [NSIndexPath indexPathForRow:0 inSection:0];
//    NSIndexPath *cell2 = [NSIndexPath indexPathForRow:1 inSection:0];
    NSSet *cellsToReload = [NSSet setWithObjects:cell1, 
//                            cell2, 
                            indexPath, nil];
    [tableView reloadRowsAtIndexPaths:[cellsToReload allObjects] withRowAnimation:UITableViewRowAnimationNone];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    } else {
        return 40.0f;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [self.tableView tableView:tableView viewForFooterInSection:section];
}

#pragma mark - Auto Fetch
 - (void)autoFetchTableViewDidStart:(AutoFetchTableView *)tableView
{
    [self loadFollowedUsersList];
}

- (BOOL)autoFetchTableViewShouldStart:(AutoFetchTableView *)tableView
{
    return (self.usersList.count < self.usersTotal);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tableView scrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self.tableView scrollViewDidEndDecelerating:scrollView];    
}

@end

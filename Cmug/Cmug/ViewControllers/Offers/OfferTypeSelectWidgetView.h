//
//  OfferTypeSelectWidgetView.h
//  Cmug
//
//  Created by Dmitry C on 7/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfferTypesSlider.h"
#import "CreateOfferRequest.h"

@interface OfferTypeSelectWidgetView : UIView
@property (retain, nonatomic) IBOutlet OfferTypesSlider *offerTypesSlider;
@property (retain, nonatomic) IBOutlet UILabel *offerTypeDisplayLabel;

@property (unsafe_unretained, nonatomic) CreateOfferRequest *offerData;
@end

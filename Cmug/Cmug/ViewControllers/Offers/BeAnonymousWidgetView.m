//
//  BeAnonymousWidgetView.m
//  Cmug
//
//  Created by Dmitry C on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BeAnonymousWidgetView.h"

#define kGrayCheckmarkButtonImage @"checkmark_gray"
#define kGreenCheckmarkButtonImage @"checkmark_green"
#define LABEL_TAG 4341

@implementation BeAnonymousWidgetView
@synthesize anonymousCheckButton = _anonymousCheckButton;
@synthesize offerData = _offerData;

#pragma mark - Observers
- (void)setupDataObservers
{
    @try {
        [self.offerData removeObserver:self forKeyPath:@"isAnonymous"];
    }
    @catch (NSException *exception) {}
    
    [self.offerData addObserver:self forKeyPath:@"isAnonymous" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    self.anonymousCheckButton.selected = self.offerData.isAnonymous;
    [self updateLabel];
}

#pragma mark - 
- (void)setOfferData:(CreateOfferRequest *)offerData
{
    if (_offerData != offerData) {
        _offerData = offerData;
        self.anonymousCheckButton.selected = _offerData.isAnonymous;
        [self updateLabel];
        [self setupDataObservers];
    }
}

- (UIButton *)anonymousCheckButton
{
    if (!_anonymousCheckButton) {
        CGRect frame = CGRectMake(self.frame.size.width - 25 - 10, 10, 25, 25);
        _anonymousCheckButton = [[UIButton alloc] initWithFrame:frame];
        [_anonymousCheckButton setImage:[UIImage imageNamed:kGrayCheckmarkButtonImage] forState:UIControlStateNormal];
        [_anonymousCheckButton setImage:[UIImage imageNamed:kGreenCheckmarkButtonImage] forState:UIControlStateSelected];
        [_anonymousCheckButton addTarget:self action:@selector(checkedAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _anonymousCheckButton;
}

- (void)updateLabel
{
    UILabel *beAnonymousLabel = (UILabel *)[self viewWithTag:LABEL_TAG];
    if (self.offerData.isAnonymous) {
        // set label color to blue
        // rgb: 62 103 122
        beAnonymousLabel.textColor = [UIColor colorWithRed:62.0f/255.0f green:103.0f/255.0f blue:122.0f/255.0f alpha:1.0f];
    } else {
        // set label color to gray
        CGFloat grayLevel = 122.0f / 255.0f;
        beAnonymousLabel.textColor = [UIColor colorWithRed:grayLevel green:grayLevel blue:grayLevel alpha:1.0f];
    }
}

- (IBAction)checkedAction:(UIButton*)sender
{
    sender.selected = !sender.selected;
    self.offerData.isAnonymous = sender.selected;
}

- (void)setup
{
    UILabel *beAnonymousLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 200, 24.0f)];
    beAnonymousLabel.text = @"Be anonymous";
    CGFloat grayLevel = 122.0f / 255.0f;
    beAnonymousLabel.textColor = [UIColor colorWithRed:grayLevel green:grayLevel blue:grayLevel alpha:1.0f];
    beAnonymousLabel.backgroundColor = [UIColor clearColor];
    beAnonymousLabel.tag = LABEL_TAG;
    [self addSubview:beAnonymousLabel];
    [self addSubview:self.anonymousCheckButton];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)dealloc
{
    @try {
        [self.offerData removeObserver:self forKeyPath:@"isAnonymous"];
    }
    @catch (NSException *exception) {}
}
@end

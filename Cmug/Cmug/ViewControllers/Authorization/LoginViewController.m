//
//  LoginViewController.m
//  Cmug
//
//  Created by Dmitry C on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "LabelEditCell.h"
#import "ProfileService.h"
#import "AuthorizationManager.h"
#import "NavigationManager.h"

#import "UITableView+LoadCell.h"


@interface LoginViewController() <UITableViewDataSource, 
                                    UITextFieldDelegate>

@property (nonatomic, retain) NSArray* fields;
@end


@implementation LoginViewController
@synthesize userData = _userData;

@synthesize loginTableView;
@synthesize fields = _fields;

@synthesize returnViewController;


- (NSArray*) fields
{
    if (!_fields) {
        _fields = [NSArray arrayWithObjects:
                   @"email",
                   @"password",
                   nil];
    }
    return _fields;
}

- (LoginRequest*) userData
{
    if (!_userData) {
        _userData = [[LoginRequest alloc] init];
        _userData.email = [[AuthorizationManager sharedManager] email];
        _userData.password = [[AuthorizationManager sharedManager] password];
    }
    return _userData;
}

- (void) doLogin
{
    [self.activeTextField resignFirstResponder];
    
    [[ProfileService new] login:self.userData
                      onSuccess:^(LoginSignupResponse *responseData) {
                          NSLog(@"onSuccess: %@\n", responseData);  
                          
                          [[AuthorizationManager sharedManager] storeAuthToken:responseData.authToken expiresAt:responseData.authTokenExpires forUserWithId:responseData.userId];
                          [self navigateToReturnViewController];
                      }
                      onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                          
                          NSLog(@"onFailure: %@", errorResponse.errors);
                      }
     ];
    
}

- (void) navigateToReturnViewController
{
    if (self.returnViewController) {
        NSLog(@"Redirecting to %@", self.returnViewController);
    }
    // NOTE: Login View was opened as modal and navigation controller
    // should point to the view the login would "redirect" to
    [self dismissModalViewControllerAnimated:YES];
    
}

- (BOOL) shouldAddTextFieldDoneButton
{
    // don't override "Done" button in the right corner
    return NO;
}


#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self.userData setValue:textField.text forKey:[self.fields objectAtIndex:textField.tag-INPUT_TAG_INDEX_OFFSET]];

    NSLog(@"%@", textField.text);
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [self.userData setValue:textField.text forKey:[self.fields objectAtIndex:(NSUInteger) textField.tag-INPUT_TAG_INDEX_OFFSET]];
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark PrevNextDone Accessory

- (UIView*) textFieldsSuperView
{
    return self.loginTableView;
}

- (int) maxTabIndexForTextInputs
{
    return self.fields.count;
}


- (void) accessoryView:(PrevNextDoneAccessoryView *)view didClick:(UIBarButtonItem *)sender {
    if (sender.tag == DONE_BUTTON_TAG)
    {
        [self.activeTextField resignFirstResponder];
        [self doLogin];
    }
}


#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (int) maxTabIndex 
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *LabelTextCellIdentifier = @"IDLabelTextCell";
    LabelEditCell *cell = [tableView loadCellNamed:@"LabelEditCell"];
    cell.input.tag = indexPath.row+INPUT_TAG_INDEX_OFFSET;
    cell.input.delegate = self;
    cell.input.inputAccessoryView = self.keyboardAccesoryView;
    
    if (indexPath.row == 0) {
        // init email text field
        cell.label.text = @"Email";
        cell.input.text = [self.userData email];
    } else if (indexPath.row == 1) {
        // init password text field
        cell.label.text = @"Password";        
        cell.input.secureTextEntry = YES;
        cell.input.text = [self.userData password];
    }

    if (indexPath.row == self.tabIndex) {
        [cell.input becomeFirstResponder];
    }
    
    return cell;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.requiresAuthorization = NO;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.loginTableView.backgroundColor = [UIColor clearColor];
    if (!self.navigationItem.rightBarButtonItem) {
        
        UIBarButtonItem * doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" 
                                                                        style:UIBarButtonItemStylePlain 
                                                                       target:self 
                                                                       action:@selector(doLogin)];
        self.navigationItem.rightBarButtonItem = doneButton;
    }
    
    self.navigationItem.title = @"Login";
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.loginTableView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (BOOL)allowEditingScrolling
{
    return NO;
}

- (BOOL)allowFreeScrolling
{
    return NO;
}
@end

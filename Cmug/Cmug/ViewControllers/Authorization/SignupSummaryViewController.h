//
//  SignupSummaryViewController.h
//  Cmug
//
//  Created by Dmitry C on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupRequest.h"
#import "MultiInputsViewController.h"


@interface SignupSummaryViewController : MultiInputsViewController

@property (nonatomic, retain) SignupRequest *userData;

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *profileDataTableView;

@property (nonatomic, retain) NSDictionary *fieldLabels;
@property (nonatomic, retain) NSArray *fields;
@end

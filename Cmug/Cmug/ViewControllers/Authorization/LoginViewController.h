//
//  LoginViewController.h
//  Cmug
//
//  Created by Dmitry C on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginRequest.h"
#import "MultiInputsViewController.h"

@interface LoginViewController : MultiInputsViewController
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *loginTableView;

@property (retain, nonatomic) UIViewController *returnViewController;

//dataSource
@property (nonatomic, retain) LoginRequest* userData;

- (void) navigateToReturnViewController;
@end

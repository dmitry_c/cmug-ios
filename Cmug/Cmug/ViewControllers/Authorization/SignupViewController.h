//
//  SignupViewController.h
//  Cmug
//
//  Created by Dmitry C on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlexibleViewController.h"

@interface SignupViewController : FlexibleViewController 

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *welcomeText;

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *facebookSignupButton;

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *twitterSignupButton;

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *emailSignupButton;

@end

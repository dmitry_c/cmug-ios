//
//  SignupSummaryViewController.m
//  Cmug
//
//  Created by Dmitry C on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SignupSummaryViewController.h"
#import "LabelEditCell.h"
#import "LabelImageCell.h"
#import "PrevNextDoneAccessoryView.h"
#import "ProfileService.h"
#import "CameraViewController.h"
#import "AuthorizationManager.h"
#import "NSString+Validators.h"
#import "ResourceStrings.h"
#import "UIAlertView+Additions.h"


@interface SignupSummaryViewController() <UITableViewDataSource,
                                            UITextFieldDelegate,
                                            CameraViewControllerDelegate>

@end


@implementation SignupSummaryViewController

@synthesize profileDataTableView;
@synthesize userData = _userData;
@synthesize fieldLabels = _fieldLabels;
@synthesize fields = _fields;

- (NSArray*) fields
{
    if (!_fields) {
    _fields = [NSArray arrayWithObjects:
               @"firstName",
               @"lastName",
               @"email",
               @"password",
               @"phone",
               @"picturePath",
               nil];
    }

    return _fields;
}

- (NSDictionary*) fieldLabels
{
    if (!_fieldLabels) {
    // keys are SignupRequest properties
    NSArray *labels = [NSArray arrayWithObjects: 
                       @"First name",
                       @"Last name",
                       @"Email",
                       @"Password",
                       @"Phone",
                       @"Photo", 
                       nil];
    
    _fieldLabels = [NSDictionary dictionaryWithObjects: labels
                                               forKeys: _fields]; 
    }
    return _fieldLabels;
}

- (SignupRequest*) userData
{
    if (!_userData) {
        _userData = [[SignupRequest alloc] init];
    }
    return _userData;
}




- (BOOL) validate
{
    if (![self.userData.email isValidEmailAndNotFacebookProxy]) {
        [UIAlertView showWithTitle:ALERT_WarningTitle andMessage:ALERT_PleaseEnterValidEmail];
        return NO;
    }
    
    if (![self.userData.password length]) {
        [UIAlertView showWithTitle:ALERT_WarningTitle andMessage:ALERT_PleaseEnterPassword];
        return NO;
    }
    
    return YES;
}

- (BOOL) shouldAddTextFieldDoneButton
{
    // don't override "Done" button in the right corner
    return NO;
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//	self.activeTextField = textField;
////    self.tabIndex = textField.tag;
//	return;
//}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self.userData setValue:textField.text forKey:[self.fields objectAtIndex:textField.tag-INPUT_TAG_INDEX_OFFSET]];
    
    NSLog(@"%@", textField.text);
	return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}


- (UIView*) textFieldsSuperView
{
    return self.profileDataTableView;
}

- (int) maxTabIndexForTextInputs
{
    return self.fields.count - 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.fields.count;
}


- (void)handleImageRowTap:(UITapGestureRecognizer *)recognizer
{
//   CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    CameraViewController* cameraViewController = 
        [[CameraViewController alloc] initWithNibName:@"CRCameraViewController"
                                                 bundle:nil];
    
//    [cameraViewController setMode:CRCameraViewControllerModeTakePhoto];
    [cameraViewController setDelegate:self];
    
//    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:cameraViewController];
    [self presentModalViewController:cameraViewController animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *LabelTextCellIdentifier = @"IDLabelTextCell";
    static NSString *LabelImageCellIdentifier = @"IDLabelImageCell";

    if ([tableView respondsToSelector:@selector(registerNib:forCellReuseIdentifier:)]) {
        [tableView registerNib:[UINib nibWithNibName:@"LabelEditCell" bundle:nil] forCellReuseIdentifier:LabelTextCellIdentifier];
        [tableView registerNib:[UINib nibWithNibName:@"LabelImageCell" bundle:nil] forCellReuseIdentifier:LabelImageCellIdentifier];
    }
    
    NSString *fieldName = [self.fields objectAtIndex:indexPath.row];
    id cell=nil;
    
    if (indexPath.row >= 0 && indexPath.row < 5) {
        // init text fields
        cell = (LabelEditCell*)[tableView dequeueReusableCellWithIdentifier:LabelTextCellIdentifier];

        if (cell == nil) {
            cell = [LabelEditCell loadFromNib];
//            cell = [[LabelEditCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                    reuseIdentifier:LabelTextCellIdentifier];
        }
        [((LabelEditCell*) cell) input].inputAccessoryView = self.keyboardAccesoryView;
        [((LabelEditCell*) cell) input].delegate = self;
        [((LabelEditCell*) cell) input].tag = indexPath.row + INPUT_TAG_INDEX_OFFSET;

        if (fieldName == @"email") {
            [[((LabelEditCell*) cell) input] setPlaceholder:@"example@example.com"];
            [((LabelEditCell*) cell) input].keyboardType = UIKeyboardTypeEmailAddress;
        }

        if (fieldName == @"password") {
            [((LabelEditCell*) cell) input].secureTextEntry = YES;
        }
            
        if (fieldName == @"phone") {
            [[((LabelEditCell*) cell) input] setPlaceholder:@"optional"];
            [((LabelEditCell*) cell) input].keyboardType = UIKeyboardTypePhonePad;
        }
        
        if (self.userData) {
            // Show user data retrieved through facebook or twitter 
            [[((LabelEditCell*) cell) input] setText: [self.userData valueForKey:fieldName]];
        }

//        if (indexPath.row == self.tabIndex) {
//            [[((LabelEditCell*) cell) input] becomeFirstResponder];
//        }
    } else {
        // init user picture 
        cell = (LabelImageCell*)[tableView dequeueReusableCellWithIdentifier:LabelImageCellIdentifier];
        
        if (cell == nil) {
//            cell = [[LabelImageCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                        reuseIdentifier:LabelImageCellIdentifier];
            cell = [LabelImageCell loadFromNib];

            // Attach image tapped event
            UITapGestureRecognizer *singleFingerTap = 
            [[UITapGestureRecognizer alloc] initWithTarget:self 
                                                    action:@selector(handleImageRowTap:)];
            [cell addGestureRecognizer:singleFingerTap];
        }
        
        if (self.userData) {
            // Show user image TODO:
//            [[((LabelImageCell*) cell) image] loadUrl: [self.userData valueForKey:fieldName]];
        }
        
    }
    

    ((LabelEditCell*)cell).label.text = [self.fieldLabels valueForKey:fieldName];
    
    return cell;
}


- (void) setupView
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.profileDataTableView.backgroundColor = [UIColor clearColor];

    if (!self.navigationItem.rightBarButtonItem) {

        UIBarButtonItem * doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" 
                                                                        style:UIBarButtonItemStylePlain 
                                                                       target:self 
                                                                       action:@selector(doneClick:)];
        self.navigationItem.rightBarButtonItem = doneButton;
    }
    
    self.navigationItem.title = @"Signup";

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.requiresAuthorization = NO;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Events

- (IBAction)doneClick:(id)sender {
    [self.activeTextField resignFirstResponder];
    
    if (![self validate]) return;
     
    [[ProfileService new] signup:self.userData 
                       onSuccess:^(LoginSignupResponse *responseData) {
                           NSLog(@"onSuccess: %@\n", responseData);    
                           
                           // Store auth token for user from signup response
                           [[AuthorizationManager sharedManager] storeAuthToken:[responseData authToken] 
                                                                      expiresAt:[responseData authTokenExpires] 
                                                                  forUserWithId:[responseData userId]];
                           
                           // Store login/password from userData
                           [[AuthorizationManager sharedManager] storeLogin:[self.userData email] andPassword:[self.userData password]];
                           
                           NSLog(@"Redirect to posts");
                           [self dismissModalViewControllerAnimated:YES];
                       }
                       onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                           NSLog(@"onFailure: %@", errorResponse.errors);
                           // TODO: activate input?
                       }
     ];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self setupKeyboardListeners];
//    _tabIndex = -1; //The number of auto-focused textfield
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupView];    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.userData = nil;
    self.profileDataTableView = nil;
//    [self removeKeyboardListeners];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - CameraViewController delegate
- (void) cameraDidCancel:(id)cameraViewController
{
    [cameraViewController dismissModalViewControllerAnimated:YES];
}

- (void) camera:(id)cameraViewController didFinishWithImage:(MediaObject *)cameraObject
{
    [cameraViewController dismissModalViewControllerAnimated:YES];
    NSLog(@"TODO: Set Picture");
}

@end

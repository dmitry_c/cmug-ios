//
//  SignupViewController.m
//  Cmug
//
//  Created by Dmitry C on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "LoginViewController.h"
#import "SignupViewController.h"
#import "SignupSummaryViewController.h"
#import "ResourceStrings.h"
#import "Restkit/JSONKit.h"
#import "IIViewDeckController.h"
#import "MBProgressHUD.h"
// IOS_5
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "AppDelegate.h"
#import "SHK.h"
#import "SHKFacebook.h"
#import "SHKTwitter.h"
#import "SHKSharerDelegate.h"
#import "TwitterWrapper.h"

@interface SignupViewController() <SHKSharerDelegate, UIAlertViewDelegate>
@property (nonatomic, retain) SignupRequest* userData;
@property (nonatomic, retain) TwitterWrapper* ios5Twitter;
@end


@implementation SignupViewController

@synthesize welcomeText;
@synthesize facebookSignupButton;
@synthesize twitterSignupButton;
@synthesize emailSignupButton;
@synthesize userData = _userData;
@synthesize ios5Twitter = _ios5Twitter;

- (TwitterWrapper *)ios5Twitter
{
    if (!_ios5Twitter) {
        _ios5Twitter = [[TwitterWrapper alloc] init];
    }
    return _ios5Twitter;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.requiresAuthorization = NO;
    }
    return self;
}


- (void) setupView
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


- (void) pushSignupSummaryViewController:(SignupRequest*)userData
{
    SignupSummaryViewController *vc = [[SignupSummaryViewController alloc] initWithNibName:@"SignupSummaryViewController" bundle:nil];
    if (userData) {
        vc.userData = userData;
    }

//    LoginViewController *lvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    
    if (self.navigationController) {
        [self.navigationController pushViewController:vc animated:YES];
    }
//    else
//        [self presentModalViewController: vc animated:YES];

}

#pragma mark -
#pragma mark SHKSharer delegate

- (void)sharerStartedSending:(SHKSharer *)sharer {}

- (void)sharerFinishedSending:(SHKSharer *)sharer {}

- (void)sharer:(SHKSharer *)sharer failedWithError:(NSError *)error shouldRelogin:(BOOL)shouldRelogin
{
    if (shouldRelogin) {
        // Logout and repeat actions
        [sharer.class logout];
        
            if ([sharer isMemberOfClass:SHKFacebook.class]) {
                [self signupWithFacebook];
            } else if ([sharer isMemberOfClass:SHKTwitter.class]) {
                [self signupWithTwitter];
            }
        return;
    }
    
    NSString *title=nil;
    NSString *message=nil;
    
    if ([sharer isMemberOfClass:SHKFacebook.class]) {
        title = ALERT_FacebookErrorTitle;
        message = ALERT_FacebookErrorMessage;
    }

    UIAlertView *av = [[UIAlertView alloc] initWithTitle:title
                                                 message:message
                                                delegate:nil 
                                       cancelButtonTitle:@"Ok" 
                                       otherButtonTitles:nil];
    [av show];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

}

- (void)sharerCancelledSending:(SHKSharer *)sharer
{
    CRLog(@"Cancelled %@", sharer);
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)sharerShowBadCredentialsAlert:(SHKSharer *)sharer 
{
    CRLog(@"Bad credentials %@", sharer);
}

- (void)sharerShowOtherAuthorizationErrorAlert:(SHKSharer *)sharer 
{
    CRLog(@"Authorization Error %@", sharer);
}

- (void) sharerAuthDidFinish:(SHKSharer *)sharer success:(BOOL)success
{
    if (!success) {
        // Cancel pressed in app authorization window
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
}


- (void) gotUserInfo:(NSNotification*)aNotification
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:@"SHKSendDidFinish" 
                                                  object:nil];
    
    if ([aNotification.object isMemberOfClass: SHKFacebook.class]) {
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSHKFacebookUserInfo"];
        _userData = [SignupRequest makeFromFacebookData:userInfo];

    } else if ([aNotification.object isMemberOfClass: SHKTwitter.class]){
        // this branch if only for ios4
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSHKTwitterUserInfo"];
        _userData = [SignupRequest makeFromTwitterData:userInfo];

    }
    
    [self pushSignupSummaryViewController:self.userData];
}

#pragma mark -


- (void) signupWithFacebook
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(gotUserInfo:) 
                                                 name:@"SHKSendDidFinish" 
                                               object:nil];

    SHKSharer *sharer = [SHKFacebook getUserInfo];
    sharer.shareDelegate = self;
}


- (void) didCancel
{
    [self.navigationController popViewControllerAnimated:YES];
}

//
// Due to lack of method in ShareKit for SHKShareTypeUserInfo
// in Twitter ios5 sharer we need to fetch user data
// using Accounts framework
// 
- (void) signupWithTwitterIOS5
{
    [self.ios5Twitter account:^(ACAccount *account) {
        if (account) {
            // Build a twitter request
            TWRequest *userInfoRequest = [[TWRequest alloc] initWithURL:
                                          [NSURL URLWithString:@"https://api.twitter.com/1/account/verify_credentials.json"]
                                                             parameters:nil 
                                                          requestMethod:TWRequestMethodGET];
            // Post the request
            [userInfoRequest setAccount:account];
            
            // Block handler to manage the response
            [userInfoRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) 
             {
                 _userData = [SignupRequest makeFromTwitterData:[responseData objectFromJSONData]];
                 [self pushSignupSummaryViewController:self.userData];
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             }];    
        } else {
            // hide progress
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            UIAlertView * av = [[UIAlertView alloc] initWithTitle:ALERT_NoTwitterAccountsTitle 
                                                          message:ALERT_NoTwitterAccountsMessage
                                                         delegate:self 
                                                cancelButtonTitle:@"Close" 
                                                otherButtonTitles:nil];
            [av show];
        }
        
    }];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self didCancel];
}

- (void) signupWithTwitter
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if ([TWTweetComposeViewController class] == nil) {
        // Use ShareKit
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(gotUserInfo:) 
                                                     name:@"SHKSendDidFinish" 
                                                   object:nil];
        SHKSharer *sharer = [SHKTwitter getUserInfo];
        sharer.shareDelegate = self;
    } else {
        // Sharekit doesn't provide a method to get user data 
        // usign twitter ios5 framework
        [self signupWithTwitterIOS5];
    }
    
}

- (void) signupWithEmail
{
    //show email signup view with empty user data
    [self pushSignupSummaryViewController:self.userData];
}

- (IBAction)socialSignupClick:(UIButton*)sender 
{
    if (sender.tag == 1) {
        [self signupWithFacebook];
    } else if (sender.tag == 2) {
        [self signupWithTwitter];
    } else if (sender.tag == 3) {
        [self signupWithEmail];
    }
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self setupView];
}

- (void)viewDidUnload
{
    [self setFacebookSignupButton:nil];
    [self setTwitterSignupButton:nil];
    [self setEmailSignupButton:nil];
    [self setWelcomeText:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

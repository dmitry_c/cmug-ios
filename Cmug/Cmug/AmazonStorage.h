//
//  AmazonStorage.h
//  Crowd-iOS
//
//  Created by Skeeet on 3/31/11.
//  Copyright 2011 Munky Interactive / munkyinteractive.com. All rights reserved.
//	File created using Singleton XCode Template by Mugunth Kumar (http://mugunthkumar.com
//  Permission granted to do anything, commercial/non-commercial with this file apart from removing the line/URL above

#import <Foundation/Foundation.h>
#import "ASIS3Bucket.h"
#import "ASIS3Request.h"
#import "ASIS3BucketObject.h"
#import "ASIS3ObjectRequest.h"
#import "ASINetworkQueue.h"
#import "AmazonObject.h"
#import "CRMediaSettings.h"


//#define ACCESSKEY	@"AKIAIGD5F2QOT7DC644A"
//#define SECRETKEY	@"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"
//#define BUCKET	@"crowdtest"

#define PHOTOCDNSERVER	@"http://d37xqhvz1n20sa.cloudfront.net/photos"
#define VIDEOCDNSERVER	@"http://d21mqnbomfeymz.cloudfront.net/video-out"
///photos/photo_15_0.jpg

@interface AmazonStorage : NSObject 
{

}

//will keep upload operations
@property(retain,nonatomic) ASINetworkQueue * uploadQueue;

//will keep download operations
@property(retain,nonatomic) ASINetworkQueue * downloadQueue;

//will keep url-disk file pairs
@property(retain,nonatomic) NSMutableDictionary * uploadObjects;

//will keep url-disk file pairs
@property(retain,nonatomic) NSMutableDictionary * downloadObjects;

@property(assign,nonatomic) id delegate;
@property(assign,nonatomic) SEL requestCompletedSelector;
@property(assign,nonatomic) SEL requestFailedSelector;
@property(assign,nonatomic) SEL uploadProgressSelector;


@property(retain,nonatomic) NSMutableArray * downloadsInProgressRequests;

+ (AmazonStorage*) sharedStorage;

//S3 related
-(ASIS3ObjectRequest*) beginUpload:(AmazonObject*) object progressIndicator:(id)progressIndicator mediaSettings:(CRMediaSettings*)mediaSettings;

//CloudFront related
-(NSURL*)makeCDNUrl:(NSString*)filepath;
-(void) beginnDownloadFromCDN:(NSString*)object;

//used for photo download in media browser
-(ASIHTTPRequest*) beginnDownloadFromCDNWithLink:(NSString*)object delegate:(id)requestDelegate onSuccess:(SEL)onSuccessSelector onFail:(SEL)onFailSelector;


@end

//
//  UIAlertView+Additions.m
//  Cmug
//
//  Created by Dmitry C on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIAlertView+Additions.h"

@implementation UIAlertView (Additions)

+ (UIAlertView*) showWithTitle:(NSString*) title andMessage:(NSString*) message
{
    UIAlertView* alert = ALERT(title, message);
    [alert show];
    return alert;
}

+ (UIAlertView*) showErrorResponse:(ErrorResponse*) e
{
    id errors=nil;
    if (e.errors) {
        errors = e.errors;
    } else if (e.error) {
        errors = e.error;
    }
    
    return [self showErrors:errors];
}


+ (UIAlertView*) showErrors:(id)errors
{
    // The errors may be one of dictionary, array of strings or string.
    //
    // Dictionary example:
    // {"password": ["Ensure this value has at least 6 characters (it has 1)."]}

    NSString* title = [NSString stringWithString:@"Warning"];
    NSMutableString *messages=[NSMutableString stringWithString:@""];
    if ([errors isKindOfClass:NSString.class]) {
        [messages appendString:errors];
    } else if ([errors isKindOfClass: NSArray.class]) {
        [messages appendString:[((NSArray*)errors) componentsJoinedByString:@""]];
    } else if ([errors isKindOfClass: NSDictionary.class]) {
        [errors enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            if ([obj isKindOfClass: NSArray.class]) {
                NSString* title = (NSString*) key;
                NSString* msg = (NSString*)[(NSArray*)obj objectAtIndex:0];
                NSString* message = [NSString stringWithFormat:@"%@:\n%@\n", title, msg];
                [messages appendString:message];
            } else if ([obj isKindOfClass: NSString.class]) {
                NSString* message = [NSString stringWithFormat:@"%@:\n%@\n", key, obj];
                [messages appendString:message];
            }
        }];
    }

    NSString* s = messages;
    UIAlertView* alert = ALERT(title, s);
    [alert show];
    return alert;
}
@end

//
//  UIAlertView+Additions.h
//  Cmug
//
//  Created by Dmitry C on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ErrorResponse.h"

#define ALERT(_title,_text) 		[[UIAlertView alloc] initWithTitle:_title message:_text delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]


@interface UIAlertView (Additions)
+ (UIAlertView*) showWithTitle:(NSString*) title andMessage:(NSString*) message;
+ (UIAlertView*) showErrors:(id)errors;
+ (UIAlertView*) showErrorResponse:(ErrorResponse*) e;

@end

//
//  ApiClient.m
//  Cmug
//
//  Created by Dmitry C on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ApiClient.h"
#import "Settings.h"
#import "UIDevice+IdentifierAddition.h"
#import "AuthorizationManager.h"
#import "RestKit/RKObjectMapper.h"

@implementation ApiClient

- (void) configureRequest:(RKRequest *)request 
{
    [super configureRequest:request];
    NSString *token = [[AuthorizationManager sharedManager] authToken];
    //[[NSUserDefaults standardUserDefaults] valueForKey:AUTH_TOKEN_USERDEFAULTS_KEY_NAME];
    NSString *deviceId = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSDictionary *headers = nil;
    if (token && token.length) {
        headers = [NSDictionary dictionaryWithKeysAndObjects:
                                 AUTH_TOKEN_HEADER_NAME, token, 
                                 CLIENT_DEVICE_ID_HEADER_NAME, deviceId,
                                 nil];
    } else {
        headers = [NSDictionary dictionaryWithKeysAndObjects:
                                 CLIENT_DEVICE_ID_HEADER_NAME, deviceId,
                                 nil];
        
    }
    request.additionalHTTPHeaders = headers; 
}


@end



@implementation ApiObjectManager

- (void) configureRequest:(RKRequest *)request
{
    [super configureRequest:request];
    NSString *token = [[AuthorizationManager sharedManager] authToken];
    //[[NSUserDefaults standardUserDefaults] valueForKey:AUTH_TOKEN_USERDEFAULTS_KEY_NAME];
    NSString *deviceId = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSDictionary *headers = nil;
    if (token && token.length) {
        headers = [NSDictionary dictionaryWithKeysAndObjects:
                   AUTH_TOKEN_HEADER_NAME, token, 
                   CLIENT_DEVICE_ID_HEADER_NAME, deviceId,
                   nil];
    } else {
        headers = [NSDictionary dictionaryWithKeysAndObjects:
                   CLIENT_DEVICE_ID_HEADER_NAME, deviceId,
                   nil];
        
    }
    request.additionalHTTPHeaders = headers; 
}


- (id)initWithBaseURL:(RKURL *)baseURL {
    self = [super initWithBaseURL:baseURL];
    if (self) {
        // overriding client
//        self.client = [[ApiClient alloc] initWithBaseURL:baseURL];
        self.client = [ApiClient clientWithBaseURL:baseURL];
        self.acceptMIMEType = RKMIMETypeJSON;
        
        [ApiObjectManager setSharedManager:self];
        [ApiClient setSharedClient:self.client];

        if (nil == [RKObjectManager sharedManager]) {
            NSAssert([RKObjectManager sharedManager], @"SharedManager hasn't been properly set");
            
        }
    }
    
    return self;
}


@end

//
//  ApiClient.h
//  Cmug
//
//  Created by Dmitry C on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface ApiClient : RKClient
- (void) configureRequest:(RKRequest *)request;
@end


@interface ApiObjectManager : RKObjectManager
- (id)initWithBaseURL:(RKURL *)baseURL;
@end
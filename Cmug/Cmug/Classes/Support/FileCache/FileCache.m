//
//  FileCache.m
//  Crowd-iOS
//
//  Created by Skeeet on 3/31/11.
//  Copyright 2011 Munky Interactive / munkyinteractive.com. All rights reserved.


#import "FileCache.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIImage+Resize.h"

#define DATE_LAST_CLEARING_KEY @"DATE_LAST_CLEARING_KEY"
#define CACHE_CLEARING_INTERVAL 2 * 24 * 60 * 60 // 2 days

static FileCache *_instance;
@implementation FileCache

+ (FileCache*)sharedCache
{
	@synchronized(self) {
		
        if (_instance == nil) 
		{	
            _instance = [[super allocWithZone:NULL] init];   
        }
    }
    return _instance;
}

+ (NSString*)fileAdditional:(FileCacheImageSize)imageSize
{
	NSString* result = @"";
	switch (imageSize)
	{
		case FileCacheImageSize49x41:
			result = @"_49x41";
			break;
		case FileCacheImageSize230x175:
			result = @"_230x175";
			break;
		//case FileCacheImageSize86x86:
		//	result = @"_86x86";
		//	break;
		default:
			break;
	}
	return result;
}

#pragma mark Singleton Methods

+ (id)allocWithZone:(NSZone *)zone
{	
	return [[self sharedCache]retain];	
}


- (id)copyWithZone:(NSZone *)zone
{
    return self;	
}

- (id)retain
{	
    return self;	
}

- (unsigned)retainCount
{
    return NSUIntegerMax;  //denotes an object that cannot be released
}

- (void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;	
}

- (id) init
{
	self = [super init];
	if (self != nil) 
	{
		thumbnailsCache = [[NSMutableDictionary alloc] init];
		
		manager = [NSFileManager defaultManager];
		if([manager fileExistsAtPath:[self cacheDirectory]])
		{
			//[manager removeItemAtPath:[self cacheDirectory] error:nil];
		}
		//create chache directories
		[manager createDirectoryAtPath:[self cacheDirectory] withIntermediateDirectories:YES attributes:nil error:nil];
		[manager createDirectoryAtPath:[[self cacheDirectory] stringByAppendingPathComponent:@"/photos"] withIntermediateDirectories:YES attributes:nil error:nil];
		[manager createDirectoryAtPath:[[self cacheDirectory] stringByAppendingPathComponent:@"/videos"] withIntermediateDirectories:YES attributes:nil error:nil];
		

	}
	return self;
}

#pragma mark -
#pragma mark directory getters
-(NSString*) documentsDirectory
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return documentsDirectory;
}

-(NSString*) cacheDirectory
{
	return [[self documentsDirectory] stringByAppendingPathComponent:@"/cache"];
}


#pragma mark -
#pragma mark content management

-(id) getFullPathFromCache:(NSString*)objectName imageSize:(FileCacheImageSize)imageSize
{
	return [NSString stringWithFormat:@"%@%@", [self getFullPathFromCache:objectName], [FileCache fileAdditional:imageSize]];
}

-(id) getFullPathFromCache:(NSString*)objectName
{
	return [[self cacheDirectory] stringByAppendingPathComponent:objectName];
}

-(BOOL) putToCache:(id) object name:(NSString*)objectName
{
	return [self putToCache:object name:objectName imageSize:FileCacheImageSizeFull];
}

-(BOOL) putToCache:(id) object name:(NSString*)objectName imageSize:(FileCacheImageSize)imageSize
{
	BOOL result = NO;
	if([object isKindOfClass:[NSString class]])		// we suppose that it is path to video
	{
		NSError *err;
		if([manager fileExistsAtPath:[[self cacheDirectory] stringByAppendingPathComponent:objectName]])
		{
			[manager removeItemAtPath:[[self cacheDirectory] stringByAppendingPathComponent:objectName] error:nil];
		}

		result = [manager moveItemAtURL:[NSURL URLWithString:object] 
								  toURL:[NSURL fileURLWithPath:[[self cacheDirectory] stringByAppendingPathComponent:objectName]] 
								  error:&err];
		if(!result)
		{
			CRLog(@"%@",err);
		}
	}
	else if([object isKindOfClass:[UIImage class]])	//it is uiimage
	{
		NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
		
		NSData * imageData = UIImageJPEGRepresentation(object, 1.0);
		result = [imageData writeToFile:[[self cacheDirectory] stringByAppendingPathComponent:objectName] atomically:YES];
		imageData = nil;
		[pool release];
		
		pool = [[NSAutoreleasePool alloc] init];
		UIImage* image = nil;
		switch (imageSize)
		{
			case FileCacheImageSize49x41:
				image = [object resizedImageWithContentMode:UIViewContentModeScaleAspectFill
													 bounds:CGSizeMake(49, 41)
									   interpolationQuality:kCGInterpolationHigh];
				break;
			case FileCacheImageSize230x175:
				image = [object resizedImageWithContentMode:UIViewContentModeScaleAspectFill
													 bounds:CGSizeMake(230, 175)
									   interpolationQuality:kCGInterpolationHigh];
				break;
			//case FileCacheImageSize86x86:
			//	image = [object resizedImageWithContentMode:UIViewContentModeScaleAspectFill
			//										 bounds:CGSizeMake(86, 86)
			//						   interpolationQuality:kCGInterpolationHigh];
			//	break;
			default:
				break;
		}
		
		if(image)
		{
			objectName = [NSString stringWithFormat:@"%@%@", objectName, [FileCache fileAdditional:imageSize]];
			imageData = UIImageJPEGRepresentation(image, 1.0);
			result = [imageData writeToFile:[[self cacheDirectory] stringByAppendingPathComponent:objectName] atomically:YES];
		}
		[pool release];
	}
	
	return result;
}

-(id) getFromCache:(NSString*)objectName
{
	//CRLog(@"%@",objectName);
	if([manager fileExistsAtPath:[[self cacheDirectory] stringByAppendingPathComponent:objectName]])
	{
		if([[objectName pathExtension] isEqualToString:@"mov"])
		{
			return [[self cacheDirectory] stringByAppendingPathComponent:objectName];
		}
		else 
		{
			UIImage * cachedImage = [UIImage imageWithContentsOfFile:[[self cacheDirectory] stringByAppendingPathComponent:objectName]];
			return cachedImage;
		}
	}
	
	return nil;	
}

-(void) purgeThumbnailsCache
{
	[thumbnailsCache removeAllObjects];
}

- (void)clearCacheIfNeeded
{
	if([[NSUserDefaults standardUserDefaults] objectForKey:DATE_LAST_CLEARING_KEY])
	{
		NSDate* lastClearing = [[NSUserDefaults standardUserDefaults] objectForKey:DATE_LAST_CLEARING_KEY];
		
		if(-[lastClearing timeIntervalSinceNow] > CACHE_CLEARING_INTERVAL) // we must clear cache
		{
			[[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:DATE_LAST_CLEARING_KEY];
			[[NSUserDefaults standardUserDefaults] synchronize];
			
			NSFileManager* fm = [[[NSFileManager alloc] init] autorelease];
			NSDirectoryEnumerator* en = [fm enumeratorAtPath:[self cacheDirectory]];    
			NSError* err = nil;
			BOOL res;
			
			NSString* file;
			while (file = [en nextObject])
			{
				res = [fm removeItemAtPath:[[self cacheDirectory] stringByAppendingPathComponent:file] error:&err];
				if (!res && err)
				{
					CRLog(@"oops: %@", err);
				}
			}
		}
	}
	else
	{
		[[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:DATE_LAST_CLEARING_KEY];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

@end

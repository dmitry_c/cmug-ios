//
//  FileCache.h
//  Crowd-iOS
//
//  Created by Skeeet on 3/31/11.
//  Copyright 2011 Munky Interactive / munkyinteractive.com. All rights reserved.


#import <UIKit/UIKit.h>

#define THUMBNAIL_WIDTH 66	
#define THUMBNAIL_HEIGHT 65
//#define THUMBNAIL_PREFIX @"thumbnail"

typedef enum
{
	FileCacheImageSizeFull,
	FileCacheImageSize230x175,
	//FileCacheImageSize86x86,
	FileCacheImageSize49x41,
}FileCacheImageSize;

@interface FileCache : NSObject 
{
	NSFileManager * manager;
	NSMutableDictionary * thumbnailsCache;
}

+ (FileCache*) sharedCache;
+ (NSString*)fileAdditional:(FileCacheImageSize)imageSize;

-(NSString*) documentsDirectory;
-(NSString*) cacheDirectory;

-(BOOL) putToCache:(id) object name:(NSString*)name;
-(BOOL) putToCache:(id) object name:(NSString*)name imageSize:(FileCacheImageSize)imageSize;
-(id) getFromCache:(NSString*)objectName;
-(id) getFullPathFromCache:(NSString*)objectName;
-(id) getFullPathFromCache:(NSString*)objectName imageSize:(FileCacheImageSize)imageSize;

//thumbnails related code
-(void) purgeThumbnailsCache;
- (void)clearCacheIfNeeded;

@end

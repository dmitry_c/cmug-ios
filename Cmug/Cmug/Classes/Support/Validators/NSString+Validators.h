//
//  NSString+Validators.h
//  Cmug
//
//  Created by Dmitry C on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validators)
- (BOOL) isValidEmail;
- (BOOL) isValidEmailStrict;
- (BOOL) isValidEmailAndNotFacebookProxy;
@end

//
//  NSString+Validators.m
//  Cmug
//
//  Created by Dmitry C on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSString+Validators.h"

#define FB_EMAIL_PROXY_DOMAIN   @"@facebookappmail.com"


@implementation NSString (Validators)

-(BOOL) NSStringIsValidEmail:(NSString *)checkString strict:(BOOL)stricterFilter
{
    // "Strict" Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL) isValidEmail
{
    return [self NSStringIsValidEmail:self strict:NO];
}

- (BOOL) isValidEmailStrict
{
    return [self NSStringIsValidEmail:self strict:YES];    
}

- (BOOL) isValidEmailAndNotFacebookProxy
{
    return [self isValidEmail] && ![self hasSuffix:FB_EMAIL_PROXY_DOMAIN];
}

@end

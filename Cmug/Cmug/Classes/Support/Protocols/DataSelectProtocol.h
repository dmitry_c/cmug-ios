//
//  DataSelectProtocol.h
//  Cmug
//
//  Created by Dmitry C on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataSelectProtocol <NSObject>
- (void)selectDataSource:(id)source returnedData:(id)data;
@end

//
//  AddressBookWrapper.m
//  Cmug
//
//  Created by Dmitry C on 6/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AddressBookWrapper.h"
#import "ContactRecord.h"

static AddressBookWrapper* instance;

@implementation AddressBookWrapper
@synthesize imageCache = _imageCache;

- (NSCache *)imageCache
{
    if (!_imageCache) {
        _imageCache = [[NSCache alloc] init];
        [_imageCache setName:@"ABContactImageCache"];
    }
    return _imageCache;
}

+ (AddressBookWrapper*) sharedInstance
{
    @synchronized(self) {
        if (!instance) {
            instance = [[AddressBookWrapper alloc] init];
        }
    }
    return instance;
}

+ (ABAddressBookRef) addressBook
{
    ABAddressBookRef abRef = ABAddressBookCreate();
    return abRef;
}

+ (NSArray *) contacts
{
    ABAddressBookRef addressBook = ABAddressBookCreate();
    CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);

    NSMutableArray *contactsList = [NSMutableArray arrayWithCapacity:CFArrayGetCount(people)];
    
    for (CFIndex i = 0; i < CFArrayGetCount(people); i++) {
        ABRecordRef person = CFArrayGetValueAtIndex(people, i);
        
        ABRecordID recordID = ABRecordGetRecordID(person);
        CFTypeRef fnameProperty = ABRecordCopyValue(person, kABPersonFirstNameProperty);
        CFTypeRef lnameProperty = ABRecordCopyValue(person, kABPersonLastNameProperty);
        ABMultiValueRef emails = ABRecordCopyValue(person, kABPersonEmailProperty);
        ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);

#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_5_0
        ABMultiValueRef socialMulti = ABRecordCopyValue(person, kABPersonSocialProfileProperty);
        NSMutableDictionary *socialDict = [NSMutableDictionary dictionaryWithCapacity:ABMultiValueGetCount(socialMulti)];
        
        for (CFIndex i = 0; i < ABMultiValueGetCount(socialMulti); i++) {
            NSDictionary *socialItem = (__bridge_transfer NSDictionary*)ABMultiValueCopyValueAtIndex(socialMulti, i);
            NSString *socialKey = [socialItem valueForKey:@"service"];
            [socialDict setObject:[socialItem valueForKey:@"username"] forKey:socialKey];
        }
#endif
        
        NSString *firstName = nil;
        NSString *lastName = nil;
        if (fnameProperty) {
            firstName = [NSString stringWithFormat:@"%@", fnameProperty];            
        } else {
            firstName = @"";
        }

        if (lnameProperty) {
            lastName = [NSString stringWithFormat:@"%@", lnameProperty];
        } else {
            lastName = @"";
        }
        
        NSArray *emailsList = (__bridge_transfer NSArray *)ABMultiValueCopyArrayOfAllValues(emails);
        NSArray *phonesList = (__bridge_transfer NSArray *)ABMultiValueCopyArrayOfAllValues(phones);

        NSString *facebookId = nil;
        NSString *twitterId = nil;

#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_5_0
        facebookId = [socialDict valueForKey:@"facebook"];
        twitterId = [socialDict valueForKey:@"twitter"];
#endif

        if (ContactRecord.class) {
            ContactRecord *contact = [ContactRecord new];
            contact.firstName = firstName;
            contact.lastName = lastName;
            contact.emails = emailsList;
            contact.phones = phonesList;
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_5_0
            contact.facebookId = facebookId;
            contact.twitterId = twitterId;
#endif
            contact.recordId = [NSString stringWithFormat:@"%@", [NSNumber numberWithInt:recordID]];
            [contactsList addObject:contact];
        } else {
            NSMutableDictionary *profileDict = [NSMutableDictionary new];
            if (emailsList) {
                [profileDict setObject:emailsList forKey:@"emails"];
            }
            if (phonesList) {
                [profileDict setObject:phonesList forKey:@"phones"];
            }
            if (firstName) {
                [profileDict setObject:firstName forKey:@"first_name"];
            }
            if (lastName) {
                [profileDict setObject:lastName forKey:@"last_name"];
            }
            if (facebookId) {
                [profileDict setObject:facebookId forKey:@"facebook_id"];
            }
            if (twitterId) {
                [profileDict setObject:twitterId forKey:@"twitter_id"];
            }
            [profileDict setObject:[NSNumber numberWithInt:recordID] forKey:@"record_id"];
            
            [contactsList addObject:profileDict];
        }

        if (fnameProperty)
            CFRelease(fnameProperty);
        if (lnameProperty)
            CFRelease(lnameProperty);
        if (emails)
            CFRelease(emails);
        if (phones)
            CFRelease(phones);
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_5_0
        CFRelease(socialMulti);
#endif
    }
    
    CFRelease(people);
    CFRelease(addressBook);
    return contactsList;
}


+ (UIImage*) imageForContactWithRecordID:(NSInteger) recordID
{
    AddressBookWrapper* ab = [self sharedInstance];
    NSString *contactKey = [NSString stringWithFormat:@"image_%d", recordID];
    UIImage* cachedImage = [ab.imageCache objectForKey:contactKey];
    if (cachedImage) {
        return cachedImage;
    } else {
        // Get contact from Address Book
        ABAddressBookRef addressBook = ABAddressBookCreate();
        ABRecordID recordId = (ABRecordID) recordID;
        ABRecordRef person = ABAddressBookGetPersonWithRecordID(addressBook, recordId);

        UIImage *contactImage;
        // Check for contact picture
        if (person != nil && ABPersonHasImageData(person)) {
            if ( &ABPersonCopyImageDataWithFormat != nil ) {
                // iOS >= 4.1
                contactImage = [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail)];
            } else {
                // iOS < 4.1
                contactImage = [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageData(person)];
            }
        }
        if (contactImage) {
            [ab.imageCache setObject:contactImage forKey:contactKey];
        }
        return contactImage;
    }
    return nil;
}

@end

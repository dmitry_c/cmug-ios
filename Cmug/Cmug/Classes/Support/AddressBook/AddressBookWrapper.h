//
//  AddressBookWrapper.h
//  Cmug
//
//  Created by Dmitry C on 6/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface AddressBookWrapper : NSObject
@property (nonatomic, retain) NSCache* imageCache;

// Address Book
+ (ABAddressBookRef) addressBook;
+ (NSArray *) contacts; // people

+ (UIImage*) imageForContactWithRecordID:(NSInteger) recordID;
+ (AddressBookWrapper*) sharedInstance;
@end
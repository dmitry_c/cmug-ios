//
//  UITableView+QuickScrollAdditions.m
//  Cmug
//
//  Created by Dmitry C on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UITableView+QuickScrollAdditions.h"
#import "QuickScrollManager.h"

@implementation UITableView (QuickScrollAdditions)

- (void)setImage:(UIImage*)image forCellAtIndexPath:(NSIndexPath*)indexPath
{
    QuickScrollManager *manager = [QuickScrollManager sharedManager];
    [manager setImage:image forCellImageViewInTableView:self atIndexPath:indexPath];
}

- (void)setImage:(UIImage*)image placeholderImage:(UIImage*)placeholderImage forCellAtIndexPath:(NSIndexPath*)indexPath
{
    QuickScrollManager *manager = [QuickScrollManager sharedManager];
    [manager setImage:image placeholderImage:placeholderImage forCellImageViewInTableView:self atIndexPath:indexPath];
}

- (void)setImageURL:(NSString*)url forCellAtIndexPath:(NSIndexPath*)indexPath
{
    QuickScrollManager *manager = [QuickScrollManager sharedManager];
    [manager setImageWithURL:url forCellImageViewInTableView:self atIndexPath:indexPath];
}

- (void)setImageURL:(NSString*)url placeholderImage:(UIImage*)image forCellAtIndexPath:(NSIndexPath*)indexPath
{
    if ([url isMemberOfClass:NSNull.class] || !url) {
        if (image) {
            [self setImage:image forCellAtIndexPath:indexPath];
        }
    } else {
        QuickScrollManager *manager = [QuickScrollManager sharedManager];
        [manager setImageWithURL:url placeholderImage:image forCellImageViewInTableView:self atIndexPath:indexPath];    
    }
}
@end

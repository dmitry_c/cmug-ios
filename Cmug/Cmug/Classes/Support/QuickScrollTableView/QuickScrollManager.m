//
//  QuickScrollManager.m
//  Cmug
//
//  Created by Dmitry C on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QuickScrollManager.h"

#define kCellIndexPath @"indexPath"
#define kImageRefKey @"imageRef"
#define kImage @"image"
#define kTableViewKey @"tableView"
#define kAdditionalAction @"additionalAction"
#define kPlaceholderImage @"placeholder"

#define CORNER_RADIUS 8.0f
#define BORDER_SIZE 0.0f

#define QSLog NSLog

static QuickScrollManager *instance;

void addRoundedRectToPath(CGRect rect, CGContextRef context, CGFloat ovalWidth, CGFloat ovalHeight) 
{
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM(context, ovalWidth, ovalHeight);
    CGFloat fw = CGRectGetWidth(rect) / ovalWidth;
    CGFloat fh = CGRectGetHeight(rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

@interface QuickScrollManager()
@property (nonatomic, retain) NSOperationQueue* operationQueue;

- (void)decodeImage:(NSDictionary *)params;
- (void)didDecodeImage:(NSDictionary *)params;
@end

@implementation QuickScrollManager
@synthesize operationQueue = _operationQueue;

- (NSOperationQueue *)operationQueue
{
    if (!_operationQueue) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 1;
    }    
    return _operationQueue;
}

+ (id)sharedManager
{
    if (instance == nil)
    {
        instance = [[QuickScrollManager alloc] init];
    }
    
    return instance;
}

#pragma mark - Public methods on TableView
- (void)setImage:(UIImage*)image placeholderImage:(UIImage*)placeholderImage forCellImageViewInTableView:(UITableView*)tableView atIndexPath:(NSIndexPath*)indexPath
{
    if (image) {
        [self setImage:image forCellImageViewInTableView:tableView atIndexPath:indexPath];
    } else if (placeholderImage) {
        [self setImage:placeholderImage forCellImageViewInTableView:tableView atIndexPath:indexPath];
    }
}

- (void)setImage:(UIImage*)image forCellImageViewInTableView:(UITableView*)tableView atIndexPath:(NSIndexPath*)indexPath
{
    CGImageRef imageRef = [image CGImage];
    
    NSDictionary *operationParameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                         indexPath, kCellIndexPath,
                                         imageRef, kImageRefKey,
                                         tableView, kTableViewKey,
                                         @"roundedCorners", kAdditionalAction,
                                         nil];
    
    NSInvocationOperation *thumbnailOperation = [[NSInvocationOperation alloc]
                                                 initWithTarget:self
                                                 selector:@selector(decodeImage:)
                                                 object:operationParameters];
    
    [[self operationQueue] addOperation:thumbnailOperation];    
}

- (void)setImageWithURL:(NSString*)url placeholderImage:(UIImage*)placeholderImage forCellImageViewInTableView:(UITableView*)tableView atIndexPath:(NSIndexPath*)indexPath
{
    // First Call the SDWebImageManager for cached image or ask to download,
    // then on success perform setImage:forCellImageViewInTableView:atIndexPath:    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    // Remove in progress downloader from queue???
    //    [manager cancelForDelegate:self];
    
    if (url)
    {
        // TODO: review options
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                  indexPath, kCellIndexPath,
                                  tableView, kTableViewKey,
                                  placeholderImage, kPlaceholderImage,
                                  nil];
        SDWebImageOptions options = SDWebImageRetryFailed | SDWebImageLowPriority;
        [manager downloadWithURL:[NSURL URLWithString:url] delegate:self options:options userInfo:userInfo];
        
    }
    
}

- (void)setImageWithURL:(NSString*)url forCellImageViewInTableView:(UITableView*)tableView atIndexPath:(NSIndexPath*)indexPath
{
    [self setImageWithURL:url placeholderImage:nil forCellImageViewInTableView:tableView atIndexPath:indexPath];
}

#pragma mark - Magic

- (void)decodeImage:(NSDictionary *)params 
{
    // thumbnailDict contains thumbnail and its index. Thumbnail
    // is represented by a UIImage object with key kThumbnailImageKey,
    // Thumbnail index is represented by an NSNumber object with key
    // kThumbnailIndexKey.
    
    CGImageRef originalImage = (__bridge CGImageRef)[params objectForKey:kImageRefKey];
    CGImageAlphaInfo alpha = CGImageGetAlphaInfo(originalImage);
    if (!(alpha == kCGImageAlphaFirst ||
        alpha == kCGImageAlphaLast ||
        alpha == kCGImageAlphaPremultipliedFirst ||
        alpha == kCGImageAlphaPremultipliedLast)) 
    {
        // image has no alpha channel
        // The bitsPerComponent and bitmapInfo values are hard-coded to prevent an "unsupported parameter combination" error
        CGContextRef offscreenContext = CGBitmapContextCreate(NULL,
                                                              CGImageGetWidth(originalImage),
                                                              CGImageGetHeight(originalImage),
                                                              8,
                                                              0,
                                                              CGImageGetColorSpace(originalImage),
                                                              kCGBitmapByteOrderDefault | kCGImageAlphaPremultipliedFirst);
        
        // Draw the image into the context and retrieve the new image, which will now have an alpha layer
        CGContextDrawImage(offscreenContext, CGRectMake(0, 0,                                                               CGImageGetWidth(originalImage),                                                               CGImageGetHeight(originalImage)), originalImage);
        CGImageRef imageRefWithAlpha = CGBitmapContextCreateImage(offscreenContext);
        originalImage = imageRefWithAlpha;
//        CGImageRelease(imageRefWithAlpha);
    }
    
    NSString* additionalOperation = [params objectForKey:kAdditionalAction];
    if (!originalImage) {
        QSLog(@"No Image");
    }

    CGDataProviderRef cgImageDataProvider = CGImageGetDataProvider(originalImage);
    CFDataRef imageData = CGDataProviderCopyData(cgImageDataProvider);
//    if (cgImageDataProvider!=nil) {
//        CGDataProviderRelease(cgImageDataProvider);
//    }
    CGDataProviderRef imageDataProvider = CGDataProviderCreateWithCFData(imageData);
    if (imageData != NULL) {
        CFRelease(imageData);
    }
    CGImageRef image = CGImageCreate(CGImageGetWidth(originalImage),
                                     CGImageGetHeight(originalImage),
                                     CGImageGetBitsPerComponent(originalImage),
                                     CGImageGetBitsPerPixel(originalImage),
                                     CGImageGetBytesPerRow(originalImage),
                                     CGImageGetColorSpace(originalImage),
                                     CGImageGetBitmapInfo(originalImage),
                                     imageDataProvider,
                                     CGImageGetDecode(originalImage),
                                     CGImageGetShouldInterpolate(originalImage),
                                     CGImageGetRenderingIntent(originalImage));
    if (imageDataProvider) {
        CGDataProviderRelease(imageDataProvider);
    }
    
    if (additionalOperation && [additionalOperation isEqual:@"roundedCorners"]) {
        size_t width = CGImageGetWidth(image);
        size_t height = CGImageGetHeight(image);
        
        CGContextRef context = CGBitmapContextCreate(NULL,
                                                     width,
                                                     height,
                                                     CGImageGetBitsPerComponent(image),
                                                     0,
                                                     CGImageGetColorSpace(image),
                                                     CGImageGetBitmapInfo(image));
        if (!context) {
            QSLog(@"No Context");
        }
        
        CGFloat borderSize = BORDER_SIZE;
        CGFloat ovalWidth = CORNER_RADIUS;
        CGFloat ovalHeight = CORNER_RADIUS;
        CGContextBeginPath(context);
        addRoundedRectToPath(CGRectMake(borderSize, borderSize, width - borderSize * 2, height - borderSize * 2), context, ovalWidth, ovalHeight);
        CGContextClosePath(context);
        CGContextClip(context);
        
        // Draw the image to the context; the clipping path will make anything outside the rounded rect transparent
        CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
        
        // Create a CGImage from the context
        CGImageRef clippedImage = CGBitmapContextCreateImage(context);
        CGContextRelease(context);

        if (image != NULL) {
            CGImageRelease(image);            
        }
        image = clippedImage;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:params];
    [dict setObject:[UIImage imageWithCGImage:image] forKey:kImage];
    if (image != NULL) {
        CGImageRelease(image);
    }

    [self performSelectorOnMainThread:@selector(didDecodeImage:)
                           withObject:dict
                        waitUntilDone:NO
     ];
}


- (void)didDecodeImage:(NSDictionary *)params 
{
    // Unpack thumbnail and its index.
//    CGImageRef imageRef = (__bridge CGImageRef)[params objectForKey:kImageRefKey];
//    UIImage *image = [UIImage imageWithCGImage:imageRef];
    UIImage *image = (UIImage*)[params objectForKey:kImage];
    
    NSIndexPath *path = [params objectForKey:kCellIndexPath];
    UITableView *tableView = [params objectForKey:kTableViewKey];
    
    NSArray *visiblePaths = [tableView indexPathsForVisibleRows];
    
    if ([visiblePaths containsObject:path]) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:path];
        // simply set decoded and updated
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.imageView setImage:image];    
            [cell setNeedsLayout];
        });
    }
}


#pragma mark - SDWebImageManager Delegate implementation
// Called when image download is completed successfuly.
- (void)webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image forURL:(NSURL *)url userInfo:(NSDictionary *)info
{
    if (image) {
        [self setImage:image forCellImageViewInTableView:[info objectForKey:kTableViewKey] atIndexPath:[info objectForKey:kCellIndexPath]];
    } else {
        UIImage *placeHolderImage = [info valueForKey:kPlaceholderImage];
        if (placeHolderImage) {
            [self setImage:placeHolderImage forCellImageViewInTableView:[info objectForKey:kTableViewKey] atIndexPath:[info objectForKey:kCellIndexPath]];
        }        
    }
}

- (void)webImageManager:(SDWebImageManager *)imageManager didFailWithError:(NSError *)error forURL:(NSURL *)url userInfo:(NSDictionary *)info
{
    QSLog(@"Oops for: %@, error: %@", info, error);
    UIImage *placeHolderImage = [info valueForKey:kPlaceholderImage];
    if (placeHolderImage) {
        [self setImage:placeHolderImage forCellImageViewInTableView:[info objectForKey:kTableViewKey] atIndexPath:[info objectForKey:kCellIndexPath]];
    }
}


@end

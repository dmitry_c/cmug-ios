//
//  QuickScrollManager.h
//  Cmug
//
//  Created by Dmitry C on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDWebImage/SDWebImageManager.h"

void addRoundedRectToPath(CGRect rect, CGContextRef context, CGFloat ovalWidth, CGFloat ovalHeight);

@interface QuickScrollManager : NSObject <SDWebImageManagerDelegate>
+ (id)sharedManager;

- (void)setImage:(UIImage*)image forCellImageViewInTableView:(UITableView*)tableView atIndexPath:(NSIndexPath*)indexPath;
- (void)setImage:(UIImage*)image placeholderImage:(UIImage*)placeholderImage forCellImageViewInTableView:(UITableView*)tableView atIndexPath:(NSIndexPath*)indexPath;

- (void)setImageWithURL:(NSString*)url forCellImageViewInTableView:(UITableView*)tableView atIndexPath:(NSIndexPath*)indexPath;
- (void)setImageWithURL:(NSString*)url placeholderImage:(UIImage*)placeholderImage forCellImageViewInTableView:(UITableView*)tableView atIndexPath:(NSIndexPath*)indexPath;
@end

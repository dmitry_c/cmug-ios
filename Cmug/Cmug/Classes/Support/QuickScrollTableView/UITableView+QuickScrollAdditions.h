//
//  UITableView+QuickScrollAdditions.h
//  Cmug
//
//  Created by Dmitry C on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (QuickScrollAdditions)
- (void)setImage:(UIImage*)image forCellAtIndexPath:(NSIndexPath*)indexPath;
- (void)setImage:(UIImage*)image placeholderImage:(UIImage*)placeholderImage forCellAtIndexPath:(NSIndexPath*)indexPath;
- (void)setImageURL:(NSString*)url forCellAtIndexPath:(NSIndexPath*)indexPath;
- (void)setImageURL:(NSString*)url placeholderImage:(UIImage*)image forCellAtIndexPath:(NSIndexPath*)indexPath;
@end

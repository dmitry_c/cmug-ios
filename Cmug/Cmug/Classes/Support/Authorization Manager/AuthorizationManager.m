//
//  AuthorizationManager.m
//
#import <Security/Security.h>

#import "AuthorizationManager.h"
#import "ApiClient.h"
#import "RestKit/RKParams.h"
#import "Settings.h"
#import "ProfileService.h"
#import "ExtendedLoginRequest.h"
#import "AppDelegate.h"


static AuthorizationManager *_instance;
@implementation AuthorizationManager
{
    BOOL isTokenRefreshing;
}

@synthesize userId=_userId;
@synthesize authToken=_authToken;
@synthesize authTokenExpires=_authTokenExpires;

@synthesize email=_email;
@synthesize password=_password;

@synthesize keychainLogin=_keychainLogin;
@synthesize keychainAuthToken=_keychainAuthToken;
@synthesize firstLogin;

+ (AuthorizationManager*)sharedManager
{
	@synchronized(self) 
	{	
        if (_instance == nil) 
		{
            _instance = [[super alloc] init];
        }
    }
    return _instance;
}


#pragma mark Singleton Methods

#pragma mark -
#pragma mark init

- (id) init
{
	self = [super init];
	if (self != nil) 
	{
        
	}
	return self;
}

-(void) logout
{
    // Note: logout should be sen before token cleared
//    [(ApiClient*)[[AppDelegate sharedApplication] apiClient] post:@"/account/logout/" 
//                            params:nil
//                          delegate:self];
    [[ApiClient sharedClient] post:@"/account/logout/" 
                            params:nil
                          delegate:self];
    
    
//    [self.keychainAuthToken resetKeychainItem];
//
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AUTH_TOKEN_EXPIRES_USERDEFAULTS_KEY_NAME];
//    [[NSUserDefaults standardUserDefaults] synchronize];

}


-(void) unregister
{
    [self logout];
    [self.keychainLogin resetKeychainItem];
}

- (KeychainItemWrapper*) keychainLogin
{
    if (!_keychainLogin) {
        _keychainLogin = [[KeychainItemWrapper alloc] initWithIdentifier:KEYCHAIN_LOGIN_ID accessGroup:KEYCHAIN_GROUP];
    }
    return _keychainLogin;
}

- (KeychainItemWrapper*) keychainAuthToken
{
    if (!_keychainAuthToken) {
        _keychainAuthToken = [[KeychainItemWrapper alloc] initWithIdentifier:KEYCHAIN_AUTHTOKEN_ID accessGroup:KEYCHAIN_GROUP];
    }
    return _keychainAuthToken;
}

-(BOOL) tokenExpired
{
    return !([self.authTokenExpires compare:[NSDate date]] == NSOrderedDescending);
}

-(BOOL) authorized
{
	if (self.authToken!=nil && self.authToken.length > 0) {
        if (self.authTokenExpires && !self.tokenExpired) {
            return YES;
        } else {
            // auth token expired -- invalidate at background
            if (self.registered) {
                [self refreshToken];
            }

            return YES;
        }
    }
	return NO;
}

-(BOOL) checkRegistered
{
    //If email and password were not stored in keychain,
    //there still may be a mark in userdefaults
    return ([[NSUserDefaults standardUserDefaults] stringForKey:REGISTRATION_DATE_USERDEFAULTS_KEY_NAME] != nil);
}

-(BOOL) registered
{
    //email and password are stored in keychain,
    //they will be available even after application reinstalled, 
    //so check for those first
    if (self.email && self.email.length && self.password && self.password.length)
        return YES;
    
    if (self.checkRegistered)
        return YES;
    
    return NO;
}

- (NSString*) email
{
    if (!_email) {
        _email = [self.keychainLogin objectForKey:(__bridge id)kSecAttrAccount];
    }
    return _email;
}

- (NSString*) password
{
    if (!_password) {
        _password = [self.keychainLogin objectForKey:(__bridge id)kSecValueData];
    }
    return _password;
}

- (NSString*) authToken
{
    if (!_authToken) {
        _authToken = [self.keychainAuthToken objectForKey:(__bridge id)kSecValueData];
    }
    return _authToken;
}

- (void) setAuthToken:(NSString *)authToken
{
    if(self.authToken != authToken) {
        [self storeAuthToken:authToken expiresAt:self.authTokenExpires forUserWithId:self.userId];
    }
}

- (NSDate*) authTokenExpires
{
    if (!_authTokenExpires) {
        _authTokenExpires = [[NSUserDefaults standardUserDefaults] objectForKey:AUTH_TOKEN_EXPIRES_USERDEFAULTS_KEY_NAME];
    }
    return _authTokenExpires;
}

- (NSString*) userId
{
    if (!_userId) {
        _userId = [self.keychainAuthToken objectForKey:(__bridge id)kSecAttrAccount];
    }
    return _userId;
}

-(void) storeLogin:(NSString*)email andPassword:(NSString*)password
{
    // Store username(email) to keychain
    if (email)
    	[self.keychainLogin setObject:email forKey:(__bridge id)kSecAttrAccount];
    
    // Store password to keychain
    if (password)
    	[self.keychainLogin setObject:password forKey:(__bridge id)kSecValueData];    	    
}

-(void) storeAuthToken:(NSString*)token expiresAt:(NSDate*)expires forUserWithId:(NSString*)userId
{
    _authToken = token;
    _authTokenExpires = expires;
    [self.keychainAuthToken setObject:token forKey:(__bridge id)kSecValueData];             
    [self.keychainAuthToken setObject:userId forKey:(__bridge id)kSecAttrAccount];    
    [[NSUserDefaults standardUserDefaults] setObject:expires forKey:AUTH_TOKEN_EXPIRES_USERDEFAULTS_KEY_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) loginAndForceTokenRefresh:(BOOL)forceTokenRefresh {
    ExtendedLoginRequest *loginRequestData = [ExtendedLoginRequest new];
    loginRequestData.email = self.email;
    loginRequestData.password = self.password;
    loginRequestData.authToken = self.authToken;
    loginRequestData.forceAuthTokenRefresh = forceTokenRefresh;
    
    NSLog(@"Token refresh");
    @synchronized(self) {
        if (!isTokenRefreshing) {
            isTokenRefreshing = YES;
        
            [[ProfileService new] login:loginRequestData
                              onSuccess:^(LoginSignupResponse *responseData) {
                                  NSLog(@"onSuccess: %@\n", responseData); 
                                  NSLog(@"Token refreshed");
                                  
                                  [[AuthorizationManager sharedManager] storeAuthToken:responseData.authToken expiresAt:responseData.authTokenExpires forUserWithId:responseData.userId];
                                  isTokenRefreshing = NO;
                              }
                              onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                                  NSLog(@"onFailure: %@", errorResponse.errors);
                                  NSLog(@"Token not refreshed");
                                  // If Failed to Refresh token in background then 
                                  // Show Login View
                                  self.authToken = @"";
                                  [[NSNotificationCenter defaultCenter] postNotificationName:kUserLoggedOutNotification object:nil];
                                  isTokenRefreshing = NO;
                              }
             
             ];
        }
    }
    
}
         
-(void) refreshToken 
{
    [self loginAndForceTokenRefresh:YES];
}

-(void) markRegistered 
{
    [[NSUserDefaults standardUserDefaults] setValue: [NSDate new] forKey:REGISTRATION_DATE_USERDEFAULTS_KEY_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
    self.authToken = @""; // Do not set this to nil
    NSLog(@"Logout Success %@", response);
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserLoggedOutNotification object:nil];
}

- (void) request:(RKRequest *)request didFailLoadWithError:(NSError *)error
{
    NSLog(@"Logout Failed %@", error);
}

@end

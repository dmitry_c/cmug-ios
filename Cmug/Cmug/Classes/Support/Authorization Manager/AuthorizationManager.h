//
//  AuthorizationManager.h
//

#import <Foundation/Foundation.h>
#import "KeychainItemWrapper.h"
#import "RestKit/RKRequest.h"

#define KEYCHAIN_LOGIN_ID @"com.cmug.keychain.login"
#define KEYCHAIN_AUTHTOKEN_ID @"com.cmug.keychain.authToken"
#define KEYCHAIN_GROUP nil
#define kUserLoggedOutNotification @"AuthorizationManager.UserLoggedOut"


@interface AuthorizationManager : NSObject <RKRequestDelegate>

@property (nonatomic, copy) NSString* userId;
@property (nonatomic, copy) NSString* email;
@property (nonatomic, copy) NSString* password;

@property (nonatomic, copy) NSString* authToken;
@property (nonatomic, retain) NSDate* authTokenExpires;

@property (nonatomic, readonly) BOOL authorized;
@property (nonatomic, readonly) BOOL registered;
@property (nonatomic) BOOL firstLogin;

@property (nonatomic, retain) KeychainItemWrapper* keychainLogin;
@property (nonatomic, retain) KeychainItemWrapper* keychainAuthToken;

-(void) storeLogin:(NSString*)email andPassword:(NSString*)password;
-(void) storeAuthToken:(NSString*)token expiresAt:(NSDate*)expires forUserWithId:(NSString*)userId;

-(void) loginAndForceTokenRefresh:(BOOL)forceTokenRefresh;
-(void) refreshToken;
-(void) logout;

-(void) unregister;
-(BOOL) tokenExpired;

-(void) markRegistered;

+ (AuthorizationManager*) sharedManager;


@end

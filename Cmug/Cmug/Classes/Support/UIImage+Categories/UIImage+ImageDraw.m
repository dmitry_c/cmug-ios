//
//  UIImage+ImageDraw.m
//  Cmug
//
//  Created by Dmitry C on 7/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIImage+ImageDraw.h"

#define SCREEN_SCALE ([[UIScreen mainScreen] scale ])

@implementation UIImage (ImageDraw)
- (UIImage *) normalize {
    
    CGSize size = CGSizeMake(round(self.size.width*SCREEN_SCALE), round(self.size.height*SCREEN_SCALE));
    CGColorSpaceRef genericColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef thumbBitmapCtxt = CGBitmapContextCreate(NULL, 
                                                         size.width, 
                                                         size.height, 
                                                         8, (4 * size.width), 
                                                         genericColorSpace, 
                                                         kCGImageAlphaPremultipliedFirst);
    CGColorSpaceRelease(genericColorSpace);
    CGContextSetInterpolationQuality(thumbBitmapCtxt, kCGInterpolationDefault);
    CGRect destRect = CGRectMake(0, 0, size.width, size.height);
    CGContextDrawImage(thumbBitmapCtxt, destRect, self.CGImage);
    CGImageRef tmpThumbImage = CGBitmapContextCreateImage(thumbBitmapCtxt);
    CGContextRelease(thumbBitmapCtxt);    
    UIImage *result = [UIImage imageWithCGImage:tmpThumbImage scale:SCREEN_SCALE orientation:UIImageOrientationUp];
    CGImageRelease(tmpThumbImage);
    
    return result;    
}

- (UIImage*)drawImage:(UIImage*)image inRect:(CGRect)dest fromRect:(CGRect)src
{
    CGImageRef background = [self normalize].CGImage;
    CGImageRef sprite = [image CGImage];
    CGFloat scale = [[UIScreen mainScreen] scale];
    if (scale == 0.0f) scale = 1.0f;

    if (CGRectIsNull(dest)) {
        dest = CGRectMake(0, 0, self.size.width, self.size.height);
    }
    if (CGRectIsNull(src)) {
        src = CGRectMake(0, 0, image.size.width, image.size.height);
    }
    
    dest = CGRectApplyAffineTransform(dest, CGAffineTransformMakeScale(scale, scale));
    src = CGRectApplyAffineTransform(src, CGAffineTransformMakeScale(scale, scale));

    // Creating combined image
    size_t width = CGImageGetWidth(background);
    size_t height = CGImageGetHeight(background);
    
    // Find coordinates for center alignment
    CGRect sourceRect = CGRectMake(0.5f * (src.size.width - width) + src.origin.x,
                                   0.5f * (src.size.height - height) + src.origin.y,
                                   width,
                                   height);

    CGImageRef overImage = CGImageCreateWithImageInRect(sprite, sourceRect);
    //    CGImageRelease(sprite);

    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 width,
                                                 height,
                                                 CGImageGetBitsPerComponent(background),
                                                 0, //CGImageGetBytesPerRow(background),
                                                 CGImageGetColorSpace(background),
                                                 CGImageGetBitmapInfo(background));

//    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
//    CGContextSetAllowsAntialiasing(context, NO);
    // Draw the image to the context
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), background);
    
    // Draw the image place over the background
    CGContextDrawImage(context, dest, overImage);
    CGImageRelease(overImage);
    
    // Create a CGImage from the context
    CGImageRef resultImage = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    
    UIImage *result = [UIImage imageWithCGImage:resultImage scale:scale orientation:UIImageOrientationUp];
    
    CGImageRelease(resultImage);
    
//    CGImageRelease(background);
    return result;
}
@end

//
//  UIImage+ImageDraw.h
//  Cmug
//
//  Created by Dmitry C on 7/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageDraw)
// Draws an extracted image with src bounds in current image in the inRect bounds.
- (UIImage*)drawImage:(UIImage*)image inRect:(CGRect)dest fromRect:(CGRect)src;
@end

//
//  PrevNextDoneAccessoryView.h
//  Cmug
//
//  Created by Dmitry C on 4/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define INPUT_TAG_INDEX_OFFSET 1024

#define DONE_BUTTON_TAG 200
#define PREVIOUS_BUTTON_TAG 201
#define NEXT_BUTTON_TAG 202


@class PrevNextDoneAccessoryView;

@protocol PrevNextDoneAccessoryViewDelegate <NSObject>
-(void) accessoryView: (PrevNextDoneAccessoryView*) view 
             didClick: (UIBarButtonItem*) sender;

@property (nonatomic, assign) int tabIndex;
-(int) maxTabIndexForTextInputs;
-(UIView*) textFieldsSuperView; //view that contains text fields
-(UITextField*) activeTextField;
@end


@interface PrevNextDoneAccessoryView : UINavigationBar     

@property (nonatomic, retain) UIBarButtonItem* prevButton;
@property (nonatomic, retain) UIBarButtonItem* nextButton;
@property (nonatomic, retain) UIBarButtonItem* doneButton;

@property (nonatomic, unsafe_unretained) id<PrevNextDoneAccessoryViewDelegate> delegate;

- (void) updateButtons;
@end


//
//  PrevNextDoneAccessoryView.m
//  Cmug
//
//  Created by Dmitry C on 4/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PrevNextDoneAccessoryView.h"

@implementation PrevNextDoneAccessoryView

@synthesize delegate;
@synthesize prevButton, nextButton, doneButton;

- (void) updateButtons
{
    if ([self.delegate respondsToSelector:@selector(isPreviousEnabled)]) {
        self.prevButton.enabled = (BOOL)[self.delegate performSelector:@selector(isPreviousEnabled)];
    }
    
    if ([self.delegate respondsToSelector:@selector(isNextEnabled)]) {
        self.nextButton.enabled = (BOOL)[self.delegate performSelector:@selector(isNextEnabled)];
    }
}

- (void) buttonClicked: (UIBarButtonItem*) sender {
    [self.delegate accessoryView: self
                        didClick: sender];
    
    switch (sender.tag) {
        case DONE_BUTTON_TAG:
            self.delegate.tabIndex = -1;
            break;
        case PREVIOUS_BUTTON_TAG:
            self.delegate.tabIndex--;
            break;
        case NEXT_BUTTON_TAG:
            self.delegate.tabIndex++;
            break;
        default:
            break;
    }
    
    // Try to find next responder
    UIResponder* nextResponder = [self.delegate.textFieldsSuperView viewWithTag:
                                  self.delegate.tabIndex + INPUT_TAG_INDEX_OFFSET];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        [self.delegate.activeTextField resignFirstResponder];
        self.delegate.tabIndex = -1;
    }

    [self updateButtons];
}

- (id)initWithFrame:(CGRect)frame
{
    // create nav bar for keyboard
    self = [super initWithFrame:frame];
    [self setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [self setBarStyle:UIBarStyleBlackTranslucent];
    
    UINavigationItem* navItem = [[UINavigationItem alloc] initWithTitle:@""];
    self.doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                    target:self
                                                                    action:@selector(buttonClicked:)];
    self.doneButton.tag = DONE_BUTTON_TAG;
    
    [navItem setRightBarButtonItem:self.doneButton];
    self.prevButton = [[UIBarButtonItem alloc] initWithTitle:@"Previous"
                                                       style:UIBarButtonItemStyleBordered
                                                      target:self
                                                      action:@selector(buttonClicked:)];
    self.prevButton.tag = PREVIOUS_BUTTON_TAG;
    
    self.nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                       style:UIBarButtonItemStyleBordered
                                                      target:self
                                                      action:@selector(buttonClicked:)];
    self.nextButton.tag = NEXT_BUTTON_TAG;
    
    if ([navItem respondsToSelector:@selector(setLeftBarButtonItems:)]) {
        [navItem setLeftBarButtonItems:[NSArray arrayWithObjects:self.prevButton, self.nextButton, nil]];
    } else {
        // Fix for ios4
        UIToolbar *tools = [[UIToolbar alloc]
                            initWithFrame:CGRectMake(0.0f, 0.0f, 153.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
        tools.clearsContextBeforeDrawing = NO;
        tools.clipsToBounds = NO;
        tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f];
        tools.barStyle = -1; // clear background
        NSArray *buttons = [NSArray arrayWithObjects:self.prevButton, self.nextButton, nil];

        // Add buttons to toolbar and toolbar to nav bar.
        [tools setItems:buttons animated:NO];
        UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];

        navItem.leftBarButtonItem = twoButtons;
    }

    [self pushNavigationItem:navItem animated:NO];
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

//
//  TwitterWrapper.h
//  Cmug
//
//  Created by Dmitry C on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "NSMutableDictionary+NSNullsToEmptyStrings.h"
#import "SHKTwitter.h"
#import "RestKit/JSONKit.h"
#import "ResourceStrings.h"
#import "SHKTwitterExtended.h"

#define kUserDefaultsCurrentTwitterAccount @"com.cmug.twitter.account.current"
#define TWITTER_FRIENDS_ENDPOINT @"https://api.twitter.com/1/friends/ids.json"
#define TWITTER_INFO_LOOKUP_ENDPOINT @"https://api.twitter.com/1/users/lookup.json"

typedef void(^TWSuccessResponseBlock)(NSArray* usersList);
typedef void(^TWErrorResponseBlock)(NSError* error);
typedef void(^TWGotCurrentAccountBlock)(ACAccount* account);

typedef enum {
    TwitterWrapperPendingAction_None = 0,
    TwitterWrapperPendingAction_FriendIds,
    TwitterWrapperPendingAction_Friends,
    TwitterWrapperPendingAction_SelectAccount,
    TwitterWrapperPendingAction_UserInfo
} TwitterWrapperPendingAction;

@class TwitterWrapper;

@protocol TwitterFriendsFetchFlowDelegate<NSObject>
- (void) twitterFriendsFetchedIds:(NSArray*)friendIds withController:(TwitterWrapper*)controller;
- (void) twitterFriendsFetchedInfo:(NSArray*)friends withController:(TwitterWrapper*)controller;
@optional
- (void) twitterFriendsFetchFailedWithError:(NSError*) error;
@end


@interface TwitterWrapper : NSObject <SHKSharerDelegate>
@property (nonatomic, retain) ACAccount* twitterAccount;

+ (id)get;
- (BOOL)isConfigured;
- (void)configureShare:(void(^)(id result))callback;

// Returns a list of friends with fetched data from twitter.
- (void) friendsWithInfo:(BOOL)fetchUsersInfo 
            andDelegate:(id<TwitterFriendsFetchFlowDelegate>)delegate 
              onSuccess:(TWSuccessResponseBlock)successBlock
              onFailure:(TWErrorResponseBlock)failureBlock;

- (void) sendDirectMessage:(NSString*)message to:(NSString*)twitterScreenName;

// Tries to get selected ios5 twitter account from user defaults,
// otherwise shows select modal dialog.
- (void) currentTwitterAccount:(TWGotCurrentAccountBlock)gotAccount;

- (void) account:(TWGotCurrentAccountBlock)gotAccount;

- (void)userInfo:(void(^)(id userInfo))gotUserInfo;
- (void)screenName:(void(^)(NSString* result))gotScreenName;

// Removes ios5 account name stored in userdefaults
+ (void) cancelAccount;

@property (nonatomic, retain) id<TwitterFriendsFetchFlowDelegate> delegate;
@end

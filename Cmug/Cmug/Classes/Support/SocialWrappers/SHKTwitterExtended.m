//
//  SHKTwitterExtended.m
//  Cmug
//
//  Created by Dmitry C on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHKTwitterExtended.h"
#import "SHKXMLResponseParser.h"
#import "RestKit/JSONKit.h"
#import "NSMutableDictionary+NSNullsToEmptyStrings.h"
#import "TwitterWrapper.h"
#import "NSString+Additions.h"


@interface SHKTwitterExtended ()
- (void)handleUnsuccessfulTicket:(NSData *)data;
- (void)sendPostInfoLookup;
- (void)sendGetFriendsWithInfo:(BOOL)withInfo;
@end

@implementation SHKTwitterExtended
@synthesize friendIds = _friendIds;
@synthesize friends = _friends;

- (void)show
{
    if (item.shareType == SHKShareTypeExtended_TwitterFriends ||
        item.shareType == SHKShareTypeExtended_TwitterFriendIds) {
        [self setQuiet:YES];
        [self tryToSend];
    } else {
        [super show];
    }
}

- (BOOL)send
{
    if (self.item.shareType == SHKShareTypeExtended_TwitterFriendIds) {
        [self sendGetFriendsWithInfo:NO]; // only ids
        [self sendDidStart];
        return YES;
    } else if (self.item.shareType == SHKShareTypeExtended_TwitterFriends) {
        [self sendGetFriendsWithInfo:YES];
        [self sendDidStart];
        return YES;
    }
    
    return [super send];
}

- (void)handleUnsuccessfulTicket:(NSData *)data
{
    if (SHKDebugShowLogs)
		SHKLog(@"Twitter Send Status Error: %@", [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]);
	
	// CREDIT: Oliver Drobnik
	
	NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];		
	
	// in case our makeshift parsing does not yield an error message
	NSString *errorMessage = @"Unknown Error";		
	
	NSScanner *scanner = [NSScanner scannerWithString:string];
	
	// skip until error message
	[scanner scanUpToString:@"\"error\":\"" intoString:nil];
	
	
	if ([scanner scanString:@"\"error\":\"" intoString:nil])
	{
		// get the message until the closing double quotes
		[scanner scanUpToCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@"\""] intoString:&errorMessage];
	}
	
	
	// this is the error message for revoked access ...?... || removed app from Twitter
	if ([errorMessage isEqualToString:@"Invalid / used nonce"] || [errorMessage isEqualToString:@"Could not authenticate with OAuth."]) {
		
		[self shouldReloginWithPendingAction:SHKPendingSend];
		
	} else {
		
		//when sharing image, and the user removed app permissions there is no JSON response expected above, but XML, which we need to parse. 401 is obsolete credentials -> need to relogin
		if ([[SHKXMLResponseParser getValueForElement:@"code" fromResponse:data] isEqualToString:@"401"]) {
			
			[self shouldReloginWithPendingAction:SHKPendingSend];
			return;
		}
	}
	
	NSError *error = [NSError errorWithDomain:@"Twitter" code:2 userInfo:[NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey]];
	[self sendDidFailWithError:error];
}


#pragma mark - Get Friends Ids

- (void)getFriendIds
{
    self.pendingAction = TwitterWrapperPendingAction_FriendIds;
	self.item.shareType = SHKShareTypeExtended_TwitterFriendIds;
	[self share];
}

- (void)getFriends
{
    self.pendingAction = TwitterWrapperPendingAction_Friends;
    self.item.shareType = SHKShareTypeExtended_TwitterFriends;
    [self share];
}

-(void) getFriendsWithInfo:(BOOL)fetchUsersInfo
{
    if (fetchUsersInfo) {
        [self getFriends];
    } else {
        [self getFriendIds];
    }
}

#pragma mark - Fetchers

- (void)sendGetFriendsWithInfo:(BOOL)withInfo
{
    NSDictionary *twitterUserInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSHKTwitterUserInfo"];
    NSString *twScreenName = [twitterUserInfo objectForKey:@"screen_name"];
    // We assume that before this method called 
    // the user had already authorized and their data stored in user defaults,
    // or something went wrong.
    if (!twScreenName) {
        NSLog(@"Error: Can't get the user's twitter screen name.");
        return;
    }
    
    if (!consumer || !accessToken) {
        NSLog(@"Error: No access token");
        return;
    }
    
    OAMutableURLRequest *oRequest = [[OAMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?cursor=-1&screen_name=%@", TWITTER_FRIENDS_ENDPOINT, twScreenName]]
                                                                    consumer:consumer
                                                                       token:accessToken
                                                                       realm:nil   // our service provider doesn't specify a realm
                                                           signatureProvider:nil]; // use the default method, HMAC-SHA1
    
    [oRequest setHTTPMethod:@"GET"];
    OADataFetcher *fetcher = [[OADataFetcher alloc] init];
    
    if (withInfo) {
        [fetcher fetchDataWithRequest:oRequest
                             delegate:self
                    didFinishSelector:@selector(getFriends:didFinishWithData:)
                      didFailSelector:@selector(getFriends:didFailWithError:)];
    } else {
        [fetcher fetchDataWithRequest:oRequest
                             delegate:self
                    didFinishSelector:@selector(getFriendIds:didFinishWithData:)
                      didFailSelector:@selector(getFriends:didFailWithError:)];
    }
}

- (void)sendPostInfoLookup
{
//    NSDictionary *twitterUserInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSHKTwitterUserInfo"];
//    NSString *twScreenName = [twitterUserInfo objectForKey:@"screen_name"];
//    // We assume that before this method called 
//    // the user had already authorized and their data stored in user defaults,
//    // or something went wrong.
//    if (!twScreenName) {
//        NSLog(@"Error: Can't get the user's twitter screen name.");
//        return;
//    }
    
    if (!consumer || !accessToken) {
        NSLog(@"Error: No access token");
        return;
    }
    
    static const int kItemsPerView = 50; // up to 100
    int numViews = (int) ceil((float) [self.friendIds count] / kItemsPerView);
    
    // NSLog(@"%d", [self.friendIds count]);
    // NSLog(@"%d", numViews);
    
    for (int viewIndex = 0; viewIndex < numViews; viewIndex++) {
        
        int numItems;
        
        if (viewIndex * kItemsPerView + kItemsPerView - 1 <= [self.friendIds count]) {
            numItems = kItemsPerView;
        }
        else {
            numItems = [self.friendIds count] - viewIndex * kItemsPerView;
        }
        
        NSRange rangeForView = NSMakeRange(viewIndex * kItemsPerView, numItems);
        NSArray *itemsForView = [self.friendIds subarrayWithRange:rangeForView];
        
        NSString *userIds = [itemsForView componentsJoinedByString:@","];
        
        NSURL *url = [NSURL URLWithString:TWITTER_INFO_LOOKUP_ENDPOINT];
        OAMutableURLRequest *oRequest = [[OAMutableURLRequest alloc] initWithURL:url
                                                                        consumer:consumer
                                                                           token:accessToken
                                                                           realm:nil   // our service provider doesn't specify a realm
                                                               signatureProvider:nil];
        [oRequest setHTTPMethod:@"POST"];
        // user_id -- string of comma-separated user ids
        OARequestParameter* userIdsParameter = [[OARequestParameter alloc] initWithName:@"user_id" value:userIds];
        [oRequest setParameters:[NSArray arrayWithObject:userIdsParameter]];

        OADataFetcher *fetcher = [[OADataFetcher alloc] init];
        
        [fetcher fetchDataWithRequest:oRequest
                             delegate:self
                    didFinishSelector:@selector(getFriends:didFinishWithData:)
                      didFailSelector:@selector(getFriends:didFailWithError:)];

    }

}

#pragma mark - Callbacks

- (void)getFriends:(OAServiceTicket *)ticket didFinishWithData:(NSData *)data 
{	
	if (ticket.didSucceed) {
		
		NSError *error = nil;
		NSMutableDictionary *responseData;
		Class serializator = NSClassFromString(@"NSJSONSerialization");
		if (serializator) {
			responseData = [serializator JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
		} else {
			responseData = [[JSONDecoder decoder] mutableObjectWithData:data error:&error];
		}    
		
		if (error) {
			SHKLog(@"Error when parsing json twitter user info request:%@", [error description]);
		}
		
		[responseData convertNSNullsToEmptyStrings];
        NSLog(@"RESPONSE: %@", responseData);
        NSString *url = [[[ticket request] URL] URLStringWithoutQuery];
        if ([url equalsTo:TWITTER_FRIENDS_ENDPOINT]) {
            _friendIds = [responseData valueForKey:@"ids"];
            NSLog(@"Friends Ids list: %@", _friendIds);    
            if (!_friendIds || _friendIds.count == 0) {
                [self sendDidFinish];
            }
            // second step: fetch users info
            [self sendPostInfoLookup];
        } else if ([url equalsTo:TWITTER_INFO_LOOKUP_ENDPOINT]) {
            [self.friends addObjectsFromArray:(NSArray*)responseData];
            
            if (self.friends.count == self.friendIds.count) {  // fetched all parts
                [self sendDidFinish];
            }
        }
	} else {
		[self handleUnsuccessfulTicket:data];
	}
}

- (void)getFriends:(OAServiceTicket *)ticket didFailWithError:(NSError*)error
{
	[self sendDidFailWithError:error];
}

#pragma mark -
- (void)getFriendIds:(OAServiceTicket *)ticket didFinishWithData:(NSData *)data 
{	
	if (ticket.didSucceed) {
		
		NSError *error = nil;
		NSMutableDictionary *responseData;
		Class serializator = NSClassFromString(@"NSJSONSerialization");
		if (serializator) {
			responseData = [serializator JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
		} else {
			responseData = [[JSONDecoder decoder] mutableObjectWithData:data error:&error];
		}    
		
		if (error) {
			SHKLog(@"Error when parsing json twitter user info request:%@", [error description]);
		}
		
		[responseData convertNSNullsToEmptyStrings];
        NSLog(@"RESPONSE: %@", responseData);
        if ([ticket.request.URL.URLStringWithoutQuery equalsTo:TWITTER_FRIENDS_ENDPOINT]) {
            _friendIds = [responseData valueForKey:@"ids"];
            NSLog(@"Friends Ids list: %@", _friendIds);
       		[self sendDidFinish];
        }
	} else {
		[self handleUnsuccessfulTicket:data];
	}
}

@end

//
//  TwitterAccountSelectViewController.h
//  Cmug
//
//  Created by Dmitry C on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Accounts/Accounts.h>
#import "TwitterWrapper.h"

@interface TwitterAccountSelectViewController : UITableViewController
@property (strong, nonatomic) ACAccountStore *accountStore; 
@property (strong, nonatomic) NSArray *accounts;

@property (nonatomic, copy) TWGotCurrentAccountBlock onComplete;
@end

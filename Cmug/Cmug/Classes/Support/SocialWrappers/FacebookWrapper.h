//
//  FacebookWrapper.h
//  Cmug
//
//  Created by Dmitry C on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SHKFacebook.h"


typedef void(^FBSuccessResponseBlock)(NSArray* friendsList);
typedef void(^FBErrorResponseBlock)(NSError* error);
typedef void(^GenericResultBlock)(id result);

@interface SHKFacebook (Internals)
- (Facebook*)facebook;
@end

typedef enum {
    FBWrapperAction_None = 0,
    FBWrapperAction_Friends = 1,
    FBWrapperAction_Invite
} FBWrapperAction;

typedef enum {
    kSharerCancelled = 0,
    kSharerSuccess = 1,
    kSharerError = 2,
} WrapperCallbackCodes;

@interface FacebookWrapper : NSObject<SHKSharerDelegate, FBRequestDelegate>
@property (nonatomic, retain) Facebook* facebook;
@property (nonatomic, assign) FBWrapperAction pendingAction;

+ (id)get;
- (void)friends:(FBSuccessResponseBlock)successBlock onFailure:(FBErrorResponseBlock)failureBlock;
- (void)sendInvitationTo:(NSString*)facebookId delegate:(id)delegate;

+ (NSString*)pictureURLForFacebookUserWithId:(NSString*)facebookId;

- (BOOL)isConfigured;
- (void)configureShare:(void (^)(id result))callback;
@end

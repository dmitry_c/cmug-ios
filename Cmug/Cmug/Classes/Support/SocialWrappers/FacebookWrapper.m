//
//  FacebookWrapper.m
//  Cmug
//
//  Created by Dmitry C on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FacebookWrapper.h"
#import "ProfileService.h"
#import "RestKit/JSONKit.h"

#define kDelegateParam      @"delegate"
#define kFacebookIdParam    @"fbid"

@implementation SHKFacebook (Internals)
-(Facebook *)facebook
{
    return [self.class facebook];
}
@end


@interface FacebookWrapper()
// Friends
- (void)requestFriends;

@property (nonatomic, copy) FBSuccessResponseBlock friendsSuccess;
@property (nonatomic, copy) FBErrorResponseBlock friendsFailure;

@property (nonatomic, copy) GenericResultBlock callback;

// Invite
@property (nonatomic, copy) NSDictionary* pendingActionParams;
@end


static FacebookWrapper* instance;

@implementation FacebookWrapper

@synthesize facebook=_facebook;
@synthesize pendingAction;
@synthesize friendsSuccess=_friendsSuccess, friendsFailure=_friendsFailure;
@synthesize pendingActionParams;
@synthesize callback=_callback;


+ (id)get
{
    @synchronized(self) {
        if (!instance)
            instance = [FacebookWrapper new];
    }
    return instance;
}

- (Facebook*) facebook
{
    if (!_facebook) {
        _facebook = [[[SHKFacebook alloc] init] facebook];
    }
    return _facebook;
}

- (void)invokeCallback:(NSString*)description code:(NSUInteger)code
{
    if (self.callback) {
        self.callback([NSDictionary dictionaryWithObjectsAndKeys:
                       description, @"description",
                       [NSNumber numberWithInteger:code], @"code",
                       nil]);
        _callback = nil;
    }
}

#pragma mark -
#pragma mark SHKSharer delegate

- (void)sharerStartedSending:(SHKSharer *)sharer {}

- (void)sharerFinishedSending:(SHKSharer *)sharer {}

- (void)sharer:(SHKSharer *)sharer failedWithError:(NSError *)error shouldRelogin:(BOOL)shouldRelogin
{
    [self invokeCallback:error.description code:kSharerError];
    
    if (shouldRelogin) {
        // Logout and repeat actions
        [sharer.class logout];
        
//        if ([sharer isMemberOfClass:SHKFacebook.class]) {
//            [self signupWithFacebook];
//        } else if ([sharer isMemberOfClass:SHKTwitter.class]) {
//            [self signupWithTwitter];
//        }
        return;
    }
    
    NSString *title=nil;
    NSString *message=nil;
    
    if ([sharer isMemberOfClass:SHKFacebook.class]) {
//        title = ALERT_FacebookErrorTitle;
//        message = ALERT_FacebookErrorMessage;
    }
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:title
                                                 message:message
                                                delegate:nil 
                                       cancelButtonTitle:@"Ok" 
                                       otherButtonTitles:nil];
    [av show];
    
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}

- (void)sharerCancelledSending:(SHKSharer *)sharer
{
    CRLog(@"Cancelled %@", sharer);
    [self invokeCallback: @"Sharer Cancelled" code:kSharerCancelled];
}

- (void)sharerShowBadCredentialsAlert:(SHKSharer *)sharer 
{
    CRLog(@"Bad credentials %@", sharer);
}

- (void)sharerShowOtherAuthorizationErrorAlert:(SHKSharer *)sharer 
{
    CRLog(@"Authorization Error %@", sharer);
}

- (void)sharerAuthDidFinish:(SHKSharer *)sharer success:(BOOL)success
{
    if (!success) {
        // Cancel pressed in app authorization window
        [self invokeCallback: @"Sharer Cancelled" code:kSharerCancelled];        
    }
}


- (void) gotUserInfo:(NSNotification*)aNotification
{
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:@"SHKSendDidFinish" 
                                                  object:nil];
    
    if ([aNotification.object isMemberOfClass: SHKFacebook.class]) {
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSHKFacebookUserInfo"];
        NSLog(@"TODO: send profile facebook id and data to the server");
        NSLog(@"%@", userInfo);
        
        NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [userInfo JSONString],  @"facebook_data",
                                  nil];
        
        [[ProfileService sharedService] updateProfile:postData onSuccess:^(id responseData) {
            NSLog(@"Profile Updated: %@", responseData);
            
        } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
            NSLog(@"Profile Update Error: %@ %@", error, errorResponse);
        }];
        [self invokeCallback:@"OK" code:kSharerSuccess];
    } 
    
    if (self.pendingAction == FBWrapperAction_Friends)
        [self requestFriends];
    else if (self.pendingAction == FBWrapperAction_Invite) {
        [self sendInvitationTo: [self.pendingActionParams valueForKey:kFacebookIdParam]
                      delegate: [self.pendingActionParams valueForKey:kDelegateParam]];
    }
}



- (void)request:(FBRequest *)request didLoad:(id)result {
    NSArray *items = [(NSDictionary *)result objectForKey:@"data"];
//    for (int i=0; i<[items count]; i++) {
//        NSDictionary *friend = [items objectAtIndex:i];
//        long long fbid = [[friend objectForKey:@"id"]longLongValue];
//        NSString *name = [friend objectForKey:@"name"];
//        NSLog(@"id: %lld - Name: %@", fbid, name);
//    }

    if (self.friendsSuccess) {
        self.friendsSuccess(items);
    }
    _friendsSuccess = nil;
    _friendsFailure = nil;
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error
{
    if (self.friendsFailure) {
        self.friendsFailure(error);
    }
    _friendsFailure = nil;
    _friendsSuccess = nil;
}

- (void)requestFriends
{
    [self.facebook requestWithGraphPath:@"me/friends" andDelegate:self];
}

- (void)friends:(FBSuccessResponseBlock)successBlock onFailure:(FBErrorResponseBlock)failureBlock;
{
    self.pendingAction = FBWrapperAction_None;
    _friendsSuccess = successBlock;
    _friendsFailure = failureBlock;
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(gotUserInfo:) 
                                                 name:@"SHKSendDidFinish" 
                                               object:nil];

    if ([SHKFacebook isServiceAuthorized]) {
        [self requestFriends];
    } else {
        // force authorization within getUserInfo method
        self.pendingAction = FBWrapperAction_Friends;
        SHKFacebook *sharer = [SHKFacebook getUserInfo];
        sharer.shareDelegate = self;
    }
}


- (void)sendInvitationTo:(NSString*)facebookId delegate:(id<FBDialogDelegate>)delegate
{
    self.pendingAction = FBWrapperAction_None;
    
    // Show the UI Dialog
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"TODO: invitation message.",  @"message",
                                   facebookId, @"to",
                                   nil];

    if ([SHKFacebook isServiceAuthorized]) {
        [self.facebook dialog:@"apprequests"
                    andParams:params
                  andDelegate:delegate];
    } else {
        self.pendingAction = FBWrapperAction_Invite;
        self.pendingActionParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                    facebookId, kFacebookIdParam,
                                    delegate, kDelegateParam,
                                    nil];
        SHKFacebook *sharer = [SHKFacebook getUserInfo];
        sharer.shareDelegate = self;
    }
}


+ (NSString*)pictureURLForFacebookUserWithId:(NSString*)facebookId
{
    return [NSString stringWithFormat: @"http://graph.facebook.com/%@/picture", facebookId];
}

- (BOOL)isConfigured
{
    return [SHKFacebook isServiceAuthorized];
}

- (void)configureShare:(void (^)(id result))callback
{
    self.callback = callback;
    // Requesting user info forces authorization
    SHKFacebook *sharer = [SHKFacebook getUserInfo];
    sharer.shareDelegate = self;
}
@end

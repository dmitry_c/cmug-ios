//
//  TwitterWrapper.m
//  Cmug
//
//  Created by Dmitry C on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TwitterWrapper.h"
#import "TwitterAccountSelectViewController.h"
#import "AppDelegate.h"
#import "NSString+Additions.h"
#import "IIViewDeckController.h"
#import "ProfileService.h"

@interface TwitterWrapper()
- (void)fetchFriendsWithInfoIOS5:(BOOL) fetchUsersInfo;
- (void)requestFriendsWithInfoViaShareKit:(BOOL)fetchUsersInfo;

@property (nonatomic, copy) TWSuccessResponseBlock friendIdsSuccess;
@property (nonatomic, copy) TWErrorResponseBlock friendIdsFailure;

@property (nonatomic, copy) TWSuccessResponseBlock friendsSuccess;
@property (nonatomic, copy) TWErrorResponseBlock friendsFailure;

@property (nonatomic, copy) void(^userInfoCallback)(id userInfo);

@property (nonatomic, strong) NSArray* friendIds;
@property (nonatomic, strong) NSMutableArray* friends;

@property (nonatomic, assign) TwitterWrapperPendingAction pendingAction;
@property (nonatomic, retain) ACAccountStore* accountStore;

@property (nonatomic, retain) TwitterAccountSelectViewController *twaSelectDialog;
@end


static TwitterWrapper* instance;

@implementation TwitterWrapper
@synthesize friendIdsSuccess = _friendIdsSuccess;
@synthesize friendIdsFailure = _friendIdsFailure;

@synthesize friendsSuccess = _friendsSuccess;
@synthesize friendsFailure = _friendsFailure;
@synthesize userInfoCallback = _userInfoCallback;

@synthesize delegate = _delegate;

@synthesize friends = _friends;
@synthesize friendIds = _friendIds;

@synthesize pendingAction = _pendingAction;

@synthesize twitterAccount = _twitterAccount;
@synthesize accountStore = _accountStore;

@synthesize twaSelectDialog = _twaSelectDialog;

+ (id)get
{
    @synchronized(self) {
        if (!instance)
            instance = [TwitterWrapper new];            
    }
    return instance;    
}

- (BOOL)isConfigured
{
    BOOL configured = NO;
    if ([TWTweetComposeViewController class] == nil) {
        // Use ShareKit
        configured = [SHKTwitterExtended isServiceAuthorized];
        if (configured) {
            NSString *accountInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSHKTwitterUserInfo"];
            NSString *screenName = [accountInfo valueForKey:@"screen_name"];
            configured = (BOOL)(screenName && ![screenName isEqual:@""]);
        }
    } else {
        // Get stored twitter account username.
        // Note: the account persistance is not verified here.
        NSString* accountUsername = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsCurrentTwitterAccount];
        if (accountUsername && ![accountUsername isEqual:@""])
            configured = YES;
    }
    return configured;
}

- (void)configureShare:(void(^)(id result))callback
{
    [self userInfo:^(id userInfo) {
        callback(userInfo); 
    }];
}

- (TwitterAccountSelectViewController *)twaSelectDialog
{
    if (!_twaSelectDialog) {
        _twaSelectDialog = [[TwitterAccountSelectViewController alloc] init];
    }
    return _twaSelectDialog;
}


- (ACAccountStore *)accountStore
{
    if (!_accountStore) {
        _accountStore = [[ACAccountStore alloc] init];
    }
    return _accountStore;
}

- (NSMutableArray *)friends
{
    if (!_friends) {
        _friends = [NSMutableArray arrayWithCapacity:self.friendIds.count];
    }
    return _friends;
}

- (void) account:(TWGotCurrentAccountBlock)gotAccount
{
    if (!_twitterAccount) {
        [self currentTwitterAccount:^(ACAccount *account) {
            self.twitterAccount = account;
            gotAccount(account);
//            if (self.pendingAction == TwitterWrapperPendingAction_FriendIds) {
//                [self fetchFriendsWithInfoIOS5:NO];
//            } else if (self.pendingAction == TwitterWrapperPendingAction_Friends) {
//                [self fetchFriendsWithInfoIOS5:YES];
//            }
        }];
    } else {
        gotAccount(_twitterAccount);
    }
}

+ (void) cancelAccount
{
    NSString* accountUsername = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsCurrentTwitterAccount];
    NSLog(@"Cancelled selected account: %@", accountUsername);
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserDefaultsCurrentTwitterAccount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)screenName:(void(^)(NSString* result))gotScreenName
{
    if ([TWTweetComposeViewController class] == nil) {
        if ([SHKTwitterExtended isServiceAuthorized]) {
            NSString *accountInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSHKTwitterUserInfo"];
            NSString *screenName = [accountInfo valueForKey:@"screen_name"];
            gotScreenName(screenName);
        } else {
            gotScreenName(nil);
        }
    } else {
        [self account:^(ACAccount *account) {
            if (account) {
                gotScreenName(account.username);                
            } else {
                gotScreenName(nil);
            }
        }];
    }
}

- (void)userInfo:(void(^)(id userInfo))gotUserInfo
{
    if ([TWTweetComposeViewController class] == nil) {
        // Use ShareKit
        self.pendingAction = TwitterWrapperPendingAction_UserInfo;
        self.userInfoCallback = gotUserInfo;
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(gotUserInfo:) 
                                                     name:@"SHKSendDidFinish" 
                                                   object:nil];
        SHKSharer *sharer = [SHKTwitter getUserInfo];
        sharer.shareDelegate = self;
    } else {
        [self account:^(ACAccount *account) {
            if (account) {
                // Build a twitter request
                TWRequest *userInfoRequest = [[TWRequest alloc] initWithURL:
                                              [NSURL URLWithString:@"https://api.twitter.com/1/account/verify_credentials.json"]
                                                                 parameters:nil 
                                                              requestMethod:TWRequestMethodGET];
                // Post the request
                [userInfoRequest setAccount:account];
                
                // Block handler to manage the response
                [userInfoRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) 
                 {
                     self.pendingAction = TwitterWrapperPendingAction_None;

                     // Update profile
                     NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [responseData objectFromJSONData], @"twitter_data",
                                               nil];
                     [[ProfileService sharedService] updateProfile:postData onSuccess:^(id responseData) {
                         NSLog(@"Profile updated");
                     } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                         NSLog(@"Profile update error: %@ response: %@", error, errorResponse);
                     }];
                     
                     gotUserInfo([responseData objectFromJSONData]);
                 }];    
            } else {
                gotUserInfo(nil);
            }
        }];
    } 
}

#pragma mark -
#pragma mark SHKSharer delegate

- (void)sharerStartedSending:(SHKSharer *)sharer {}

- (void)sharerFinishedSending:(id)sharer 
{
    if ([sharer isMemberOfClass:SHKTwitterExtended.class] &&
        ((SHKTwitterExtended*)sharer).item.shareType != SHKShareTypeUserInfo) {

        switch (((SHKTwitterExtended*)sharer).item.shareType) {
            case SHKShareTypeExtended_TwitterFriendIds:
                if (self.friendIdsSuccess) {
                    self.friendIdsSuccess(((SHKTwitterExtended*)sharer).friendIds);
                }

                if ([self.delegate respondsToSelector:@selector(twitterFriendsFetchedIds:withController:)]) {
                    [self.delegate twitterFriendsFetchedIds:((SHKTwitterExtended*)sharer).friendIds withController:self];
                }

                _friendIdsSuccess = nil;
                _friendIdsFailure = nil;

                break;
                
            case SHKShareTypeExtended_TwitterFriends:
                if (self.friendsSuccess) {
                    self.friendsSuccess(((SHKTwitterExtended*)sharer).friends);
                }
                
                if ([self.delegate respondsToSelector:@selector(twitterFriendsFetchedInfo:withController:)]) {
                    [self.delegate twitterFriendsFetchedInfo:((SHKTwitterExtended*)sharer).friends withController:self];
                }
                
                _friendsSuccess = nil;
                _friendsFailure = nil;
                break;
                
            default:
                break;
        }
        
    }
}

- (void)sharer:(SHKSharer *)sharer failedWithError:(NSError *)error shouldRelogin:(BOOL)shouldRelogin
{
    if (shouldRelogin) {
        // Logout and repeat actions
        [sharer.class logout];
        if (self.pendingAction == TwitterWrapperPendingAction_FriendIds) {
            [self requestFriendsWithInfoViaShareKit:NO];
        } else if (self.pendingAction == TwitterWrapperPendingAction_Friends) {
            [self requestFriendsWithInfoViaShareKit:YES];
        }
        return;
    }
    
    NSString *title=@"Error";
    NSString *message=[NSString stringWithFormat:@"%@", error.userInfo];
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:title
                                                 message:message
                                                delegate:nil 
                                       cancelButtonTitle:@"Ok" 
                                       otherButtonTitles:nil];
    [av show];
    
    
    if ([sharer isMemberOfClass:SHKTwitterExtended.class] &&
        ((SHKTwitterExtended*)sharer).item.shareType != SHKShareTypeUserInfo) {
        
        switch (((SHKTwitterExtended*)sharer).item.shareType) {
            case SHKShareTypeExtended_TwitterFriendIds:
                if (self.friendIdsFailure) {
                    self.friendIdsFailure(error);
                }
                
                _friendIdsSuccess = nil;
                _friendIdsFailure = nil;
                
                break;
                
            case SHKShareTypeExtended_TwitterFriends:
                if (self.friendsFailure) {
                    self.friendsFailure(error);
                }
                
                _friendsSuccess = nil;
                _friendsFailure = nil;
                break;
                
            default:
                break;
        }
        
        if ([self.delegate respondsToSelector:@selector(twitterFriendsFetchFailedWithError:)]) {
            [self.delegate twitterFriendsFetchFailedWithError:error];
        }
    }
    self.pendingAction = TwitterWrapperPendingAction_None;
}


- (void)sharerCancelledSending:(SHKSharer *)sharer
{
    CRLog(@"Cancelled %@", sharer);
    if (self.pendingAction == TwitterWrapperPendingAction_UserInfo) {
        self.pendingAction = TwitterWrapperPendingAction_None;
        self.userInfoCallback(nil);
        _userInfoCallback = nil;
    }
}

- (void)sharerShowBadCredentialsAlert:(SHKSharer *)sharer 
{
    CRLog(@"Bad credentials %@", sharer);
}

- (void)sharerShowOtherAuthorizationErrorAlert:(SHKSharer *)sharer 
{
    CRLog(@"Authorization Error %@", sharer);
}

- (void) sharerAuthDidFinish:(SHKSharer *)sharer success:(BOOL)success
{
    if (!success) {
        if (self.pendingAction == TwitterWrapperPendingAction_UserInfo) {
            self.pendingAction = TwitterWrapperPendingAction_None;
            self.userInfoCallback(nil);
            _userInfoCallback = nil;
        }
    }
}


- (void) gotUserInfo:(NSNotification*)aNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:@"SHKSendDidFinish" 
                                                  object:nil];
    
    if ([aNotification.object isMemberOfClass: SHKTwitter.class]) {
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSHKTwitterUserInfo"];
        NSLog(@"%@", userInfo);

        if (self.userInfoCallback) {
            NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                      userInfo, @"twitter_data",
                                      nil];
            [[ProfileService sharedService] updateProfile:postData onSuccess:^(id responseData) {
                NSLog(@"Profile updated");
            } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
                NSLog(@"Profile update error: %@ response: %@", error, errorResponse);
            }];
            
            self.userInfoCallback(userInfo);
            _userInfoCallback = nil;
        }
    } 
    
    if (self.pendingAction == TwitterWrapperPendingAction_FriendIds) {
        [self requestFriendsWithInfoViaShareKit:NO];        
    } else if (self.pendingAction == TwitterWrapperPendingAction_Friends) {
        [self requestFriendsWithInfoViaShareKit:YES];
    }

}

- (void)requestFriendsWithInfoViaShareKit:(BOOL)fetchUsersInfo
{
    SHKTwitterExtended *sharer = [[SHKTwitterExtended alloc] init];
    sharer.shareDelegate = self;
    [sharer getFriendsWithInfo:fetchUsersInfo];
}


- (void) currentTwitterAccount:(TWGotCurrentAccountBlock)gotAccount;
{
    NSString* accountUsername = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsCurrentTwitterAccount];
    __block ACAccount* account;
    
    if (accountUsername) {
        ACAccountType *accountTypeTwitter = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        [self.accountStore requestAccessToAccountsWithType:accountTypeTwitter withCompletionHandler:^(BOOL granted, NSError *error) {
            if(granted) {
//                NSString *attributeName = @"username";
//                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K like '%@'",
//                                          attributeName, accountUsername];
                NSPredicate *unameMatchPredicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                    if ([((ACAccount*)evaluatedObject).username equalsTo:accountUsername]) {
                        return YES;
                    } else {
                        return NO;
                    }
                }];
                NSArray *accountsList = [self.accountStore accountsWithAccountType:accountTypeTwitter];
                if (accountsList.count > 0) {
                    NSArray *filteredArray = [accountsList filteredArrayUsingPredicate:unameMatchPredicate];
                    if (filteredArray.count == 1) {
                        account = (ACAccount*)[filteredArray objectAtIndex:0];                        
                    }
                }
                NSLog(@"Got Account from store %@", account);
                if (account == nil) {
                    // Probably user deleted their twitter account from settings
                    [TwitterWrapper cancelAccount];
                }
                gotAccount(account);
            } else {
                // no permission given
                [TwitterWrapper cancelAccount];
                gotAccount(nil);
            }
        }];
    } else
    if (!accountUsername) {
        // Show Twitter Account Selection Dialog
//        TwitterAccountSelectViewController *dialog = self.twaSelectDialog;
        self.twaSelectDialog.onComplete = gotAccount;
        UINavigationController *wrapper = [[UINavigationController alloc] initWithRootViewController:self.twaSelectDialog];
        UIViewController *nav = [[[AppDelegate sharedApplication] window] rootViewController];

        if (([nav respondsToSelector:@selector(modalViewController)] && nav.modalViewController) ||
            ([nav respondsToSelector:@selector(presentedViewController)] && nav.presentedViewController)) 
        {
            // if we are in modal view 
            [nav.presentedViewController presentModalViewController:wrapper animated:YES];
        } else {
            // open twitter accouny select as modal
            [nav presentModalViewController:wrapper animated:YES];
        }
    } 
}


- (void) friendsWithInfo:(BOOL)fetchUsersInfo 
            andDelegate:(id<TwitterFriendsFetchFlowDelegate>)delegate
              onSuccess:(TWSuccessResponseBlock)successBlock 
              onFailure:(TWErrorResponseBlock)failureBlock
{
    // setup "callbacks"
    self.delegate = delegate;
    if (fetchUsersInfo) {
        self.friendsFailure = failureBlock;
        self.friendsSuccess = successBlock;
    } else {
        self.friendIdsFailure = failureBlock;
        self.friendIdsSuccess = successBlock;
    }
    
    if ([TWTweetComposeViewController class] == nil) {
        // Use ShareKit
        if ([SHKTwitterExtended isServiceAuthorized]) {
            [self requestFriendsWithInfoViaShareKit:fetchUsersInfo];
        } else {
            [[NSNotificationCenter defaultCenter] addObserver:self 
                                                     selector:@selector(gotUserInfo:) 
                                                         name:@"SHKSendDidFinish" 
                                                       object:nil];
            SHKTwitterExtended *sharer = [SHKTwitterExtended getUserInfo];
            sharer.shareDelegate = self;
        }
    } else {
        [self fetchFriendsWithInfoIOS5:fetchUsersInfo];
    }
}

- (void)fetchFriendsWithInfoIOS5:(BOOL) fetchUsersInfo
{
    if (self.pendingAction == TwitterWrapperPendingAction_None) {
        if (fetchUsersInfo) {
            self.pendingAction = TwitterWrapperPendingAction_Friends;
        } else {
            self.pendingAction = TwitterWrapperPendingAction_FriendIds;
        }
    }
    
    [self account:^(ACAccount *account) {
        if (!account) {
            [[[UIAlertView alloc] initWithTitle:@"Error" 
                                        message:@"Can't get twitter account"
                                       delegate:nil 
                              cancelButtonTitle:@"Cancel" 
                              otherButtonTitles:nil] show];
            return;
        }
        
        // We have to first get user ids, then only request user info.
        NSURL *url = [NSURL URLWithString:TWITTER_FRIENDS_ENDPOINT];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"-1", @"cursor", 
                                @"screen_name", [account username],
                                nil];
        TWRequest *request = [[TWRequest alloc] initWithURL:url 
                                                 parameters:params 
                                              requestMethod:TWRequestMethodGET];
        [request setAccount:self.twitterAccount];    
        
        [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
            if ([urlResponse statusCode] == 200) {
                NSError *jsonError = nil;
                id jsonResult = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonError];
                if (jsonResult != nil) {
                    self.friendIds = [jsonResult objectForKey:@"ids"];
                    
                    if ([self.delegate respondsToSelector:@selector(twitterFriendsFetchedIds:withController:)]) {
                        [self.delegate twitterFriendsFetchedIds:self.friendIds withController:self];
                    }
                    
                    if (fetchUsersInfo) {
                        // Not sure whether I can just call the function directly here...
                        [self fetchFriendsInfoIOS5];
                    }
                }
                else {
                    NSString *message = [NSString stringWithFormat:@"Could not parse your friends list: %@", [jsonError localizedDescription]];
                    [[[UIAlertView alloc] initWithTitle:@"Error" 
                                                message:message
                                               delegate:nil 
                                      cancelButtonTitle:@"Cancel" 
                                      otherButtonTitles:nil] show];
                }
            }
        }];

    }];
}


- (void)fetchFriendsInfoIOS5
{
    // Twitter only allows getting info for up to a fixed number of user_ids at once (supposedly 100),
    // so we'll have to break the ids array into smaller pieces and make multiple TWRequests.
    
    static const int kItemsPerView = 50;
    int numViews = (int) ceil((float) [self.friendIds count] / kItemsPerView);
    
    // NSLog(@"%d", [self.friendIds count]);
    // NSLog(@"%d", numViews);
    [self account:^(ACAccount *account) {
        self.friends = nil; // clear array
        
        for (int viewIndex = 0; viewIndex < numViews; viewIndex++) {
            
            int numItems;
            
            if (viewIndex * kItemsPerView + kItemsPerView - 1 <= [self.friendIds count]) {
                numItems = kItemsPerView;
            }
            else {
                numItems = [self.friendIds count] - viewIndex * kItemsPerView;
            }
            
            // There must be a more straightforward way of slicing self.friendIds into subarrays of length at most kItemsPerView...
            
            NSRange rangeForView = NSMakeRange(viewIndex * kItemsPerView, numItems);
            NSArray *itemsForView = [self.friendIds subarrayWithRange:rangeForView];
            
            NSString *userIds = [itemsForView componentsJoinedByString:@","];
            
            NSURL *url = [NSURL URLWithString:TWITTER_INFO_LOOKUP_ENDPOINT];
            TWRequest *request = [[TWRequest alloc] initWithURL:url 
                                                     parameters:[NSDictionary dictionaryWithObjectsAndKeys:userIds, @"user_id", nil] 
                                                  requestMethod:TWRequestMethodPOST];
            [request setAccount:self.twitterAccount];
            [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                if ([urlResponse statusCode] == 200) {
                    NSError *jsonError = nil;
                    id jsonResult = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonError];
                    if (jsonResult != nil) {
                        // Append to self.friends (which must be mutable):
                        [self.friends addObjectsFromArray:jsonResult];
                        // Since there may be several parts call the delegate method instead of block invokation
                        if (self.friends.count == self.friendIds.count) {
                            if ([self.delegate respondsToSelector:@selector(twitterFriendsFetchedInfo:withController:)]) 
                            {
                                [self.delegate twitterFriendsFetchedInfo:self.friends withController:self];
                            }

                            if (self.friendsSuccess) {
                                self.friendsSuccess(self.friends);
                            }
                            _friendsSuccess = nil;
                            _friendsFailure = nil;
                            self.pendingAction = TwitterWrapperPendingAction_None;
                        }
                    }
                    else {
                        NSString *message = [NSString stringWithFormat:@"Could not parse your friends info: %@", [jsonError localizedDescription]];
                        [[[UIAlertView alloc] initWithTitle:@"Error" 
                                                    message:message
                                                   delegate:nil 
                                          cancelButtonTitle:@"Cancel" 
                                          otherButtonTitles:nil] show];
                    }
                }
            }];
        }

    }];
}


- (void) sendDirectMessage:(NSString*)message to:(NSString*)twitterScreenName
{
//    TWTweetComposeViewController *controller = [[TWTweetComposeViewController alloc] init];
//    [controller setInitialText:@"d some-twitter-username Something I want to tweet";  

    NSString *text = [NSString stringWithFormat:@"d %@ %@", twitterScreenName, message];
    [SHKTwitter shareText:text];
}

@end

//
//  SHKTwitterExtended.h
//  Cmug
//
//  Created by Dmitry C on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHKTwitter.h"

typedef enum 
{
  	SHKShareTypeExtended_TwitterFriends = 100,
	SHKShareTypeExtended_TwitterFriendIds,
	SHKShareTypeExtended_TwitterUsersInfo
} SHKShareTypeExtended;

@interface SHKTwitterExtended : SHKTwitter
@property (nonatomic, strong) NSArray* friendIds;
@property (nonatomic, strong) NSMutableArray* friends;

-(void) getFriendIds;
-(void) getFriends;
-(void) getFriendsWithInfo:(BOOL)fetchUsersInfo;
@end

//
//  NavigationManager.h
//  Cmug
//
//  Created by Dmitry C on 6/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IIViewDeckController.h"
#import "CustomTexturedNavigationBar.h"

#define kLeftViewToggleButtonImage @"btn_left_view_toggle.png"
#define kLeftViewToggleButtonImageInner @"btn_left_view_toggle_inner.png"

enum ViewControllerNamesEnum {
    kViewControllerName_Intro = 1,
    kViewControllerName_Login,
    kViewControllerName_Signup,
    kViewControllerName_Posts,
    kViewControllerName_MainMenu,
    kViewControllerName_CreateOffer,
    kViewControllerName_Following
};

@interface NavigationManager : NSObject

@property (nonatomic, retain) NSDictionary* views;

- (IIViewDeckController*) deck;

- (IIViewDeckController*) deckWithCenterViewController: (UIViewController*) viewController;
- (IIViewDeckController*) deckWithCenterViewControllerByKey: (enum ViewControllerNamesEnum) key;

- (id) viewControllerByKey: (enum ViewControllerNamesEnum) key;

// Some ViewControllers should be inside navigation controller
- (id) viewControllerByKeyAutoWrapped: (enum ViewControllerNamesEnum) key;

+ (NavigationManager*) sharedManager;

- (UINavigationController *)customizedNavigationController;

- (IIViewDeckController*) deckWithMainMenuAndCenterViewController: (UIViewController*) initialViewController;
- (IIViewDeckController*) deckWithMainMenuAndCenterViewControllerByKey: (enum ViewControllerNamesEnum) key;

- (void) setDeckCenterViewController: (id) initialViewController;
@end

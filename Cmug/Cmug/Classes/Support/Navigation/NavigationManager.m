//
//  NavigationManager.m
//  Cmug
//
//  Created by Dmitry C on 6/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NavigationManager.h"
#import "LoginViewController.h"
#import "SignupViewController.h"
#import "SignupSummaryViewController.h"
#import "PostsViewController.h"
#import "MainMenuTableViewController.h"


static NavigationManager *_instance;

@interface NavigationManager()
@property (nonatomic, strong) NSArray* wrappedViewControllers;

// The main deck
@property (nonatomic, retain) IIViewDeckController* deck;
@property (nonatomic, retain) UINavigationController *centerNavigationController;
@end


@implementation NavigationManager

+ (NavigationManager*)sharedManager
{
	@synchronized(self) 
	{	
        if (_instance == nil) 
		{
            _instance = [[super alloc] init];
        }
    }
    return _instance;
}

@synthesize views=_views;
@synthesize wrappedViewControllers=_wrappedViewControllers;
@synthesize deck=_deck, centerNavigationController=_centerNavigationController;

- (NSDictionary*) views 
{
    if (!_views) {
        _views = [NSDictionary dictionaryWithObjectsAndKeys:
                  @"IntroViewController", 
                  [NSNumber numberWithInt:kViewControllerName_Intro],
                  @"LoginViewController",
                  [NSNumber numberWithInt:kViewControllerName_Login],
                  @"SignupViewController", 
                  [NSNumber numberWithInt:kViewControllerName_Signup],
                  @"PostsViewController",
                  [NSNumber numberWithInt:kViewControllerName_Posts], 
                  @"MainMenuTableViewController",
                  [NSNumber numberWithInt:kViewControllerName_MainMenu],   
                  @"CreateOfferViewController",
                  [NSNumber numberWithInt:kViewControllerName_CreateOffer],
                  @"FollowingViewController",
                  [NSNumber numberWithInt:kViewControllerName_Following],
                  nil];
    }
    return _views;
}

- (NSArray*) wrappedViewControllers
{
    if (!_wrappedViewControllers) {
        _wrappedViewControllers = [NSArray arrayWithObjects:[NSNumber numberWithInt:kViewControllerName_CreateOffer], nil];
    }
    return _wrappedViewControllers;
}

#pragma - Interface Customization

- (UINavigationController *)customizedNavigationController
{
    UINavigationController *navController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
    
    // Ensure the UINavigationBar is created so that it can be archived. If we do not access the
    // navigation bar then it will not be allocated, and thus, it will not be archived by the
    // NSKeyedArchvier.
    [navController navigationBar];
    
    // Archive the navigation controller.
    NSMutableData *data = [NSMutableData data];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:navController forKey:@"root"];
    [archiver finishEncoding];
    //    [archiver release];
    //    [navController release];
    
    // Unarchive the navigation controller and ensure that our UINavigationBar subclass is used.
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [unarchiver setClass:[CustomTexturedNavigationBar class] forClassName:@"UINavigationBar"];
    UINavigationController *customizedNavController = [unarchiver decodeObjectForKey:@"root"];
    [unarchiver finishDecoding];
    //    [unarchiver release];
    
    // Modify the navigation bar to have a background image.
    CustomTexturedNavigationBar *navBar = (CustomTexturedNavigationBar *)[customizedNavController navigationBar];
    //    [navBar setTintColor:[UIColor colorWithRed:0.39 green:0.72 blue:0.62 alpha:1.0]];
    [navBar setTintColor:[UIColor colorWithRed:0.10 green:0.65 blue:0.80 alpha:1.0]];
    [navBar setBackgroundImage:[UIImage imageNamed:kNavBarBackgroundImageName] forBarMetrics:UIBarMetricsDefault];
    
    //    [navBar setBackgroundImage:[UIImage imageNamed:@"bg_navbar_textured_blue_landscape.png"] forBarMetrics:UIBarMetricsLandscapePhone];
    
    return customizedNavController;
}


#pragma - ViewController factory

- (IIViewDeckController*) deck
{
    IIViewDeckController *deck = [[IIViewDeckController alloc] initWithCenterViewController:nil];
    deck.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
    return deck;
}


- (IIViewDeckController*) deckWithCenterViewController: (UIViewController*) viewController
{
    UINavigationController *centerViewController = [self customizedNavigationController];
    
    [centerViewController setViewControllers:[NSArray arrayWithObject:viewController]];
    
    IIViewDeckController *deck = [[IIViewDeckController alloc] initWithCenterViewController:centerViewController];
    deck.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToCloseBouncing;
    return deck;
}


- (IIViewDeckController*) deckWithCenterViewControllerByKey: (enum ViewControllerNamesEnum) key
{
    IIViewDeckController *deck = [[IIViewDeckController alloc] initWithCenterViewController:[self viewControllerByKey:key]];
    deck.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
    return deck;
}


- (id) viewControllerByKey: (enum ViewControllerNamesEnum) key
{
    NSString* viewControllerName = [self.views objectForKey:[NSNumber numberWithInt:key]];
    return [[NSClassFromString(viewControllerName) alloc] initWithNibName:viewControllerName bundle:nil];
}

- (id) viewControllerByKeyAutoWrapped: (enum ViewControllerNamesEnum) key 
{
    NSString* viewControllerName = [self.views objectForKey:[NSNumber numberWithInt:key]];
    id initialViewController = [[NSClassFromString(viewControllerName) alloc] initWithNibName:viewControllerName bundle:nil];
    return initialViewController;
}


- (void) setupMenuToggleButtonFor:(UIViewController*) initialViewController andDeck:(IIViewDeckController*)deck
{
    UIImage *toggleImage = [UIImage imageNamed:kLeftViewToggleButtonImage];
    UIButton *toggleButtonView =  [UIButton buttonWithType:UIButtonTypeCustom];
    toggleButtonView.frame = CGRectMake(0.0, 0.0, toggleImage.size.width, toggleImage.size.height);
    //    [toggleButtonView setImage:toggleImage forState:UIControlStateNormal];
    [toggleButtonView setBackgroundImage:toggleImage forState:UIControlStateNormal];
    toggleButtonView.showsTouchWhenHighlighted = NO;
    [toggleButtonView addTarget:deck action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *toggleButton = [[UIBarButtonItem alloc] initWithCustomView:toggleButtonView];
    //    toggleButton.target = centerViewController.viewDeckController;
    //    toggleButton.action = @selector(toggleLeftView);
    
    initialViewController.navigationItem.leftBarButtonItem = toggleButton;
}


- (IIViewDeckController*) deckWithMainMenuAndCenterViewController: (UIViewController*) initialViewController
{
    UIViewController* mainMenuViewController = [self viewControllerByKey:kViewControllerName_MainMenu];
    
    UINavigationController *centerViewController = [self customizedNavigationController];
    
    [centerViewController setViewControllers:[NSArray arrayWithObject:initialViewController]];
    
    // set button
    if ([centerViewController.navigationItem respondsToSelector:@selector(setLeftItemsSupplementBackButton:)]) {
        [centerViewController.navigationItem setLeftItemsSupplementBackButton:YES];
    } else {
        // ios4
        CRLog(@"TODO: supplement menu button");
    }
    
    IIViewDeckController *deck = [[IIViewDeckController alloc] initWithCenterViewController:centerViewController leftViewController:mainMenuViewController];

    deck.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToCloseBouncing;
    // Setup toggle
    [self setupMenuToggleButtonFor:initialViewController andDeck: centerViewController.viewDeckController];

    self.deck = deck;
    self.centerNavigationController = centerViewController;
    
    return deck;
}


- (void) setDeckCenterViewController: (id) initialViewController
{
    if (self.deck) {
        if ([self.centerNavigationController isKindOfClass:UINavigationController.class]) {
            [self.centerNavigationController setViewControllers:[NSArray arrayWithObject:initialViewController]];
            
            [self setupMenuToggleButtonFor:initialViewController andDeck:self.centerNavigationController.viewDeckController];
        }
    }
}


- (IIViewDeckController*) deckWithMainMenuAndCenterViewControllerByKey: (enum ViewControllerNamesEnum) key
{
    return [self deckWithMainMenuAndCenterViewController:[self viewControllerByKey:key]];
}

@end

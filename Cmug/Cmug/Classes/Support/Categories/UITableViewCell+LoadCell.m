//
//  UITableViewCell+LoadCell.m
//  Cmug
//
//  Created by Dmitry C on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UITableViewCell+LoadCell.h"

@implementation UITableViewCell (LoadCell)

+ (id)loadFromNib
{
    NSString *nibName = NSStringFromClass(self.class);
    return [self loadFromNibNamed:nibName];
}

+ (id)loadFromNibNamed:(NSString*)nibName
{
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[self class]])
        {
            return currentObject;
        }
    }
    
    return nil;
}
@end

//
//  NSNull+Additions.h
//  Cmug
//
//  Created by Dmitry C on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNull (Additions)
+ (id) nullOrValue:(id)value;
+ (id) emptyStringOrValue:(id)value;
@end

//
//  NSString+Additions.m
//  Cmug
//
//  Created by Dmitry C on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)
- (BOOL) hasSubstring:(NSString*)substring
{
    if ([self rangeOfString:substring].location == NSNotFound) {
        return NO;
    } else {
        return YES;
    }   
}

- (BOOL) equalsTo:(NSString*)str
{
    NSComparisonResult result = [self compare:str];
    return (result == NSOrderedSame);
}

- (NSString*)stringFromCleanedTag
{
    NSString* trimmedTag = [self stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];

    // replace redundant spaces if any
    NSRange whiteSpaceRange;
    do {
        whiteSpaceRange = [trimmedTag rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (whiteSpaceRange.length > 1) {
            trimmedTag = [trimmedTag stringByReplacingCharactersInRange:whiteSpaceRange withString:@" "];
        }
    } while (whiteSpaceRange.length > 1);

    // surround a tag with quotes if it contains space
    if ([trimmedTag hasSubstring:@" "] && (![trimmedTag hasSubstring:@"\""])) {
        trimmedTag = [NSString stringWithFormat:@"\"%@\"", trimmedTag];
    }
    return trimmedTag;
}
@end

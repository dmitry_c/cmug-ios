//
//  UIImage+CappedImage.h
//  Cmug
//
//  Created by Dmitry C on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CappedImage)
- (UIImage*)cappedImageWithCapInsets:(UIEdgeInsets)insets;
@end

//
//  UIView+LoadFromNib.m
//  Cmug
//
//  Created by Dmitry C on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIView+LoadFromNib.h"

@implementation UIView (LoadFromNib)
+ (id) loadFromNib:(NSString*)nibName
{
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[self class]])
        {
            return currentObject;
        }
    }
    return nil;
}

+ (id) loadFromNib
{
    return [self loadFromNib: NSStringFromClass(self)];
}
@end

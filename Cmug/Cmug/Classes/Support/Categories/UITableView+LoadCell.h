//
//  UITableView+LoadCell.h
//  Cmug
//
//  Created by Dmitry C on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (LoadCell)
- (id) loadCellNamed:(NSString*)nibName;
- (id) loadCellNamed:(NSString *)nibName withClass:(Class)CellClass;
- (id) loadCellWithClass:(Class)CellClass;
@end

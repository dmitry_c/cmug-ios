//
//  UITableView+LoadCell.m
//  Cmug
//
//  Created by Dmitry C on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UITableView+LoadCell.h"
#import "UITableViewCell+LoadCell.h"

@implementation UITableView (LoadCell)

- (id) loadCellNamed:(NSString*)nibName
{
    Class CellClass = NSClassFromString(nibName);
    return [self loadCellNamed:nibName withClass:CellClass];
}

- (id) loadCellNamed:(NSString *)nibName withClass:(Class)CellClass
{
    if (![CellClass isSubclassOfClass:UITableViewCell.class]) {
        return nil;
    }

    NSString* reuseIdentifier = nil;
    reuseIdentifier = [[CellClass.class new] reuseIdentifier];        

    if (!reuseIdentifier) {
        reuseIdentifier = [NSString stringWithFormat:@"ID%@", nibName];
    }

    // for ios5 
    if ([self respondsToSelector:@selector(registerNib:forCellReuseIdentifier:)]) {
        [self registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:reuseIdentifier];
    }

    id cell = [self dequeueReusableCellWithIdentifier:reuseIdentifier];

    if (cell == nil) {
        cell = [CellClass.class loadFromNib];
    }
    return cell;
}

- (id) loadCellWithClass:(Class)CellClass
{
    if (![CellClass isSubclassOfClass:UITableViewCell.class]) {
        return nil;
    }
    
    NSString* CellIdentifier = [NSString stringWithFormat:@"ID%@", NSStringFromClass(CellClass.class)];

    id cell = [self dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[CellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    return cell;
}
@end

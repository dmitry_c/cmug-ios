//
//  UIView+LoadFromNib.h
//  Cmug
//
//  Created by Dmitry C on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (LoadFromNib)
+ (id) loadFromNib:(NSString*)nibName;
+ (id) loadFromNib;
@end

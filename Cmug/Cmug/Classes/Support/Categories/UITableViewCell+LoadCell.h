//
//  UITableViewCell+LoadCell.h
//  Cmug
//
//  Created by Dmitry C on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (LoadCell)
+ (id)loadFromNib;
+ (id)loadFromNibNamed:(NSString*)nibName;
@end

//
//  UIImage+CappedImage.m
//  Cmug
//
//  Created by Dmitry C on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIImage+CappedImage.h"

@implementation UIImage (CappedImage)
- (UIImage*)cappedImageWithCapInsets:(UIEdgeInsets)insets
{
    if ([self respondsToSelector:@selector(resizableImageWithCapInsets:)])
        return [self resizableImageWithCapInsets:insets];

    if ([self respondsToSelector:@selector(stretchableImageWithLeftCapWidth:topCapHeight:)])
        return [self stretchableImageWithLeftCapWidth:insets.left topCapHeight:self.size.height];
    
    return self;
}
@end

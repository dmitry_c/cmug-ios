//
//  NSString+Additions.h
//  Cmug
//
//  Created by Dmitry C on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)
- (BOOL) hasSubstring:(NSString*)substring;
- (BOOL) equalsTo:(NSString*)str;
- (NSString*)stringFromCleanedTag;
@end

//
//  NSNull+Additions.m
//  Cmug
//
//  Created by Dmitry C on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSNull+Additions.h"

@implementation NSNull (Additions)

+ (id) nullOrValue:(id)value
{
    if (value) {
        return value;
    } else {
        return [NSNull null];
    }
}

+ (id) emptyStringOrValue:(id)value
{
    if (value) {
        return value;
    } else {
        return @"";
    }
}
@end

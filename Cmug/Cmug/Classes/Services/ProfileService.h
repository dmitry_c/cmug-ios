//
//  ProfileService.h
//  Cmug
//
//  Created by Dmitry C on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseService.h"
#import "LoginRequest.h"
#import "ExtendedLoginRequest.h"
#import "SignupRequest.h"
#import "ProfileShort.h"
#import "LoginSignupResponse.h"

@interface ProfileService : BaseService

- (void) login: (LoginRequest*) requestData 
       onSuccess: (void (^) (LoginSignupResponse* responseData)) successBlock
       onFailure: (ServiceErrorResponseBlock) failureBlock;

- (void) signup: (SignupRequest*) requestData 
     onSuccess: (void (^) (LoginSignupResponse* responseData)) successBlock
     onFailure: (ServiceErrorResponseBlock) failureBlock;

- (void)updateProfile:(NSDictionary *)requestData 
            onSuccess:(ServiceSuccessResponseBlock)successBlock 
            onFailure:(ServiceErrorResponseBlock)failureBlock;
@end

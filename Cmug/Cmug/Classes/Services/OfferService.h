//
//  OfferService.h
//  Cmug
//
//  Created by Dmitry C on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseService.h"
#import "CreateOfferRequest.h"
#import "Offer.h"

@interface OfferService : BaseService

- (void) createOffer: (CreateOfferRequest*) offer
           onSuccess: (ServiceSuccessResponseBlock) successBlock
           onFailure: (ServiceErrorResponseBlock) failureBlock;

@end

//
//  OfferService.m
//  Cmug
//
//  Created by Dmitry C on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OfferService.h"

@implementation OfferService

+ (void) setupMappingsAndRoutes
{
    [super setupMappingsAndRoutes];
    
    // create offer serialization mapping
    RKObjectMapping* createOfferSerializationMapping = [[CreateOfferRequest getMapping] inverseMapping]; 
    [[RKObjectManager sharedManager].mappingProvider setSerializationMapping:createOfferSerializationMapping forClass:[CreateOfferRequest class]];
    
    // create offer response mapping
    RKObjectMapping* createOfferResponse = [Offer getMapping]; 
    [[RKObjectManager sharedManager].mappingProvider addObjectMapping:createOfferResponse];
    
    // setting routes
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager.router routeClass:[CreateOfferRequest class] toResourcePath:@"/offer/" forMethod:RKRequestMethodPOST];
//    [objectManager.router routeClass:[Offer class] toResourcePathPattern:@"/account/:login/" forMethod:RKRequestMethodPOST];
    
}

+ (void) initialize
{
    // This method dispatches only once
    [self setupMappingsAndRoutes];
}


- (void) createOffer: (CreateOfferRequest*) offer
           onSuccess: (ServiceSuccessResponseBlock) successBlock
           onFailure: (ServiceErrorResponseBlock) failureBlock
{
    NSLog(@"Create Offer Post");
    [self postObject:offer onSuccess:successBlock onFailure:failureBlock usingBlock:^(RKObjectLoader *loader) {
        loader.targetObject = nil; //[Offer new];
        loader.objectMapping = [loader.mappingProvider objectMappingForClass:Offer.class];
    }];
}

@end

//
//  BaseService.h
//  Cmug
//
//  Created by Dmitry C on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"
#import "ErrorResponse.h"
#import "Settings.h"
#import "ApiClient.h"

typedef void(^ServiceSuccessResponseBlock)(id responseData);
typedef void(^ServiceErrorResponseBlock)(NSError* error, ErrorResponse* errorResponse);

@interface BaseService : NSObject <RKRequestDelegate, RKObjectLoaderDelegate>
+ (id)sharedService;

+ (void) setupMappingsAndRoutes;

- (void) postObject: (id) requestData
          onSuccess: (ServiceSuccessResponseBlock) successBlock
          onFailure: (ServiceErrorResponseBlock) failureBlock
         usingBlock: (void (^)(RKObjectLoader* loader)) loaderBlock;

- (void) postObject: (id) requestData
          onSuccess: (ServiceSuccessResponseBlock) successBlock
          onFailure: (ServiceErrorResponseBlock) failureBlock;

- (void) putObject: (id) requestData
         onSuccess: (ServiceSuccessResponseBlock) successBlock
         onFailure: (ServiceErrorResponseBlock) failureBlock
        usingBlock: (void (^)(RKObjectLoader* loader)) loaderBlock;

- (void) putObject: (id) requestData
         onSuccess: (ServiceSuccessResponseBlock) successBlock
         onFailure: (ServiceErrorResponseBlock) failureBlock;

- (void) loadObjectsAtResourcePath: (NSString*)resourcePath 
                  onDidLoadObjects: (RKObjectLoaderDidLoadObjectsBlock)didLoadObjectsBlock 
                         onSuccess: (ServiceSuccessResponseBlock) successBlock
                         onFailure: (ServiceErrorResponseBlock) failureBlock
                        usingBlock: (void (^)(RKObjectLoader* loader)) loaderBlock;


- (void) loadObjectsAtResourcePath: (NSString*)resourcePath 
                  onDidLoadObjects: (RKObjectLoaderDidLoadObjectsBlock)didLoadObjectsBlock 
                         onSuccess: (ServiceSuccessResponseBlock) successBlock
                         onFailure: (ServiceErrorResponseBlock) failureBlock;


@end

//
//  ProfileService.m
//  Cmug
//
//  Created by Dmitry C on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProfileService.h"
#import "AuthorizationManager.h"
#import "GenericResponse.h"

@implementation ProfileService

+ (void) setupMappingsAndRoutes
{
    [super setupMappingsAndRoutes];
    
    // login serialization mapping
    RKObjectMapping* loginSerializationMapping = [[LoginRequest getMapping] inverseMapping]; 
    [[RKObjectManager sharedManager].mappingProvider setSerializationMapping:loginSerializationMapping forClass:[LoginRequest class]];

    RKObjectMapping* extLoginSerializationMapping = [[ExtendedLoginRequest getMapping] inverseMapping]; 
    [[RKObjectManager sharedManager].mappingProvider setSerializationMapping:extLoginSerializationMapping forClass:[ExtendedLoginRequest class]];
    
    // signup serialization mapping
    RKObjectMapping* signupSerializationMapping = [[SignupRequest getMapping] inverseMapping]; 
    [[RKObjectManager sharedManager].mappingProvider setSerializationMapping:signupSerializationMapping forClass:[SignupRequest class]];
    
    // login/signup response mapping
    RKObjectMapping* accountMapping = [LoginSignupResponse getMapping]; 
    [[RKObjectManager sharedManager].mappingProvider addObjectMapping:accountMapping];

    
//    [[RKObjectManager sharedManager].mappingProvider setObjectMapping:accountMapping forResourcePathPattern:@"/account/:login/"];
//    [[RKObjectManager sharedManager].mappingProvider setObjectMapping:accountMapping forKeyPath:@"/account/login/"];

    // setting routes
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager.router routeClass:[LoginRequest class] toResourcePath:@"/account/login/" forMethod:RKRequestMethodPOST];
    [objectManager.router routeClass:[SignupRequest class] toResourcePath:@"/account/" forMethod:RKRequestMethodPOST];
    [objectManager.router routeClass:[LoginSignupResponse class] toResourcePathPattern:@"/account/:login/" forMethod:RKRequestMethodPOST];

}

+ (void)initialize
{
    // This method dispatches only once
    [self setupMappingsAndRoutes];
}


- (void) postObject:(id)requestData 
          onSuccess:(ServiceSuccessResponseBlock) successBlock 
          onFailure:(ServiceErrorResponseBlock) failureBlock
{
    [self postObject:requestData onSuccess:successBlock onFailure:failureBlock usingBlock:^(RKObjectLoader *loader) {
        // update class mapping from LoginRequest to LoginSignupResponse
        loader.targetObject = nil; //[LoginSignupResponse new];
        loader.objectMapping = [loader.mappingProvider objectMappingForClass:LoginSignupResponse.class];
    }];
}

- (void)updateProfile:(NSDictionary *)requestData 
            onSuccess:(ServiceSuccessResponseBlock)successBlock 
            onFailure:(ServiceErrorResponseBlock)failureBlock
{
    [[ApiClient sharedClient] put:@"/account/" usingBlock:^(RKRequest *request) {
        request.params = [[RKParams alloc] initWithDictionary:requestData];
        request.onDidLoadResponse = ^(RKResponse *response) {
            NSError *error;
            id parsedResponse = [response parsedBody:&error];
            
            if (response.statusCode == 200 && !error) {
                successBlock(parsedResponse);
            } else {
                failureBlock(error, parsedResponse);
            }
        };

        request.onDidFailLoadWithError = ^(NSError *error) {
            failureBlock(error, nil);
        };
        
    }];
}

- (void) login: (LoginRequest*) requestData 
       onSuccess: (void (^) (LoginSignupResponse* responseData)) successBlock
       onFailure: (ServiceErrorResponseBlock) failureBlock
{
    
    [self postObject:requestData onSuccess:successBlock onFailure:failureBlock];
}

- (void) signup: (SignupRequest*) requestData 
      onSuccess: (void (^) (LoginSignupResponse* responseData)) successBlock
      onFailure: (ServiceErrorResponseBlock) failureBlock
{
    [self postObject:requestData onSuccess:^(LoginSignupResponse* responseData) {
        [[AuthorizationManager sharedManager] markRegistered];
        successBlock(responseData);   
    } onFailure:failureBlock];
}

@end

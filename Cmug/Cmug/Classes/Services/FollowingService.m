//
//  FollowingService.m
//  Cmug
//
//  Created by Dmitry C on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FollowingService.h"

@implementation FollowingService

+ (void) setupMappingsAndRoutes
{
    [super setupMappingsAndRoutes];
    
    // GET /social/follow/ mapping
    RKObjectMapping* followingMapping = [FollowingResponse getMapping]; 
    [[RKObjectManager sharedManager].mappingProvider setObjectMapping:followingMapping forResourcePathPattern:@"/social/follow/"];

    // POST /social/match/ mapping
    RKObjectMapping* matchMapping = [MatchRequest getMapping]; 
//    [[RKObjectManager sharedManager].mappingProvider setObjectMapping:matchMapping forResourcePathPattern:@"/social/match/"];
    [[RKObjectManager sharedManager].mappingProvider setSerializationMapping: [matchMapping inverseMapping] forClass:MatchRequest.class];
    
//    [[RKObjectManager sharedManager].mappingProvider setObjectMapping:[MatchResponse getMapping] forResourcePathPattern:@"/social/match/"];
    [[RKObjectManager sharedManager].mappingProvider setObjectMapping:[MatchResponse getDynamicMapping] forResourcePathPattern:@"/social/match/"];
    
    // setting routes
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager.router routeClass:MatchRequest.class toResourcePath:@"/social/match/" forMethod:RKRequestMethodPOST];
    [objectManager.router routeClass:MatchResponse.class toResourcePath:@"/social/match/" forMethod:RKRequestMethodPOST];
}

+ (void)initialize
{
    [self setupMappingsAndRoutes];
}


- (void) loadFollowedUsersWithLimit: (NSInteger) limit 
                 andOffset: (NSInteger) offset
                 onSuccess: (ServiceSuccessResponseBlock) successBlock
                 onFailure: (ServiceErrorResponseBlock) failureBlock
{
    NSDictionary *queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithInt:limit], @"limit", 
                                 [NSNumber numberWithInt:offset], @"offset",
                                 nil];
    NSString *resourcePath = [@"/social/follow/" stringByAppendingQueryParameters:queryParams];
    
    [self loadObjectsAtResourcePath:resourcePath onDidLoadObjects:^(NSArray *objects) {
//        NSLog(@"Did load objects %@", objects);
    } onSuccess:^(id responseData) {

        if (successBlock)
            successBlock([((NSArray*)responseData) objectAtIndex:0]);
        
    } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
        if (failureBlock)
            failureBlock(error, errorResponse);
        
    } usingBlock:^(RKObjectLoader *loader) {
        // NOTE: mappings have been set for resource path @"/social/follow/"
        loader.objectMapping = [loader.mappingProvider objectMappingForClass:FollowingResponse.class];
        loader.targetObject = nil;
    }];
}

- (void) followUserWithId: (NSString*) userId
                onSuccess: (ServiceSuccessResponseBlock) successBlock
                onFailure: (ServiceErrorResponseBlock) failureBlock
{
    [[[RKObjectManager sharedManager] client] post:[NSString stringWithFormat:@"/social/follow/%@/", userId] usingBlock:^(RKRequest *request) {
        request.onDidLoadResponse = ^(RKResponse *response) {
            if (response.statusCode / 100 == 2) // should be 201 (followed), or 200 (already followed)
                successBlock(response);
            else
                failureBlock(nil, nil);
        };
        request.onDidFailLoadWithError = ^(NSError *error) {
            failureBlock(error, nil);
        };
    }];
}

- (void) unfollowUserWithId: (NSString*) userId
                onSuccess: (ServiceSuccessResponseBlock) successBlock
                onFailure: (ServiceErrorResponseBlock) failureBlock
{
    [[[RKObjectManager sharedManager] client] delete:[NSString stringWithFormat:@"/social/follow/%@/", userId] usingBlock:^(RKRequest *request) {
        request.onDidLoadResponse = ^(RKResponse *response) {
            if (response.statusCode == 204) // 204 - unfollowed
                successBlock(response);
            else // 404
                failureBlock(nil, nil);
        };
        request.onDidFailLoadWithError = ^(NSError *error) {
            failureBlock(error, nil);
        };
    }];
}

- (void) matchProfiles: (NSArray*) items
            fromSource: (ProfileMatchSource) source
             onSuccess: (ServiceSuccessResponseBlock) successBlock
             onFailure: (ServiceErrorResponseBlock) failureBlock
{
    [self postObject:[MatchRequest requestFromSource:source andItems:items] onSuccess:^(id responseData) {
        NSLog(@"Success: %@", responseData);
        successBlock(responseData);
    } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
        NSLog(@"Error: %@", error);
        failureBlock(error, errorResponse);
    } usingBlock:^(RKObjectLoader *loader) {
        // NOTE: mappings have been set for resource path /social/match/
        loader.targetObject = nil;
        loader.objectMapping = [loader.mappingProvider objectMappingForClass:MatchResponse.class];
    }];

//    [self loadObjectsAtResourcePath:@"/social/match/" onDidLoadObjects:^(NSArray *objects) {
//        NSLog(@"Did load objects: %@", objects);
//    } onSuccess:^(id responseData) {
//        NSLog(@"Success: %@", responseData);
//        successBlock(responseData);
//    } onFailure:^(NSError *error, ErrorResponse *errorResponse) {
//        NSLog(@"Error: %@", error);
//        failureBlock(error, errorResponse);
//    } usingBlock:^(RKObjectLoader *loader) {
//        loader.method = RKRequestMethodPOST;
//        loader.targetObject = nil;
//        loader.objectMapping = [loader.mappingProvider objectMappingForClass:MatchRequest.class];
//    }];
    
//    [[[RKObjectManager sharedManager] client] post:@"/social/match/" usingBlock:^(RKRequest *request) {
//        RKParams *params = [[RKParams alloc] initWithDictionary:
//                            [NSDictionary dictionaryWithKeysAndObjects:
//                             @"source", [NSNumber numberWithInt:source], 
//                             @"items", items,
//                             nil]];
//        request.params = params;
//        request.onDidLoadResponse = ^(RKResponse *response) {
//            NSError *error;
//            NSDictionary *results = [response parsedBody:&error];
//            if (!error) {
//                NSDictionary *matchedProfilesDict = [results valueForKey:@"matches"];
//                NSMutableArray *resultList = [NSMutableArray arrayWithCapacity: items.count];
//                
//                for (NSDictionary* profileDict in items) {
//                    [resultList addObject:[ProfileMatch matchWithSource:profileDict andResponse:[matchedProfilesDict valueForKey:[profileDict valueForKey:@"recordId"]]]];
//                }
//
//                successBlock(results);
//            } else {
//                failureBlock(error, nil);
//            }
//        };
//
//        request.onDidFailLoadWithError = ^(NSError *error) {
//            failureBlock(error, nil);
//        };
//    }];
}


- (void) inviteAccount: (id) accountInfo
             onSuccess: (ServiceSuccessResponseBlock) successBlock
             onFailure: (ServiceErrorResponseBlock) failureBlock
{
    NSLog(@"Track Invitations");
    // Will be used for tracking invitations - 
    // store information about already invited users on client
    // or post to the server
    successBlock(accountInfo);
}

@end

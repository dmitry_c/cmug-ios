//
//  BaseService.m
//  Cmug
//
//  Created by Dmitry C on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseService.h"
#import "UIAlertView+Additions.h"

@implementation BaseService 

static id instance;

+ (id)sharedService
{
    @synchronized(self) {
        if (instance == nil)
            instance = [self new];
    }
    return instance;
}

+ (void) setupMappingsAndRoutes 
{
    [ErrorResponse initialize];
}


// Loader setup with default actions and callbacks,
// used for all kind of request methods.
- (void)setupLoader:(RKObjectLoader *)loader 
          onSuccess:(ServiceSuccessResponseBlock)successBlock 
          onFailure:(ServiceErrorResponseBlock)failureBlock
         usingBlock:(void (^)(RKObjectLoader* loader))loaderBlock
{
    //        loader.objectMapping = [[RKObjectManager sharedManager].mappingProvider objectMappingForClass:[LoginSignupResponse class]];
    if (loaderBlock) {
        loaderBlock(loader);
    }
    
    loader.onDidLoadResponse = ^(RKResponse* r) {
        NSLog(@"%d %@", [r statusCode], [r bodyAsString]);
    };
    
    loader.onDidFailWithError = ^(NSError* error){ 
        NSArray* e = [[error userInfo] valueForKey:RKObjectMapperErrorObjectsKey];
        
        NSLog(@"CLASS: %@", [[e objectAtIndex:0] class]);
        if (error.domain == NSURLErrorDomain) {
            NSLog(@"Server is down");
            [UIAlertView showWithTitle:@"Oops" andMessage:@"Server is down"];
        } else if (error.domain == RKErrorDomain) {
            NSLog(@"onDidFailLoadWithError: %@\nUSER INFO:%@", error, [[error userInfo] valueForKey:RKObjectMapperErrorObjectsKey]);
            // Show alert with list of errors
            // TODO:
            // replace this with MBProgressHud message
            [UIAlertView showErrorResponse:((ErrorResponse*)[e objectAtIndex:0])];
        }
        
        failureBlock(error, (ErrorResponse*)[e objectAtIndex:0]); 
    }; 
    
    loader.onDidLoadObject = ^(id object) { 
        successBlock(object); 
    };
}


- (void) postObject: (id) requestData
          onSuccess: (ServiceSuccessResponseBlock) successBlock
          onFailure: (ServiceErrorResponseBlock) failureBlock
         usingBlock: (void (^)(RKObjectLoader* loader)) loaderBlock
{
    [[RKObjectManager sharedManager] postObject:requestData usingBlock:^(RKObjectLoader *loader) {
        [self setupLoader:loader onSuccess:successBlock onFailure:failureBlock usingBlock:loaderBlock];
    }];
}

- (void) putObject: (id) requestData
         onSuccess: (ServiceSuccessResponseBlock) successBlock
         onFailure: (ServiceErrorResponseBlock) failureBlock
        usingBlock: (void (^)(RKObjectLoader* loader)) loaderBlock
{
    [[RKObjectManager sharedManager] putObject:requestData usingBlock:^(RKObjectLoader *loader) {
        [self setupLoader:loader onSuccess:successBlock onFailure:failureBlock usingBlock:loaderBlock];
    }];
}


- (void) postObject: (id) requestData
          onSuccess: (ServiceSuccessResponseBlock) successBlock
          onFailure: (ServiceErrorResponseBlock) failureBlock
{
    [self postObject:requestData onSuccess:successBlock onFailure:failureBlock usingBlock:nil];
}

- (void) putObject: (id) requestData
         onSuccess: (ServiceSuccessResponseBlock) successBlock
         onFailure: (ServiceErrorResponseBlock) failureBlock
{
    [self putObject:requestData onSuccess:successBlock onFailure:failureBlock usingBlock:nil];
}

- (void) loadObjectsAtResourcePath: (NSString*)resourcePath 
                  onDidLoadObjects: (RKObjectLoaderDidLoadObjectsBlock)didLoadObjectsBlock 
                         onSuccess: (ServiceSuccessResponseBlock) successBlock
                         onFailure: (ServiceErrorResponseBlock) failureBlock
                        usingBlock: (void (^)(RKObjectLoader* loader)) loaderBlock
{
    [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader *loader) {
        if (loaderBlock) {
            loaderBlock(loader);
        }
        
        loader.onDidLoadObjects = ^(NSArray* objects) {
            if (successBlock != nil)
                successBlock(objects);
            didLoadObjectsBlock(objects);
        };
        
//        loader.onDidLoadObjectsDictionary = ^(NSDictionary* objects) {
//            NSLog(@"Dictionary Loaded %@", objects);
//        };
//        
//        loader.onDidLoadResponse = ^(RKResponse* r) {
//            NSLog(@"RESPONSE: %d %@", [r statusCode], [r bodyAsString]);
//        };
        
        loader.onDidFailWithError = ^(NSError* error){ 
            NSArray* e = [[error userInfo] valueForKey:RKObjectMapperErrorObjectsKey];
            
            NSLog(@"CLASS: %@", [[e objectAtIndex:0] class]);
            if (error.domain == NSURLErrorDomain) {
                NSLog(@"Server is down");
                [UIAlertView showWithTitle:@"Oops" andMessage:@"Server is down"];
            } else if (error.domain == RKErrorDomain) {
                NSLog(@"onDidFailLoadWithError: %@\nUSER INFO:%@", error, [[error userInfo] valueForKey:RKObjectMapperErrorObjectsKey]);
                // Show alert with list of errors
                // TODO:
                // replace this with MBProgressHud message
                [UIAlertView showErrorResponse:((ErrorResponse*)[e objectAtIndex:0])];
            }
            
            failureBlock(error, (ErrorResponse*)[e objectAtIndex:0]); 
        }; 
        
    }];
}

- (void) loadObjectsAtResourcePath: (NSString*)resourcePath 
                  onDidLoadObjects: (RKObjectLoaderDidLoadObjectsBlock)didLoadObjectsBlock 
                         onSuccess: (ServiceSuccessResponseBlock) successBlock
                         onFailure: (ServiceErrorResponseBlock) failureBlock
{
    [self loadObjectsAtResourcePath:resourcePath onDidLoadObjects:didLoadObjectsBlock onSuccess:successBlock onFailure:failureBlock usingBlock:nil];
}


/**
 * Sent when a request has finished loading
 */
- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response {}


/**
 * Sent when a request has failed due to an error
 */
- (void)request:(RKRequest *)request didFailLoadWithError:(NSError *)error {}

/**
 * Sent when a request has started loading
 */
- (void)requestDidStartLoad:(RKRequest *)request {}

/**
 * Sent when a request has uploaded data to the remote site
 */
- (void)request:(RKRequest *)request didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite 
{
    
}

/**
 * Sent when request has received data from remote site
 */
- (void)request:(RKRequest*)request didReceivedData:(NSInteger)bytesReceived totalBytesReceived:(NSInteger)totalBytesReceived totalBytesExectedToReceive:(NSInteger)totalBytesExpectedToReceive {}

/**
 * Sent to the delegate when a request was cancelled
 */
- (void)requestDidCancelLoad:(RKRequest *)request {}

/**
 * Sent to the delegate when a request has timed out. This is sent when a
 * backgrounded request expired before completion.
 */
- (void)requestDidTimeout:(RKRequest *)request {}






/**
 * Sent when an object loaded failed to load the collection due to an error
 */
- (void)objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error 
{
    //REQUIRED

}



/**
 When implemented, sent to the delegate when the object laoder has completed successfully
 and loaded a collection of objects. All objects mapped from the remote payload will be returned
 as a single array.
 */
- (void)objectLoader:(RKObjectLoader *)objectLoader didLoadObjects:(NSArray *)objects {}

/**
 When implemented, sent to the delegate when the object loader has completed succesfully. 
 If the load resulted in a collection of objects being mapped, only the first object
 in the collection will be sent with this delegate method. This method simplifies things
 when you know you are working with a single object reference.
 */
- (void)objectLoader:(RKObjectLoader *)objectLoader didLoadObject:(id)object {}

/**
 When implemented, sent to the delegate when an object loader has completed successfully. The
 dictionary will be expressed as pairs of keyPaths and objects mapped from the payload. This
 method is useful when you have multiple root objects and want to differentiate them by keyPath.
 */
- (void)objectLoader:(RKObjectLoader *)objectLoader didLoadObjectDictionary:(NSDictionary *)dictionar {}

/**
 Invoked when the object loader has finished loading
 */
- (void)objectLoaderDidFinishLoading:(RKObjectLoader *)objectLoader {}

/**
 Informs the delegate that the object loader has serialized the source object into a serializable representation
 for sending to the remote system. The serialization can be modified to allow customization of the request payload independent of mapping.
 
 @param objectLoader The object loader performing the serialization.
 @param sourceObject The object that was serialized.
 @param serialization The serialization of sourceObject to be sent to the remote backend for processing.
 */
- (void)objectLoader:(RKObjectLoader *)objectLoader didSerializeSourceObject:(id)sourceObject toSerialization:(inout id<RKRequestSerializable> *)serialization {}

/**
 Sent when an object loader encounters a response status code or MIME Type that RestKit does not know how to handle.
 
 Response codes in the 2xx, 4xx, and 5xx range are all handled as you would expect. 2xx (successful) response codes
 are considered a successful content load and object mapping will be attempted. 4xx and 5xx are interpretted as
 errors and RestKit will attempt to object map an error out of the payload (provided the MIME Type is mappable)
 and will invoke objectLoader:didFailWithError: after constructing an NSError. Any other status code is considered
 unexpected and will cause objectLoaderDidLoadUnexpectedResponse: to be invoked provided that you have provided 
 an implementation in your delegate class.
 
 RestKit will also invoke objectLoaderDidLoadUnexpectedResponse: in the event that content is loaded, but there
 is not a parser registered to handle the MIME Type of the payload. This often happens when the remote backend
 system RestKit is talking to generates an HTML error page on failure. If your remote system returns content
 in a MIME Type other than application/json or application/xml, you must register the MIME Type and an appropriate
 parser with the [RKParserRegistry sharedParser] instance.
 
 Also note that in the event RestKit encounters an unexpected status code or MIME Type response an error will be 
 constructed and sent to the delegate via objectLoader:didFailsWithError: unless your delegate provides an 
 implementation of objectLoaderDidLoadUnexpectedResponse:. It is recommended that you provide an implementation
 and attempt to handle common unexpected MIME types (particularly text/html and text/plain).
 
 @optional
 */
- (void)objectLoaderDidLoadUnexpectedResponse:(RKObjectLoader *)objectLoader {}

/**
 Invoked just after parsing has completed, but before object mapping begins. This can be helpful
 to extract data from the parsed payload that is not object mapped, but is interesting for one
 reason or another. The mappableData will be made mutable via mutableCopy before the delegate
 method is invoked.
 
 Note that the mappable data is a pointer to a pointer to allow you to replace the mappable data
 with a new object to be mapped. You must dereference it to access the value.
 */
- (void)objectLoader:(RKObjectLoader *)loader willMapData:(inout id *)mappableData {}


@end

//
//  FollowingService.h
//  Cmug
//
//  Created by Dmitry C on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseService.h"
#import "FollowingResponse.h"
#import "MatchRequest.h"
#import "MatchResponse.h"
#import "ProfileShort.h"

@interface FollowingService : BaseService

- (void) loadFollowedUsersWithLimit: (NSInteger) limit 
                 andOffset: (NSInteger) offset
                 onSuccess: (ServiceSuccessResponseBlock) successBlock
                 onFailure: (ServiceErrorResponseBlock) failureBlock;

- (void) followUserWithId: (NSString*) userId
                onSuccess: (ServiceSuccessResponseBlock) successBlock
                onFailure: (ServiceErrorResponseBlock) failureBlock;

- (void) unfollowUserWithId: (NSString*) userId
                onSuccess: (ServiceSuccessResponseBlock) successBlock
                onFailure: (ServiceErrorResponseBlock) failureBlock;

- (void) matchProfiles: (NSArray*) items
            fromSource: (ProfileMatchSource) source
             onSuccess: (ServiceSuccessResponseBlock) successBlock
             onFailure: (ServiceErrorResponseBlock) failureBlock;

- (void) inviteAccount: (id) accountInfo
             onSuccess: (ServiceSuccessResponseBlock) successBlock
             onFailure: (ServiceErrorResponseBlock) failureBlock;

@end

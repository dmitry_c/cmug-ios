//
//  CustomTexturedNavigationBar.h
//  Cmug
//
//  Created by Dmitry C on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kBackgroundImageName @"bg_textured_light_blue.png"
#define kNavBarBackgroundImageName @"bg_navbar_textured_blue.png"

@interface CustomTexturedNavigationBar : UINavigationBar

- (void)setBackgroundImage:(UIImage *)backgroundImage forBarMetrics:(UIBarMetrics)barMetrics;

@end

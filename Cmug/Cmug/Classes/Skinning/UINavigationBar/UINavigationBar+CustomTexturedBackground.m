//
//  UINavigationBar+CustomTexturedBackground.m
//  Cmug
//
//  Created by Dmitry C on 4/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UINavigationBar+CustomTexturedBackground.h"
#import <QuartzCore/QuartzCore.h>

@interface UIView (CustomTexturedBackground)
//- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx;
@end

@implementation UINavigationBar (CustomTexturedBackground)

//- (void)drawRect:(CGRect)rect {
//    UIImage *image = [UIImage imageNamed: @"texture.png"];
//    [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    if ([self isMemberOfClass:[UINavigationBar class]]){
        UIImage *patternImage = [UIImage imageNamed:@"bg_navbar_textured_blue.png"];

//        CALayer* backgroundLayer = [CALayer layer];
//        [backgroundLayer setFrame:layer.frame];
//
//        [backgroundLayer setBackgroundColor:[UIColor colorWithPatternImage:patternImage].CGColor];
//        [layer insertSublayer:backgroundLayer atIndex:0];
//        
//        [self setTintColor:[UIColor colorWithHue:0 saturation:0 brightness:0.5f alpha:0.1f]];

        CGContextScaleCTM(ctx, 1.0f, -1.0f);
        CGContextDrawImage(ctx, CGRectMake(0, -patternImage.size.height, self.frame.size.width, self.frame.size.height), patternImage.CGImage);

        [self setTintColor:[UIColor colorWithRed:25.0/255.0 green:44.0/255.0 blue:190.0/255.0 alpha:0.5f]];
    } else {
        [super drawLayer:layer inContext:ctx];
    }
}

@end
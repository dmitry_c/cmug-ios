//
//  ButtonsCache.m
//  Cmug
//
//  Created by Dmitry C on 6/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ButtonsCache.h"

static ButtonsCache* instance;

@implementation ButtonsCache

+ (id)sharedCache
{
    @synchronized(self) {
        if (!instance) {
            instance = [[ButtonsCache alloc] init];
        }
        return instance;
    }
}

@end

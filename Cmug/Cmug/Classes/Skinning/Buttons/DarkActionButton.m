//
//  RoundedCornerBlackBackgroundButton.m
//  Cmug
//
//  Created by Dmitry C on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DarkActionButton.h"
#import <QuartzCore/QuartzCore.h>

#define CornerRadius 5.0f

@implementation DarkActionButton

- (void) setup
{
    self.layer.cornerRadius = CornerRadius;
    self.layer.borderColor = [[UIColor colorWithWhite:0.3 alpha:0.7] CGColor];
    self.layer.borderWidth = 1.5f;
    
    self.layer.masksToBounds = NO;
    self.layer.shadowRadius = CornerRadius;
    self.layer.shadowOpacity = 0.3;
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(1.5, 1.5);
    self.layer.shadowPath = [[UIBezierPath bezierPathWithRect:self.bounds] CGPath];
    
    self.layer.masksToBounds = NO;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[UIColor colorWithWhite:0.22 alpha:1.0] CGColor],
                       (id)[[UIColor blackColor] CGColor],
                       nil];
    gradient.cornerRadius = CornerRadius;
    [self.layer insertSublayer:gradient atIndex:0];

}

- (id)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame]) {
        self.titleLabel.frame = CGRectMake(2.0f, 2.0f, self.bounds.size.width - 4.0f, self.bounds.size.height - 4.0f);
        self.titleLabel.textColor = [UIColor whiteColor];
        
        self.titleLabel.font = [UIFont systemFontOfSize:12];;
        self.titleLabel.shadowColor = [UIColor colorWithWhite:0.22 alpha:1.0];
        self.titleLabel.shadowOffset = CGSizeMake(0.2f, 0.5f);

        [self setup];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self setup];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

//
//  ButtonsCache.h
//  Cmug
//
//  Created by Dmitry C on 6/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ButtonsCache : NSCache
+ (id)sharedCache;
@end

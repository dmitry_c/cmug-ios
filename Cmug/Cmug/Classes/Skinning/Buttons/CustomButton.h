//
//  CustomButton.h
//  Cmug
//
//  Created by Dmitry C on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ButtonsCache.h"

@interface CustomButton : UIButton
- (UIImage *)_createStandardImage;
- (UIImage *)_createStandardImageWithGradientColors:(NSArray*)gradientColors 
                                   andGradientStops:(NSArray*)gradientStops 
                                        borderColor:(UIColor*)borderColor 
                                        borderWidth:(CGFloat)borderWidth 
                                       cornerRadius:(CGFloat)cornerRadius
                                        shadowColor:(UIColor*)shadowColor 
                                       shadowOffset:(CGSize)shadowOffset 
                                         shadowRect:(CGRect)shadowFrame
                                   shadowBlurRadius:(CGFloat)shadowBlurRadius
                                             inRect:(CGRect)rect;
@end

//
//  LightActionButton.m
//  Cmug
//
//  Created by Dmitry C on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LightActionButton.h"
#import "CommonDrawing.h"

@implementation LightActionButton


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self != nil) {
        self.titleLabel.frame = CGRectMake(2.0f, 2.0f, self.bounds.size.width - 4.0f, self.bounds.size.height - 4.0f);
        [self setTitleColor: [UIColor colorWithRed:31.0f/255.0f green:150.0f/255.0f blue:197.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        [self setTitleColor: [UIColor colorWithRed:31.0f/255.0f green:150.0f/255.0f blue:197.0f/255.0f alpha:1.0f] forState:UIControlStateSelected];
        self.titleLabel.textColor = [UIColor colorWithRed:31.0f/255.0f green:150.0f/255.0f blue:197.0f/255.0f alpha:1.0f];
        self.titleLabel.font = [UIFont systemFontOfSize:12];;
        self.titleLabel.shadowColor = [UIColor colorWithWhite:0.22 alpha:1.0];
        self.titleLabel.shadowOffset = CGSizeMake(0.2f, 0.5f);
    }
    return self;
}

- (UIImage *)_createStandardImage
{
    NSArray *gradientColors = [NSArray arrayWithObjects:
                               (id)[[UIColor colorWithRed:232.0f/255.0f green:232.0f/255.0f blue:232.0f/255.0f alpha:1.0] CGColor],
                               (id)[[UIColor colorWithRed:231.0f/255.0f green:231.0f/255.0f blue:231.0f/255.0f alpha:1.0] CGColor],
                               (id)[[UIColor colorWithRed:205.0f/255.0f green:205.0f/255.0f blue:205.0f/255.0f alpha:1.0] CGColor],
                               nil];
    NSArray *gradientStops =  [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0f],
                               [NSNumber numberWithFloat:0.32f],
                               [NSNumber numberWithFloat:1.0f],
                               nil];
    
//    CGRect shadowRect = CGRectNull;
    CGRect shadowRect = CGRectMake(self.frame.origin.x, 
                                   self.frame.origin.y + 0.5, 
                                   self.frame.size.width, 
                                   self.frame.size.height - 0.5);
    CGSize shadowOffset = CGSizeMake(0.0, 1.5);
    CGFloat shadowBlurRadius = 2.0f;
    
	return [self _createStandardImageWithGradientColors:gradientColors 
                                       andGradientStops:gradientStops 
                                            borderColor:[UIColor clearColor]
                                            borderWidth:0.0 
                                           cornerRadius:4.0f
                                            shadowColor:[UIColor colorWithRed:155.0f/255.0f green:155.0f/255.0f blue:155.0f/255.0f alpha:1.0f] 
                                           shadowOffset:shadowOffset 
                                             shadowRect:shadowRect
                                       shadowBlurRadius:shadowBlurRadius 
                                                 inRect:self.frame];
}
@end

//
//  CustomButton.m
//  Cmug
//
//  Created by Dmitry C on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomButton.h"
#import "CommonDrawing.h"

@interface CustomButton()
{
    CGRect originalFrame;
    CGRect realFrame;
}

@end


@implementation CustomButton

- (id)initWithFrame:(CGRect)frame
{
    originalFrame = frame;
    realFrame = frame;

    self = [super initWithFrame:frame];
    
    if (self != nil)
	{
		[self setTitle:@"Custom Button" forState:UIControlStateNormal];
        
        self.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:13];
        
        NSString *cacheKey = [NSString stringWithFormat:@"%@-%@", NSStringFromClass(self.class), NSStringFromCGRect(self.bounds)];
        
        UIImage *bgImage = [[ButtonsCache sharedCache] objectForKey:cacheKey];
        if (bgImage == nil) {
            bgImage = [self _createStandardImage];
        }   
        
        [[ButtonsCache sharedCache] setObject:bgImage forKey:cacheKey];
		[self setBackgroundImage:bgImage forState:UIControlStateNormal];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!CGRectEqualToRect(originalFrame, realFrame)) {

        self.frame = realFrame;    
        
//        self.titleLabel.frame = CGRectMake((originalFrame.origin.x - realFrame.origin.x + realFrame.size.width - self.titleLabel.frame.size.width) * 0.5,
//                                           originalFrame.origin.y - realFrame.origin.y,
//                                           originalFrame.size.width,
//                                           originalFrame.size.height);
    }
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	[self setNeedsDisplay];
	return YES;
}

- (UIImage *)_createStandardImageWithGradientColors:(NSArray*)gradientColors 
                                   andGradientStops:(NSArray*)gradientStops 
                                        borderColor:(UIColor*)borderColor 
                                        borderWidth:(CGFloat)borderWidth 
                                       cornerRadius:(CGFloat)cornerRadius
                                        shadowColor:(UIColor*)shadowColor 
                                       shadowOffset:(CGSize)shadowOffset 
                                         shadowRect:(CGRect)shadowFrame
                                   shadowBlurRadius:(CGFloat)shadowBlurRadius
                                             inRect:(CGRect)rect
{
    CGFloat shadowOffsetLeft = shadowBlurRadius - shadowOffset.width;
    CGFloat shadowOffsetTop = shadowBlurRadius - shadowOffset.height;
    
    CGRect aShadowFrame;

    if (CGRectIsNull(shadowFrame)) {
        aShadowFrame = originalFrame;
    } else {
        aShadowFrame = shadowFrame;
    }
    
    realFrame.origin.x = aShadowFrame.origin.x - shadowOffsetLeft;
    realFrame.origin.y = aShadowFrame.origin.y - shadowOffsetTop;
    realFrame.size.width = aShadowFrame.size.width + 2 * shadowBlurRadius;
    realFrame.size.height = aShadowFrame.size.height + 2 * shadowBlurRadius;

    CGSize contextSize = CGSizeMake(realFrame.size.width - realFrame.origin.x, 
                                    realFrame.size.height - realFrame.origin.y);
    // General Declarations
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
	// Retina awareness.
	if ([[UIScreen mainScreen] scale] > 0.0)
	{
		UIGraphicsBeginImageContextWithOptions(contextSize, NO, [[UIScreen mainScreen] scale]);
	}
	else
	{
		UIGraphicsBeginImageContext(contextSize);
	}

	CGContextRef context = UIGraphicsGetCurrentContext();
    NSLog(@"Current context: %@", NSStringFromCGRect(CGContextGetClipBoundingBox(context)));
    
    // Stroke color
	UIColor* strokeColor = [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 0.4];
    if (borderColor) {
        strokeColor = borderColor;
    }
    
    
    // Setup gradient stop points
    int locCount = 2;
    if (gradientStops) {
        locCount = [gradientStops count];
    }
    CGFloat gradientLocations[locCount];
    
    if (gradientStops != nil) {
        for (int i = 0; i < locCount; i++)
        {
            NSNumber *location = [gradientStops objectAtIndex:i];
            gradientLocations[i] = [location floatValue];
        }
    } else {
        gradientLocations[0] = 0.0f;
        gradientLocations[1] = 1.0f;
    }

	CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);
    

	// Shadow Declarations
    CGColorRef arrowShadow;
    CGSize arrowShadowOffset = shadowOffset;
	CGFloat arrowShadowBlurRadius = shadowBlurRadius;

    // Shadow color
    if (shadowColor) {
        arrowShadow = shadowColor.CGColor;
    } else {
        CGFloat colorHSBA[4];
        RGBtoHSVA(strokeColor, &colorHSBA[0], &colorHSBA[1], &colorHSBA[2], &colorHSBA[3]);
        UIColor* darkColor = [UIColor colorWithHue: colorHSBA[0] saturation: colorHSBA[1] brightness: 0.6 alpha: colorHSBA[3]];
        arrowShadow = darkColor.CGColor;
    }
    
    CGRect relativeToOriginFrame = CGRectMake(originalFrame.origin.x - realFrame.origin.x, 
                                              originalFrame.origin.y - realFrame.origin.y, 
                                              originalFrame.size.width, 
                                              originalFrame.size.height);
    CGRect shadowRelativeToOriginFrame = CGRectMake(aShadowFrame.origin.x - realFrame.origin.x, 
                                                    aShadowFrame.origin.y - realFrame.origin.y, 
                                                    originalFrame.size.width, 
                                                    originalFrame.size.height);
    
    NSLog(@"Frames: relative: %@ \noriginal: %@ \nreal:%@", NSStringFromCGRect(relativeToOriginFrame), NSStringFromCGRect(originalFrame), NSStringFromCGRect(realFrame));
    
	// Rounded Rectangle Drawing
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRoundedRect:shadowRelativeToOriginFrame
                                                          cornerRadius:cornerRadius];
	UIBezierPath *roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect:relativeToOriginFrame
                                                                    cornerRadius:cornerRadius];

    NSLog(@"shadowPath.bounds %@", NSStringFromCGRect(shadowPath.bounds));
    NSLog(@"roundedCornerPath.bounds %@", NSStringFromCGRect(roundedRectanglePath.bounds));
    
    // Shadow Drawing
	CGContextSaveGState(context);
	CGContextSetShadowWithColor(context, arrowShadowOffset, arrowShadowBlurRadius, arrowShadow);
	CGContextSetFillColorWithColor(context, arrowShadow);
	CGContextAddPath(context, shadowPath.CGPath);
	CGContextFillPath(context);
//	CGContextRestoreGState(context);
//    
//
//	CGContextSaveGState(context);
    CGContextAddPath(context, roundedRectanglePath.CGPath);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, 
                                CGPointMake(CGRectGetMinX(shadowRelativeToOriginFrame), CGRectGetMinY(shadowRelativeToOriginFrame)), 
                                CGPointMake(CGRectGetMinX(shadowRelativeToOriginFrame), CGRectGetMaxY(shadowRelativeToOriginFrame)), 0);

	CGContextRestoreGState(context);

    
	[strokeColor setStroke];
	roundedRectanglePath.lineWidth = borderWidth; //0.5f;
	[roundedRectanglePath stroke];
  
	// Cleanup
	CGGradientRelease(gradient);
	CGColorSpaceRelease(colorSpace);
    
	return UIGraphicsGetImageFromCurrentImageContext();
}

- (UIImage *)_createStandardImage
{
    NSArray *gradientColors = [NSArray arrayWithObjects:
                       (id)[[UIColor colorWithRed:232.0f/255.0f green:232.0f/255.0f blue:232.0f/255.0f alpha:1.0] CGColor],
                       (id)[[UIColor colorWithRed:231.0f/255.0f green:231.0f/255.0f blue:231.0f/255.0f alpha:1.0] CGColor],
                       (id)[[UIColor colorWithRed:205.0f/255.0f green:205.0f/255.0f blue:205.0f/255.0f alpha:1.0] CGColor],
                       nil];
    NSArray *gradientStops =  [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0f],
                               [NSNumber numberWithFloat:0.32f],
                               [NSNumber numberWithFloat:1.0f],
                               nil];

    CGRect shadowRect = CGRectNull;
//    CGRect shadowRect = CGRectMake(self.frame.origin.x - 5.0, 
//                                   self.frame.origin.y - 5.0, 
//                                   self.frame.size.width + 2.0, 
//                                   self.frame.size.height - 0.5);
    CGSize shadowOffset = CGSizeMake(0, 2.5);
    CGFloat shadowBlurRadius = 6.0f;
    UIImage *image = [self _createStandardImageWithGradientColors:gradientColors 
                                                andGradientStops:gradientStops 
                                                     borderColor:[UIColor clearColor]
                                                     borderWidth:0.0 
                                                    cornerRadius:4.0f
                                                     shadowColor:[UIColor colorWithRed:155.0f/255.0f green:155.0f/255.0f blue:155.0f/255.0f alpha:1.0f] 
                                                    shadowOffset:shadowOffset 
                                                      shadowRect:shadowRect
                                                shadowBlurRadius:shadowBlurRadius 
                                                          inRect:self.frame];
    return image;
}

@end

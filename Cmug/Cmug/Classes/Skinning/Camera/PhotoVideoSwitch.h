//
//  PhotoVideoSwitch.h
//  Cmug
//
//  Created by Dmitry C on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaObject.h"

@interface PhotoVideoSwitch : UIView
@property (nonatomic, assign) MediaType selectedMediaType;
@property (nonatomic, copy) void(^onSwitchAction)(MediaType selectedMediaType);
@end

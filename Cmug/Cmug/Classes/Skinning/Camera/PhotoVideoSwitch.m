//
//  PhotoVideoSwitch.m
//  Cmug
//
//  Created by Dmitry C on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoVideoSwitch.h"
#import "UIImage+CappedImage.h"

#define kCameraSwitchThumbImage @"camera_switch_thumb"
#define kCameraSwitchBackground @"camera_switch_bg"
#define kCameraSwitchPhoto @"camera_switch_photo"
#define kCameraSwitchVideo @"camera_switch_video"

@interface PhotoVideoSwitch()
@property (nonatomic, retain) UISlider *slider;
@property (nonatomic, retain) UIButton *photoButton;
@property (nonatomic, retain) UIButton *videoButton;
@end

@implementation PhotoVideoSwitch
@synthesize selectedMediaType = _selectedMediaType;
@synthesize slider = _slider;
@synthesize photoButton = _photoButton;
@synthesize videoButton = _videoButton;
@synthesize onSwitchAction = _onSwitchAction;

//@interface SnapSlider : UISlider
//@end
//
//@implementation SnapSlider
//
//- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
//{
//    self.value = round(self.value);
//
//}
//
//@end

- (UISlider *)slider
{
    if (!_slider) {
        CGFloat height = 30.0f;
        CGFloat top = self.frame.size.height - height;
        CGFloat width = self.frame.size.width;

        CGRect frame = CGRectMake(-2.0f, top, width + 4.0f, height);
        _slider = [[UISlider alloc] initWithFrame:frame];
        _slider.maximumValue = 1.0f;
        _slider.minimumValue = 0.0f;
        UIImage *thumbImage = [UIImage imageNamed:kCameraSwitchThumbImage];
        [_slider setThumbImage:thumbImage forState:UIControlStateNormal];
        UIImage *bgImage = [UIImage imageNamed:kCameraSwitchBackground];
        UIImage *trackBgImage = [bgImage cappedImageWithCapInsets:UIEdgeInsetsMake(8.0f, 0, 0, 8.0f)];
        [_slider setMinimumTrackImage:trackBgImage forState:UIControlStateNormal];
        [_slider setMaximumTrackImage:trackBgImage forState:UIControlStateNormal];
        [_slider addTarget:self action:@selector(sliderTrackingAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _slider;
}

- (UIButton *)photoButton
{
    if (!_photoButton) {
        UIImage *buttonImage = [UIImage imageNamed:kCameraSwitchPhoto];
        _photoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height)];
        _photoButton.tag = kMediaTypePhoto;
        [_photoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        [_photoButton addTarget:self action:@selector(toggleAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _photoButton;
}

- (UIButton *)videoButton
{
    if (!_videoButton) {
        UIImage *buttonImage = [UIImage imageNamed:kCameraSwitchVideo];
        _videoButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - buttonImage.size.width, 0, buttonImage.size.width, buttonImage.size.height)];
        _videoButton.tag = kMediaTypeVideo;
        [_videoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        [_videoButton addTarget:self action:@selector(toggleAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _videoButton;
}

- (void)doSwitch:(MediaType)mediaType
{
    self.slider.value = (CGFloat)mediaType;
}

- (IBAction)toggleAction:(UIButton *)sender
{
    [self doSwitch:(MediaType)sender.tag];
    if (self.onSwitchAction) {
        self.onSwitchAction(self.selectedMediaType);
    }
}

- (IBAction)sliderTrackingAction:(UISlider *)sender
{
    sender.value = round(sender.value);
    self.selectedMediaType = [[NSNumber numberWithFloat:sender.value] intValue];
}

- (void)setSelectedMediaType:(MediaType)selectedMediaType
{
    // 0 - photo
    // 1 - video
    if (_selectedMediaType != selectedMediaType) {
        _selectedMediaType = selectedMediaType;
        [self doSwitch:selectedMediaType];
    }
}

- (void)setup
{
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:self.slider];
    [self addSubview:self.photoButton];
    [self addSubview:self.videoButton];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];    
}

@end

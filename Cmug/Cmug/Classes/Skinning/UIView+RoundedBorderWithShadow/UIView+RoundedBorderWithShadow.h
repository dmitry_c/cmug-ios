//
//  UIView+RoundedBorderWithShadow.h
//  Cmug
//
//  Created by Dmitry C on 6/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIView (RoundedBorderWithShadow)
- (void)decorateWithRoundedBordersAndShadow;
- (void)decorateWithVignette;
@end

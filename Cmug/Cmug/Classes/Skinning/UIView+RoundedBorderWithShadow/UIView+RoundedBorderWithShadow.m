//
//  UIView+RoundedBorderWithShadow.m
//  Cmug
//
//  Created by Dmitry C on 6/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIView+RoundedBorderWithShadow.h"

@implementation UIView (RoundedBorderWithShadow)

- (void)decorateWithRoundedBordersAndShadow
{
    self.layer.cornerRadius = 4.0f;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 1.5f;

    self.layer.masksToBounds = NO;
    self.layer.shadowRadius = 5;
    self.layer.shadowOpacity = 0.3;
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(1.5, 1.5);
    self.layer.shadowPath = [[UIBezierPath bezierPathWithRect:self.bounds] CGPath];
    
//    [self.layer setShadowPath:[UIBezierPath bezierPathWithRoundedRect: self.frame cornerRadius:4.0f].CGPath];
}

- (void)decorateWithVignette
{
    self.layer.cornerRadius = 4.0f;
	//self.layer.masksToBounds = YES;
	self.layer.borderColor = [UIColor whiteColor].CGColor;
	self.layer.borderWidth = 4.0f;
    
	self.layer.shadowRadius = 5.0f;
	self.layer.shadowOffset = CGSizeMake(0, 0);
	self.layer.shadowOpacity = 0.7;
	self.layer.shadowColor = [UIColor grayColor].CGColor;
	UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.bounds];
	self.layer.shadowPath = path.CGPath;
}
@end

//
//  OfferTypesSlider.h
//  Cmug
//
//  Created by Dmitry C on 7/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Offer.h"

@interface OfferTypesSlider : UISlider
@property (nonatomic, assign) OfferRequestType selectedOfferType;
//@property (nonatomic, assign) CGFloat position;
@end

//
//  OfferTypesSlider.m
//  Cmug
//
//  Created by Dmitry C on 7/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OfferTypesSlider.h"
#import "UIImage+ImageDraw.h"

#define kThumbImage @"offer_slider_thumb"
#define kSliderBackground @"slider_bg"
#define kSliderImagesOff @"offer_slider_images_off"
#define kSliderImagesOn @"offer_slider_images_on"


@interface OfferTypesSlider()
@property (nonatomic, retain) NSDictionary* thumbImages;
@property (nonatomic, readonly) UIImage *noImage;
@end

@implementation OfferTypesSlider
@synthesize selectedOfferType = _selectedOfferType;
@synthesize noImage = _noImage;

- (UIImage *)noImage
{
    if (!_noImage) {
        _noImage = [UIImage new];
    }
    return _noImage;
}

@synthesize thumbImages = _thumbImages;

- (void)setSelectedOfferType:(OfferRequestType)selectedOfferType
{
    if (_selectedOfferType != selectedOfferType) {
        _selectedOfferType = selectedOfferType;
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

- (NSDictionary*)makeThumbImagesFromSprite:(UIImage*)sprite
{
    UIImage *thumbBackground = [UIImage imageNamed:kThumbImage];
    CGFloat width_third = 0.33333f * sprite.size.width;

    CGRect rect = CGRectMake(0, 0, thumbBackground.size.width, thumbBackground.size.height);
    CGRect textRect = CGRectMake(0, 0, width_third, sprite.size.height);
    CGRect photoRect = CGRectMake(width_third, 0, width_third, sprite.size.height);
    CGRect videoRect = CGRectMake(2.0f * width_third, 0, width_third, sprite.size.height);
    UIImage *textSelectedImage = [thumbBackground drawImage:sprite inRect:rect fromRect:textRect];
    UIImage *photoSelectedImage = [thumbBackground drawImage:sprite inRect:rect fromRect:photoRect];
    UIImage *videoSelectedImage = [thumbBackground drawImage:sprite inRect:rect fromRect:videoRect];
    
    return [NSDictionary dictionaryWithObjectsAndKeys:
            textSelectedImage, [NSNumber numberWithInt:kOfferRequestType_Text], 
            photoSelectedImage, [NSNumber numberWithInt:kOfferRequestType_Photo],
            videoSelectedImage, [NSNumber numberWithInt:kOfferRequestType_Video],
            nil];

}

- (NSDictionary*)thumbImages
{
    if (!_thumbImages) {
        _thumbImages = [self makeThumbImagesFromSprite:[UIImage imageNamed:kSliderImagesOn]];
    }
    return _thumbImages;
}


- (void)setup
{
    // Initialize Background
    self.selectedOfferType = kOfferRequestType_Text;
    UIImage* background = [UIImage imageNamed:kSliderBackground];

    CGRect frame = CGRectIntegral(self.frame);
    // Slider lies inside the background rect.
    UIEdgeInsets sliderInset = UIEdgeInsetsMake(2.0f, 5.0f, 1.0f, 2.0f);
    self.frame = CGRectMake(self.frame.origin.x + sliderInset.left, 
                            self.frame.origin.y + sliderInset.top, 
                            self.frame.size.width - sliderInset.left - sliderInset.right, 
                            self.frame.size.height - sliderInset.top - sliderInset.bottom);
    
    UIImage *combinedBackground = [background drawImage:[UIImage imageNamed:kSliderImagesOff] inRect:CGRectNull fromRect:CGRectNull];
    
    self.continuous = YES;
    
    UIImageView* bg = [[UIImageView alloc] initWithImage: combinedBackground];
    [self addSubview:bg];
    bg.frame = CGRectMake(-sliderInset.left, -sliderInset.top, frame.size.width, frame.size.height);
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (UIImage *)maximumTrackImageForState:(UIControlState)state
{
    return self.noImage;
}

- (UIImage *)minimumTrackImageForState:(UIControlState)state
{
    return self.noImage;
}

- (float)minimumValue
{
    return 1.0f;
}

- (float)maximumValue
{
    return 3.0f;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    OfferRequestType state = round(self.value);
    self.value = state;
    self.selectedOfferType = state;
}

- (UIImage *)thumbImageForState:(UIControlState)state
{
    UIImage *thumbImage = [self.thumbImages objectForKey:[NSNumber numberWithInt:self.selectedOfferType]];
    if (thumbImage) {
        return thumbImage;
    }
    return [UIImage imageNamed:kThumbImage];
}

@end

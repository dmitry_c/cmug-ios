//
//  GenericResponse.h
//  Cmug
//
//  Created by Dmitry C on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Mapped.h"

@interface GenericResponse : NSObject
@property (nonatomic, strong) NSString* result;
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, assign) NSInteger limit;
@property (nonatomic, assign) NSInteger offset;
@end

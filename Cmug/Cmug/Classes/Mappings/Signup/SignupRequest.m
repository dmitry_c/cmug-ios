//
//  SignupRequest.m
//  Cmug
//
//  Created by Dmitry C on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SignupRequest.h"
#import "Restkit/JSONKit.h"

@implementation SignupRequest

@synthesize firstName = _firstName;
@synthesize lastName = _lastName;
@synthesize email = _email;
@synthesize password = _password;
@synthesize phone = _phone;
@synthesize picture = _picturePath;
@synthesize facebookData = _facebookData;
@synthesize twitterData = _twitterData;

- (NSData*) pictureData
{
    return [NSData dataWithContentsOfURL:[NSURL URLWithString:self.picture]];
}


+ (SignupRequest*) makeFromFacebookData: (NSDictionary*) data
{
    SignupRequest* signupRequest = [[SignupRequest alloc] init];

    if (signupRequest) {
        signupRequest.firstName = [data valueForKey:@"first_name"];
        signupRequest.lastName = [data valueForKey:@"last_name"];
        signupRequest.email = [data valueForKey:@"email"];
        signupRequest.password = @"";
        signupRequest.phone = [data valueForKey:@"phone"];
        signupRequest.picture = [data valueForKey:@"user_picture"];
        signupRequest.facebookData = [data JSONString];
    }
    return signupRequest;
}

+ (SignupRequest*) makeFromTwitterData: (NSDictionary*) data
{
    SignupRequest* signupRequest = [[SignupRequest alloc] init];
    
    if (signupRequest) {
        signupRequest.firstName = [data valueForKey:@"name"];
        signupRequest.lastName = @""; // [data valueForKey:@"screen_name"];
//        signupRequest.email = [data valueForKey:@"email"]; // not available from twitter
        signupRequest.password = @"";
        signupRequest.picture = [data valueForKey:@"profile_image_url"];
        signupRequest.twitterData = [data JSONString];
    }
    return signupRequest;
}

+ (RKObjectMapping*) getMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:[SignupRequest class]];
    
    [mapping mapAttributes:@"email", @"password", @"phone", nil];
    
    [mapping mapKeyPath:@"first_name" toAttribute:@"firstName"];
    [mapping mapKeyPath:@"last_name" toAttribute:@"lastName"];
    [mapping mapKeyPath:@"picture" toAttribute:@"picture"];
    [mapping mapKeyPath:@"facebook_data" toAttribute:@"facebookData"];
    [mapping mapKeyPath:@"twitter_data" toAttribute:@"twitterData"];
    
    return mapping;
}

@end

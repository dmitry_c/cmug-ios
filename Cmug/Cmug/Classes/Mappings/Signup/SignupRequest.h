//
//  SignupRequest.h
//  Cmug
//
//  Created by Dmitry C on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RKObjectMapper.h"

@interface SignupRequest : NSObject
@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *password;
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, retain) NSString *picture;

@property (nonatomic, retain) NSString *facebookData;
@property (nonatomic, retain) NSString *twitterData;

- (NSData*) pictureData;

+ (SignupRequest*) makeFromFacebookData: (NSDictionary*) data;
+ (SignupRequest*) makeFromTwitterData: (NSDictionary*) data;

+ (RKObjectMapping*) getMapping;
@end

//
//  NSObject+Mapped.m
//  Cmug
//
//  Created by Dmitry C on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSObject+Mapped.h"
#import "RestKit/RKObjectSerializer.h"

@implementation NSObject (Mapped)
+ (RKObjectMapping*) getMapping
{
    RKObjectMapping* mapping = [RKObjectMapping mappingForClass:self];
    
    return mapping; 
}

-(NSDictionary *)serializedObject
{
    RKObjectSerializer* serializer = [RKObjectSerializer serializerWithObject:self mapping:[[self.class getMapping] inverseMapping]];
    NSError* error = nil;
    
    // Turn an object into a dictionary
    NSMutableDictionary* dictionary = [serializer serializedObject:&error];
    return dictionary;
}
@end

//
//  MatchRequest.m
//  Cmug
//
//  Created by Dmitry C on 6/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MatchRequest.h"
#import "RestKit/JSONKit.h"
#import "RestKit/RKObjectSerializer.h"

@implementation MatchRequest
@synthesize items, source;
@synthesize serializedItems=_serializedItems;

- (NSString *)serializedItems
{
    if (!_serializedItems) {
        _serializedItems = [[self.items valueForKey:@"serializedObject"] JSONString];
    }
    return _serializedItems;
}

+ (RKObjectMapping *)getMapping
{
    RKObjectMapping *mapping = [super getMapping];
    [mapping mapAttributes:@"source", nil];
    [mapping mapKeyPath:@"items" toAttribute:@"serializedItems"];
//    [mapping mapKeyPath:@"items" toRelationship:@"items" withMapping:[ContactRecord getMapping] serialize:YES];
    return mapping;
}

+ (MatchRequest*) requestFromSource:(ProfileMatchSource)source andItems:(NSArray*)items
{
    MatchRequest* request = [MatchRequest new];
    request.source = source;
    request.items = items;
    if (source==kContacts) {
        request.serializedItems = [[request.items valueForKey:@"serializedObject"] JSONString];
    } else {
        request.serializedItems = [request.items JSONString];
    }

    return request;
}
@end

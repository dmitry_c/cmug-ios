//
//  MatchRequest.h
//  Cmug
//
//  Created by Dmitry C on 6/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Mapped.h"
#import "ProfileMatch.h"
#import "ContactRecord.h"

@interface MatchRequest : NSObject
@property (nonatomic, retain) NSArray* items;
@property (nonatomic, assign) ProfileMatchSource source;
@property (nonatomic, retain) NSString* serializedItems;

+ (MatchRequest*) requestFromSource:(ProfileMatchSource)source andItems:(NSArray*)items;
@end

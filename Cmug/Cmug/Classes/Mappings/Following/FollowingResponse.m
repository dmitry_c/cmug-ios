//
//  FollowingResponse.m
//  Cmug
//
//  Created by Dmitry C on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FollowingResponse.h"
#import "ProfileShort.h"

@implementation FollowingResponse
@synthesize usersList;

+ (RKObjectMapping *)getMapping
{
    RKObjectMapping *mapping = [super getMapping];
    // Short Profile Mapping
    RKObjectMapping* shortProfileMapping = [ProfileShort getMapping];
    [mapping mapKeyPath:@"users_list" toRelationship:@"usersList" withMapping:shortProfileMapping];
//    [mapping mapRelationship:@"usersList" withMapping:shortProfileMapping];
    return mapping;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<FollowingResponse: total: %d\n limit: %d\n offset: %d\n usersList: %@>", self.total, self.limit, self.offset, self.usersList];
}
@end

//
//  FollowingResponse.h
//  Cmug
//
//  Created by Dmitry C on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GenericResponse.h"

@interface FollowingResponse : GenericResponse
@property (nonatomic, retain) NSArray* usersList;
@end

//
//  MatchResponse.m
//  Cmug
//
//  Created by Dmitry C on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MatchResponse.h"

@implementation MatchResponse
@synthesize source, matches;

+ (RKObjectMapping*) addressBookMatchMapping
{
    RKObjectMapping *mapping = [super getMapping];
    [mapping mapAttributes:@"source", nil];
    [mapping mapKeyPath:@"matches" toRelationship:@"matches" withMapping:    [AddressBookProfileMatch getMapping]];
    return mapping;
}

+ (RKObjectMapping*) facebookMatchMapping
{
    RKObjectMapping *mapping = [super getMapping];
    [mapping mapAttributes:@"source", nil];
    
    [mapping mapKeyPath:@"matches" toRelationship:@"matches" withMapping:    [FacebookProfileMatch getMapping]];
    return mapping;
}

+ (RKObjectMapping*) twitterMatchMapping
{
    RKObjectMapping *mapping = [super getMapping];
    [mapping mapAttributes:@"source", nil];
    
    [mapping mapKeyPath:@"matches" toRelationship:@"matches" withMapping:    [TwitterProfileMatch getMapping]];
    return mapping;
}

+ (RKObjectMapping *)getMapping
{
    [[NSException exceptionWithName:@"Mapping Usage Error" reason:@"" userInfo:nil] raise];
    return nil;
}

+ (RKDynamicObjectMapping *) getDynamicMapping
{
    RKDynamicObjectMapping *dynamicMapping = [[RKDynamicObjectMapping alloc] init];
    [dynamicMapping setObjectMapping:[self addressBookMatchMapping] whenValueOfKeyPath:@"source" isEqualTo:@"1"];
    [dynamicMapping setObjectMapping:[self facebookMatchMapping] whenValueOfKeyPath:@"source" isEqualTo:@"2"];
    [dynamicMapping setObjectMapping:[self twitterMatchMapping] whenValueOfKeyPath:@"source" isEqualTo:@"3"];
    return dynamicMapping;
}
@end

//
//  MatchResponse.h
//  Cmug
//
//  Created by Dmitry C on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProfileMatch.h"
#import "GenericResponse.h"

@interface MatchResponse : GenericResponse
@property (nonatomic, retain) NSArray* matches;
@property (nonatomic, assign) ProfileMatchSource source;
+ (RKObjectMapping*) addressBookMatchMapping;
+ (RKObjectMapping*) facebookMatchMapping;
+ (RKObjectMapping*) twitterMatchMapping;
+ (RKDynamicObjectMapping *) getDynamicMapping;
@end

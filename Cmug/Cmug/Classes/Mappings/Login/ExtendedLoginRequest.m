//
//  ExtendedLoginRequest.m
//  Cmug
//
//  Created by Dmitry C on 6/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ExtendedLoginRequest.h"

@implementation ExtendedLoginRequest

@synthesize forceAuthTokenRefresh, authToken;


+ (RKObjectMapping*) getMapping
{
    RKObjectMapping* loginRequestMapping = [RKObjectMapping mappingForClass:[ExtendedLoginRequest class]];
    [loginRequestMapping mapAttributes:@"email", @"password", nil];

    [loginRequestMapping mapKeyPath:@"force_auth_token_refresh" toAttribute:@"forceAuthTokenRefresh"];
    [loginRequestMapping mapKeyPath:@"auth_token" toAttribute:@"authToken"];
    
    return loginRequestMapping;
}
@end

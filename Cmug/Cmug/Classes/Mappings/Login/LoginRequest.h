//
//  LoginRequest.h
//  Cmug
//
//  Created by Dmitry C on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Mapped.h"

@interface LoginRequest : NSObject

@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *password;

@end

//
//  LoginResponse.m
//  Cmug
//
//  Created by Dmitry C on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginSignupResponse.h"

@implementation LoginSignupResponse
@synthesize name = _name;
@synthesize firstName = _firstName;
@synthesize lastName = _lastName;
@synthesize email = _email;
@synthesize userId = _userId;
@synthesize authToken = _authToken;
@synthesize authTokenExpires = _authTokenExpires;
@synthesize picturePath = _picturePath;

+ (RKObjectMapping*) getMapping 
{
//    RKObjectMapping* loginResponseMapping = [RKObjectMapping mappingForClass:[LoginSignupResponse class]];
    
    RKObjectMapping* loginResponseMapping = [super getMapping];
    
    [loginResponseMapping mapAttributes:@"name", @"email", nil];

    [loginResponseMapping mapKeyPath:@"id" toAttribute:@"userId"];
    [loginResponseMapping mapKeyPath:@"first_name" toAttribute:@"firstName"];
    [loginResponseMapping mapKeyPath:@"last_name" toAttribute:@"lastName"];
    [loginResponseMapping mapKeyPath:@"auth_token" toAttribute:@"authToken"];
    [loginResponseMapping mapKeyPath:@"auth_token_expires" toAttribute:@"authTokenExpires"];
    [loginResponseMapping mapKeyPath:@"picture" toAttribute:@"picturePath"];
    
    return loginResponseMapping;
}
@end

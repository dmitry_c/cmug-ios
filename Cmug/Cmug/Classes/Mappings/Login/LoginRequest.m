//
//  LoginRequest.m
//  Cmug
//
//  Created by Dmitry C on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginRequest.h"

@implementation LoginRequest

@synthesize email, password;


+ (RKObjectMapping*) getMapping
{
//    RKObjectMapping* loginRequestMapping = [RKObjectMapping mappingForClass:[LoginRequest class]];
    RKObjectMapping* loginRequestMapping = [super getMapping];
    [loginRequestMapping mapAttributes:@"email", @"password", nil];
    
    return loginRequestMapping;
}

@end

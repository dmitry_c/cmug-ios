//
//  LoginResponse.h
//  Cmug
//
//  Created by Dmitry C on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Mapped.h"

@interface LoginSignupResponse : NSObject

@property (nonatomic, retain) NSString* userId;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* firstName;
@property (nonatomic, retain) NSString* lastName;
@property (nonatomic, retain) NSString* email;
@property (nonatomic, retain) NSString* authToken;
@property (nonatomic, retain) NSDate* authTokenExpires;
@property (nonatomic, retain) NSString* picturePath;

@end

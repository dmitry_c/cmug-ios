//
//  ExtendedLoginRequest.h
//  Cmug
//
//  Created by Dmitry C on 6/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginRequest.h"

@interface ExtendedLoginRequest : LoginRequest

@property (nonatomic, retain) NSString *authToken;
@property (nonatomic, assign) BOOL forceAuthTokenRefresh;

@end

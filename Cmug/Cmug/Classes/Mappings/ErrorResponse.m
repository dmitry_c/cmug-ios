//
//  ErrorResponse.m
//  Cmug
//
//  Created by Dmitry C on 4/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ErrorResponse.h"
#import "RestKit/RKObjectMapper.h"
#import "RestKit/RKObjectManager.h"
#import "RestKit/RKDynamicObjectMapping.h"

@implementation ErrorResponse

@synthesize result = _result;
@synthesize status = _status;
@synthesize error = _error;
@synthesize errors = _errors;


static BOOL defaultMappingSet = NO;
+ (void) initialize
{
    if (!defaultMappingSet) {
        // Setup default mappings

        RKObjectMapping *defaultMapping = [RKObjectMapping mappingForClass:[ErrorResponse class]];
        [defaultMapping mapAttributes:@"result", @"status", @"error", @"errors", nil];
//
//        RKDynamicObjectMapping *dm = [RKDynamicObjectMapping dynamicMappingUsingBlock:^(RKDynamicObjectMapping *dynamicMapping) {
//            dynamicMapping.rootKeyPath = @"errors";
//            [dynamicMapping setObjectMapping:defaultMapping whenValueOfKeyPath:@"result" isEqualTo:@"error"];
//        }];
                                      
        [[RKObjectManager sharedManager].mappingProvider setErrorMapping:defaultMapping];

        defaultMappingSet = YES;
    }
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"%@: %@", self.result, self.errors];
}

- (BOOL) isSuccessful
{
    return (self.result == @"ok");
}

- (BOOL) isAuthorized
{
    return (self.status != 403);
}
@end

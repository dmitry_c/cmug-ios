//
//  NSObject+Mapped.h
//  Cmug
//
//  Created by Dmitry C on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RKObjectMapper.h"

@interface NSObject (Mapped)
+ (RKObjectMapping*) getMapping;
- (NSDictionary*) serializedObject;
@end

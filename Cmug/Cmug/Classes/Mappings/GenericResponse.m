//
//  GenericResponse.m
//  Cmug
//
//  Created by Dmitry C on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GenericResponse.h"

@implementation GenericResponse
@synthesize result, total, limit, offset;

+ (RKObjectMapping *)getMapping
{
    RKObjectMapping *mapping = [super getMapping];
    [mapping mapAttributes:@"result", @"total", @"limit", @"offset", nil];
    return mapping;
}
@end

//
//  ProfileShort.h
//  Cmug
//
//  Created by Dmitry C on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Mapped.h"

//
// ProfileShort used as embed profile info in other mappings.
//
@interface ProfileShort : NSObject

@property (nonatomic, retain) NSString* userId;
@property (nonatomic, retain) NSString* firstName;
@property (nonatomic, retain) NSString* lastName;
@property (nonatomic, retain) NSString* picture;

- (BOOL) isAnonymous;

@end

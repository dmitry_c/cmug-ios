//
//  ProfileShort.m
//  Cmug
//
//  Created by Dmitry C on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProfileShort.h"

@implementation ProfileShort
@synthesize userId, firstName, lastName, picture;

- (BOOL) isAnonymous
{
    return self.userId == @"-1";
}

+ (RKObjectMapping*) getMapping
{
    RKObjectMapping* mapping = [super getMapping];
    
    [mapping mapKeyPath:@"id" toAttribute:@"userId"];
    [mapping mapKeyPath:@"first_name" toAttribute:@"firstName"];
    [mapping mapKeyPath:@"last_name" toAttribute:@"lastName"];
    [mapping mapKeyPath:@"picture" toAttribute:@"picture"];
    
    return mapping; 
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<ProfileShort: [%@] %@ %@ - %@>", self.userId, self.firstName, self.lastName, self.picture];
}

- (NSUInteger)hash
{
    return [self.userId integerValue];
}

- (BOOL)isEqual:(id)object
{
    if ([object isMemberOfClass:ProfileShort.class]) {
        return (self.userId == ((ProfileShort*)object).userId);
    }
    return NO;
}

@end

//
//  ContactRecord.m
//  Cmug
//
//  Created by Dmitry C on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ContactRecord.h"


@implementation ContactRecord
@synthesize emails, phones, facebookId, twitterId, firstName, lastName, recordId;

+(RKObjectMapping *)getMapping
{
    RKObjectMapping *mapping = [super getMapping];
    [mapping mapAttributes:@"emails", @"phones", nil];
    [mapping mapKeyPath:@"facebook_id" toAttribute:@"facebookId"];
    [mapping mapKeyPath:@"twitter_id" toAttribute:@"twitterId"];
    [mapping mapKeyPath:@"first_name" toAttribute:@"firstName"];
    [mapping mapKeyPath:@"last_name" toAttribute:@"lastName"];
    [mapping mapKeyPath:@"record_id" toAttribute:@"recordId"];
    
    return mapping;
}

@end

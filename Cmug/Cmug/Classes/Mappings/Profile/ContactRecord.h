//
//  ContactRecord.h
//  Cmug
//
//  Created by Dmitry C on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Mapped.h"

@interface ContactRecord : NSObject

@property (nonatomic, retain) NSString* recordId;
@property (nonatomic, retain) NSString* firstName;
@property (nonatomic, retain) NSString* lastName;
@property (nonatomic, retain) NSArray* emails;
@property (nonatomic, retain) NSArray* phones;
@property (nonatomic, retain) NSString* twitterId;
@property (nonatomic, retain) NSString* facebookId;

@end

//
//  ProfileMatch.m
//  Cmug
//
//  Created by Dmitry C on 6/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProfileMatch.h"

@implementation AddressBookProfileMatch
@synthesize emails, phones, facebookId, twitterId, recordId, firstName, lastName, profile;

+ (RKObjectMapping *)getMapping
{
    RKObjectMapping *mapping = [super getMapping];
    //{matches: { <record_id1>: { "profile": {}, "email": ..., "phone": ... }, ...}, 
    //source: 1}
    mapping.forceCollectionMapping = YES;
    [mapping mapKeyOfNestedDictionaryToAttribute:@"recordId"];

    [mapping mapKeyPath:@"(recordId).emails" toAttribute:@"emails"];
    [mapping mapKeyPath:@"(recordId).phones" toAttribute:@"phones"];
    [mapping mapKeyPath:@"(recordId).first_name" toAttribute:@"firstName"];
    [mapping mapKeyPath:@"(recordId).last_name" toAttribute:@"lastName"];
    [mapping mapKeyPath:@"(recordId).facebook_id" toAttribute:@"facebookId"];
    [mapping mapKeyPath:@"(recordId).twitter_id" toAttribute:@"twitterId"];
    mapping.setNilForMissingRelationships = YES;
    [mapping mapKeyPath:@"(recordId).profile" toRelationship:@"profile" withMapping:[ProfileShort getMapping]]; 
    return mapping;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"<%@: rid:%@ name:%@ %@> matches: %@", NSStringFromClass(self.class), self.recordId, self.firstName, self.lastName, self.profile];
}

@end


@implementation FacebookProfileMatch

@synthesize facebookId, name, profile;

+ (RKObjectMapping *)getMapping
{
    RKObjectMapping *mapping = [super getMapping];
    mapping.forceCollectionMapping = YES;
    //{matches: { <record_id1>: { "profile": {}, "email": ..., "phone": ... }, ...}, 
    //source: 2}
    [mapping mapKeyOfNestedDictionaryToAttribute:@"facebookId"];
    [mapping mapKeyPath:@"(facebookId).name" toAttribute:@"name"];
//    [mapping mapKeyPath:@"(record_id).facebook_id" toAttribute:@"facebookId"];
    mapping.setNilForMissingRelationships = YES;
    [mapping mapKeyPath:@"(facebookId).profile" toRelationship:@"profile" withMapping:[ProfileShort getMapping]]; 
    return mapping;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"<%@: fb:%@ name:%@ > matches: %@", NSStringFromClass(self.class), self.facebookId, self.name, self.profile];
}
@end


@implementation TwitterProfileMatch

@synthesize twitterId, profile;
@synthesize twitterName, twitterScreenName, twitterImageUrl;

+ (RKObjectMapping *)getMapping
{
    RKObjectMapping *mapping = [super getMapping];
    //{matches: { <record_id1>: { "profile": {}, "email": ..., "phone": ... }, ...}, 
    //source: 3}
    mapping.forceCollectionMapping = YES;
    [mapping mapKeyOfNestedDictionaryToAttribute:@"twitterId"];
//    [mapping mapKeyPath:@"(record_id).twitter_id" toAttribute:@"twitterId"];
    mapping.setNilForMissingRelationships = YES;
    [mapping mapKeyPath:@"(twitterId).profile" toRelationship:@"profile" withMapping:[ProfileShort getMapping]]; 
    return mapping;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"<%@: tw:%@ > matches: %@", NSStringFromClass(self.class), self.twitterId, self.profile];
}
@end

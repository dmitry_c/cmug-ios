//
//  ProfileMatch.h
//  Cmug
//
//  Created by Dmitry C on 6/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProfileShort.h"

typedef enum ProfileMatchSource {
    kContacts = 1,
    kFacebookFriends = 2,
    kTwitterFollowers = 3
} ProfileMatchSource;

// Matching Results
@interface AddressBookProfileMatch : NSObject
@property (nonatomic, retain) NSString* recordId;
@property (nonatomic, retain) NSArray* emails;
@property (nonatomic, retain) NSArray* phones;
@property (nonatomic, retain) NSString* firstName;
@property (nonatomic, retain) NSString* lastName;
@property (nonatomic, retain) NSString* twitterId;
@property (nonatomic, retain) NSString* facebookId;

@property (nonatomic, retain) ProfileShort* profile;
@end


@interface FacebookProfileMatch : NSObject
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* facebookId;

@property (nonatomic, retain) ProfileShort* profile;
@end


@interface TwitterProfileMatch : NSObject
@property (nonatomic, retain) NSString* twitterId;
// Note: Next 3 properties are not mapped. They are populated by results of twitter user info call.
@property (nonatomic, retain) NSString* twitterName;
@property (nonatomic, retain) NSString* twitterScreenName;
@property (nonatomic, retain) NSString* twitterImageUrl;

@property (nonatomic, retain) ProfileShort* profile;
@end
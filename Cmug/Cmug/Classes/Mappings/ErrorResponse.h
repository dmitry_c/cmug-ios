//
//  ErrorResponse.h
//  Cmug
//
//  Created by Dmitry C on 4/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

//
// The instance of this objects will be set by RestKit into NSError.userInfo dictionary with key
// RKObjectMapperErrorObjectsKey.
//
@interface ErrorResponse : NSObject

@property (nonatomic, retain) NSString* result;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, retain) NSString* error;
@property (nonatomic, retain) NSDictionary* errors;

- (BOOL) isSuccessful;
- (BOOL) isAuthorized;
@end

//
//  CreateOfferRequest.m
//  Cmug
//
//  Created by Dmitry C on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CreateOfferRequest.h"

@implementation CreateOfferRequest
@synthesize title, description, visibility, usersList, sharing, primaryCost, attachment, isAnonymous, requestType;
@synthesize requestTypeDisplay;
@synthesize usersListCleaned;

- (NSString *)requestTypeDisplay
{
    switch (self.requestType) {
        case kOfferRequestType_Text:
            return @"Text";
        case kOfferRequestType_Photo:
            return @"Photo";
        case kOfferRequestType_Video:
            return @"Video";
        case kOfferRequestType_Audio:
            return @"Audio";
        default:
            return @"";
            break;
    }
    return @"";
}

- (NSArray *)usersListCleaned
{
    return [self.usersList valueForKey:@"userId"];
}

+ (RKObjectMapping*) getMapping
{
    RKObjectMapping* createOfferMapping = [super getMapping];
    [createOfferMapping mapAttributes:@"title", @"description", @"visibility", @"sharing", @"attachment", nil];
    
    [createOfferMapping mapKeyPath:@"users_list" toAttribute:@"usersListCleaned"];
    [createOfferMapping mapKeyPath:@"owner_is_anonymous" toAttribute:@"isAnonymous"];
    [createOfferMapping mapKeyPath:@"primary_cost" toAttribute:@"primaryCost"];
    
    return createOfferMapping;
}

@end

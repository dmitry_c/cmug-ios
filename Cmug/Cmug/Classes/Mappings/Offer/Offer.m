//
//  Offer.m
//  Cmug
//
//  Created by Dmitry C on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Offer.h"

@implementation Offer
@synthesize title, description, attachment, cost, isAnonymous, requestType, sharing, usersList, visibility;

+ (RKObjectMapping*) getMapping
{
    return [super getMapping];   
}

@end

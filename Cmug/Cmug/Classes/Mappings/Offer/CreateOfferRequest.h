//
//  CreateOfferRequest.h
//  Cmug
//
//  Created by Dmitry C on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Mapped.h"
#import "Offer.h"


@interface CreateOfferRequest : NSObject

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *description;

@property (nonatomic, assign) enum OfferRequestType requestType;
@property (nonatomic, readonly) NSString* requestTypeDisplay;

// Designates if the name and picture of requester will be shown in offers list.
@property (nonatomic, assign) BOOL isAnonymous;

// URL for attached image 
@property (nonatomic, retain) NSString* attachment;
 
//   visibility = public|all|users
// * 'public' means everyone has access to offer
// * 'all' only following users have access
// * 'users' access restricted to usersList
@property (nonatomic, assign) enum OfferVisibilityType visibility;

// When visibility=='users' usersList array contains user ids,
// otherwise it's empty.
@property (nonatomic, retain) NSArray *usersList;

// * 1 - shared with Facebook
// * 2 - shared with Twitter
// * 1|2 = 3 - shared with both
@property (nonatomic, assign) enum OfferShareType sharing;

// Money offer for the request.
// For version 1.0 always equals 0.0 - free.
@property (nonatomic, retain) NSNumber *primaryCost;

// usersList contains ProfileShort instances,
// though we should send only ids to the server.
// This property filters usersList for only ids and to be used for
// serialization mapping.
@property (nonatomic, readonly) NSArray *usersListCleaned;
@end

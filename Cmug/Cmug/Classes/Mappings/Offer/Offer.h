//
//  Offer.h
//  Cmug
//
//  Created by Dmitry C on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Mapped.h"


typedef enum OfferVisibilityType {
    kOfferVisibilityType_Public = 1,
    kOfferVisibilityType_All,
    kOfferVisibilityType_Users
} OfferVisibilityType;

typedef enum OfferShareType {
    kOfferShareType_None = 0,
    kOfferShareType_Facebook = 1 << 0,
    kOfferShareType_Twitter = 1 << 1,
    kOfferShareType_All = 3
} OfferShareType;

typedef enum OfferRequestType {
    kOfferRequestType_Text = 1,
    kOfferRequestType_Photo,
    kOfferRequestType_Video,
    kOfferRequestType_Audio
} OfferRequestType;


//
// Offer info as it appears in Offers list.
//
@interface Offer : NSObject

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *description;

@property (nonatomic, assign) OfferRequestType requestType;

// Designates if the name and picture of requester will be shown in offers list.
@property (nonatomic, assign) BOOL isAnonymous;

// URL for attached image 
@property (nonatomic, retain) NSString* attachment;

//   visibility = public|all|users
// * 'public' means everyone has access to offer
// * 'all' only following users have access
// * 'users' access restricted to usersList
@property (nonatomic, assign) OfferVisibilityType visibility;

// When visibility=='users' usersList array contains user ids,
// otherwise it's empty.
@property (nonatomic, retain) NSArray *usersList;

// * 1 - shared with Facebook
// * 2 - shared with Twitter
// * 1|2 = 3 - shared with both
@property (nonatomic, assign) OfferShareType sharing;

// Money offer for the request.
// For version 1.0 always equals 0.0 - free.
@property (nonatomic, retain) NSNumber *cost;

@end

//
//  MultiInputsViewController.m
//  Cmug
//
//  Created by Dmitry C on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MultiInputsViewController.h"


@implementation MultiInputsViewController

@synthesize keyboardAccesoryView=_keyboardAccesoryView;
@synthesize activeTextField=_activeTextField;

@synthesize tabIndex = _tabIndex;
@synthesize maxTabIndex;

- (void) setTabIndex:(int)tabIndex
{
    if (tabIndex > self.maxTabIndex-1) {
        _tabIndex = self.maxTabIndex - 1;
    } else
        if (tabIndex < -1) {
            _tabIndex = -1;
        } else
            _tabIndex = tabIndex;
}
- (int) maxTabIndex
{
    return self.maxTabIndexForTextInputs;
}

- (BOOL) isPreviousEnabled
{
    return (self.tabIndex > 0);
}

- (BOOL) isNextEnabled
{
    return (self.tabIndex < self.maxTabIndex - 1);
}



- (UITextField*) activeTextField
{
    _activeTextField = (UITextField*)activeViewElement;
    return _activeTextField;
}

- (void) setActiveTextField:(UITextField *)activeTextField
{
    _activeTextField = activeTextField;
    activeViewElement = (UIView*)_activeTextField;
}


- (PrevNextDoneAccessoryView*) keyboardAccesoryView {
    if (!_keyboardAccesoryView) {
        
        _keyboardAccesoryView = [[PrevNextDoneAccessoryView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 44)];
        
        _keyboardAccesoryView.delegate = self;
    }
    return _keyboardAccesoryView;
}

- (UIView*) textFieldsSuperView
{
    return self.view;
}

- (void) accessoryView:(PrevNextDoneAccessoryView *)view didClick:(UIBarButtonItem *)sender {
    NSLog(@"%@ Accesory Button Cliked", sender.title);

}

- (int) maxTabIndexForTextInputs
{
    // This should be overrided in child class.
    return 10;
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
	[UIView beginAnimations:@"BarAppear" context:nil];
	[UIView setAnimationDuration:[[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
	[UIView setAnimationCurve:[[[aNotification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] doubleValue]];
	[UIView setAnimationBeginsFromCurrentState:YES];
	
	[self.keyboardAccesoryView setFrame:CGRectMake(0, 156, self.view.frame.size.width, 44)];
	
    [UIView commitAnimations];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
	[UIView beginAnimations:@"BarAppear" context:nil];
	[UIView setAnimationDuration:[[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
	[UIView setAnimationCurve:[[[aNotification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] doubleValue]];
	[UIView setAnimationBeginsFromCurrentState:YES];
	
	[self.keyboardAccesoryView setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 44)];
	
	[UIView commitAnimations];
}


- (void) setupKeyboardListeners
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void) removeKeyboardListeners
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardWillShowNotification
												  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardWillHideNotification
												  object:nil];
}


#pragma - TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [super textFieldDidBeginEditing:textField];
	self.activeTextField = textField;
    self.tabIndex = textField.tag-INPUT_TAG_INDEX_OFFSET;
    [self scrollToViewElementIfNeeded];
    [self.keyboardAccesoryView updateButtons];
}

//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [self.userData setValue:textField.text forKey:[self.fields objectAtIndex:textField.tag]];
//    
//    NSLog(@"%@", textField.text);
//	return YES;
//}

- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.textFieldsSuperView viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        self.tabIndex = -1;
        //        _tabIndex = -1;
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupKeyboardListeners];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [self removeKeyboardListeners];
    self.keyboardAccesoryView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end

//
//  AutoFetchTableView.m
//  Cmug
//
//  Created by Dmitry C on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AutoFetchTableView.h"

@interface AutoFetchTableView()
@property (nonatomic, retain) UIView* spinnerView;
@property (nonatomic, readonly) UIActivityIndicatorView* spinner;
@end

@implementation AutoFetchTableView
@synthesize spinnerView = _spinnerView;
@synthesize autoFetchDelegate;
@synthesize autoFetchState;

#define SPINNER_TAG 12032

- (UIActivityIndicatorView *)spinner
{
    return (UIActivityIndicatorView*)[self.spinnerView viewWithTag:SPINNER_TAG];
}

- (UIView *)spinnerView
{
    if (!_spinnerView) {
        CGFloat width = self.frame.size.width;
        CGFloat height = 40.0f;
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        [spinner setBackgroundColor:[UIColor clearColor]];
        CGFloat spinnerWidth = spinner.frame.size.width;
        CGFloat spinnerHeight = spinner.frame.size.height;
        // Center spinner
        spinner.frame = CGRectMake(0.5f * (width - spinnerWidth), 
                                   0.5f * (height - spinnerHeight), 
                                   spinnerWidth, 
                                   spinnerHeight);
        spinner.tag = SPINNER_TAG;
        spinner.hidesWhenStopped = YES;
        UIView *spinnerWrapper = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [spinnerWrapper addSubview:spinner];
        _spinnerView = spinnerWrapper;
    }
    return _spinnerView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.autoFetchState = AutoFetchTableViewStateNormal;
    }
    return self;
}

- (void)startSpinner
{
    [self.spinner startAnimating];
}

- (void)stopSpinner
{
    [self.spinner stopAnimating];
}

- (void) setState:(AutoFetchTableViewState)state
{
    if (state != self.autoFetchState) {
        
        switch (state) {
            case AutoFetchTableViewStateNormal:
                if (self.autoFetchState == AutoFetchTableViewStateLoading) {
                    // reload table data to show the last footer view
                    NSIndexSet *lastSection = [[NSIndexSet alloc] initWithIndex:([self.dataSource numberOfSectionsInTableView:self] - 1)];
                    [self reloadSections:lastSection withRowAnimation:UITableViewRowAnimationNone];
                }
                [self stopSpinner];
                break;

            case AutoFetchTableViewStateLoading:
                if (self.autoFetchState == AutoFetchTableViewStateScrolledToBottom) {
                    [self startSpinner];
                }
                break;
            default:
                break;
        }
        self.autoFetchState = state;
    }
}

// must be called from TableView Delegate method
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    NSInteger sectionsNum = [[tableView dataSource] numberOfSectionsInTableView:tableView];
    if (section == sectionsNum - 1)
        return self.spinnerView;
    return nil;
}

- (void)startLoadingIfNeeded
{
    if (self.autoFetchState == AutoFetchTableViewStateLoading) 
        return;
    
    if ((self.autoFetchState == AutoFetchTableViewStateScrolledToBottom) &&
        [self.autoFetchDelegate autoFetchTableViewShouldStart:self])
    {
        [self setState:AutoFetchTableViewStateLoading];
        [self.autoFetchDelegate autoFetchTableViewDidStart:self];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.autoFetchState == AutoFetchTableViewStateLoading) 
        return;
    
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_margin = 40; // cell height
    if(y > h - reload_margin) {
        [self setState:AutoFetchTableViewStateScrolledToBottom];
        [self startLoadingIfNeeded];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self startLoadingIfNeeded];
}


- (void) autoFetchTriggerEndLoading:(id)responseData
{
    if (self.autoFetchState == AutoFetchTableViewStateLoading) {
//        NSInteger offset = [[responseData valueForKey:@"offset"] intValue];
//        NSInteger limit = [[responseData valueForKey:@"limit"] intValue];
        [self setState:AutoFetchTableViewStateNormal];
    }
}

@end

//
//  CRAuthorizedView.m
//


#import "AuthorizedView.h"
#import "IntroViewController.h"
#import "LoginViewController.h"
#import "AuthorizationManager.h"
#import "NavigationManager.h"

@implementation AuthorizedView

@synthesize requiresAuthorization;

#pragma mark -
#pragma mark view load / unload


- (void) checkAuth
{
    BOOL userRegistered = [AuthorizationManager sharedManager].registered;
    BOOL userAuthorized = [AuthorizationManager sharedManager].authorized;
    
	if(self.requiresAuthorization && !(userRegistered && userAuthorized))
	{
        // if user registered but not authorized
        
        [self.navigationController presentModalIntroViewControllerAndForwardToLogin:userRegistered 
                                                                 andForwardToSignup:!userRegistered];
        
        
	}
}

- (void) setupLogoutListener
{
    NSLog(@"Setup logout listener in %@", self);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkAuth) name:kUserLoggedOutNotification object:nil];
}

- (void) killLogoutListener
{
    NSLog(@"Kill logout listener in %@", self);
   [[NSNotificationCenter defaultCenter] removeObserver:self name:kUserLoggedOutNotification object:nil];   
}


- (void)viewDidLoad 
{
	[super viewDidLoad];
    if (self.requiresAuthorization) {
        [self setupLogoutListener];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self checkAuth];
}

- (void) dealloc
{
    [self killLogoutListener];    
}

#pragma mark -
#pragma mark MBProgressHUD

-(void) showHud:(NSString*)message
{
	MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.labelText = message;
}

-(void) hideHud
{
	[MBProgressHUD hideHUDForView:self.view animated:YES];
}


@end

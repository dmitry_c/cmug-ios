//
//  FlexibleViewController.h
//

#import <UIKit/UIKit.h>
#import "AuthorizedView.h"

@interface FlexibleViewController : AuthorizedView <UITextViewDelegate, UITextFieldDelegate>
{
	CGRect keyboardSize;
	UIView * activeViewElement;
	CGRect previousViewRect;
	UIBarButtonItem * previousButton;
    BOOL doneButtonPlaced;
	int topScrollingRestriction_;
	
	BOOL allowFreeScrolling;
}
//ui
@property(retain,nonatomic) IBOutlet UIScrollView * scrollView;
@property(retain,nonatomic) IBOutlet UIView * contentView;

@property(assign,nonatomic) BOOL shouldAddTextFieldDoneButton;
@property(assign,nonatomic) BOOL allowFreeScrolling;
@property(assign,nonatomic) BOOL allowEditingScrolling;
@property(nonatomic) int topScrollingRestriction;

//iboutlets
- (IBAction)editingDidBegin:(id)sender;
- (IBAction)editingDidEnd:(id)sender;
- (IBAction)resignSender:(id)sender;
- (void)insetViewTo:(UIEdgeInsets) contentInsets;
- (void)scrollToViewElementIfNeeded:(CGRect) keyboardSize;

- (void)scrollToViewElementIfNeeded;
- (void)resignActiveViewElement;
- (void)calculateScrollView;

@end

//
//  FlexibleViewController.m
//

#import "FlexibleViewController.h"
#import "AuthorizationManager.h"

#define kBackgroundImageName @"bg_textured_light_blue.png"


@interface FlexibleViewController(Private)

- (void)initHandler;

@end 



@implementation FlexibleViewController

@synthesize scrollView,contentView,shouldAddTextFieldDoneButton;
@synthesize allowEditingScrolling;
@synthesize topScrollingRestriction=topScrollingRestriction_;

- (BOOL)allowFreeScrolling
{
	return allowFreeScrolling;
}

- (void)setAllowFreeScrolling:(BOOL)_allowFreeScrolling
{
	allowFreeScrolling = _allowFreeScrolling;
	
	self.scrollView.scrollEnabled = allowFreeScrolling;
}

- (void)initHandler
{
    doneButtonPlaced = NO;
}

- (id)init
{
	self = [super init];
	if(self)
	{
		[self initHandler];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if(self)
	{
		[self initHandler];
	}
	return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if(self)
	{
		[self initHandler];
	}
	return self;
}

#pragma mark -
#pragma mark view load / unload

- (void)calculateScrollView
{
	//determine real view dimensions
	CGFloat maxY = 0;
	for (UIView * sub in self.contentView.subviews) 
	{
		CGFloat subY = sub.frame.origin.y + sub.frame.size.height;
		if(subY > maxY)
			maxY = subY;
	}
	
    CGRect viewframe = self.view.frame;
	viewframe.origin.y = 0;
	viewframe.size.height = maxY + 10;
	self.scrollView.contentSize = viewframe.size;
}

- (void)viewDidLoad 
{
	[super viewDidLoad];

    // set the background
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:kBackgroundImageName]];
    
	activeViewElement = nil;
	
	//place current view inside UIScrollView
	self.contentView = self.view;
	CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;

    if ([self respondsToSelector:@selector(tableView)]) {
        self.scrollView = ((UITableView*)self.view);
    } else {
        self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, width, 600)]; // was 320x367
        
        self.scrollView.contentSize = self.view.frame.size;
        self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        
        [self.view addSubview:self.scrollView];
        
        
        self.scrollView.autoresizingMask = self.contentView.autoresizingMask;
        [self.scrollView addSubview:self.contentView];
        
        //	[self calculateScrollView];
        self.scrollView.backgroundColor = self.contentView.backgroundColor;
    }
	
	//init variables
	self.shouldAddTextFieldDoneButton = NO;
	self.allowFreeScrolling = NO;
	self.allowEditingScrolling = YES;
}

- (void)viewDidUnload 
{
	[super viewDidUnload];
  
}

#pragma mark -
#pragma mark view appear / disappear
- (void)viewWillAppear:(BOOL)animated 
{
	[super viewWillAppear:animated];
	//keyboard notifications
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
	[nc addObserver:self selector:@selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
	
	self.scrollView.scrollEnabled = self.allowFreeScrolling;
}

- (void)viewWillDisappear:(BOOL)animated 
{
	[super viewWillDisappear:animated];
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[nc removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark -
#pragma mark keyboard notifications

- (void)setDoneButton:(BOOL)visible
{
    if (visible) {
        if(self.shouldAddTextFieldDoneButton && self.navigationItem && !doneButtonPlaced)
        {
            //store current button
            if (self.navigationItem.rightBarButtonItem!=nil)	
                previousButton = self.navigationItem.rightBarButtonItem;
            
            UIBarButtonItem * doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(resignActiveViewElement)];
            [self.navigationItem setRightBarButtonItem:doneButton animated:YES];
            doneButtonPlaced = YES;
            
        }
        
    } else {
        if(self.shouldAddTextFieldDoneButton && self.navigationItem && doneButtonPlaced)
        {
            if(previousButton!=nil)
            {
                [self.navigationItem setRightBarButtonItem:previousButton animated:YES];
                previousButton = nil;
            }
            else 
            {
                [self.navigationItem setRightBarButtonItem:nil animated:YES];
            }
            
            doneButtonPlaced = NO;
        }
    }
}

- (void)keyboardWillShow:(NSNotification *)note
{	
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardSize];
	
	UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.size.height, 0.0);
    if ([self respondsToSelector:@selector(keyboardAccesoryView)]) {
        contentInsets.bottom += ((UIView*) [self performSelector:@selector(keyboardAccesoryView)]).frame.size.height;;
    }
	
    self.scrollView.scrollEnabled = self.allowEditingScrolling;
	[self insetViewTo:contentInsets];
	[self scrollToViewElementIfNeeded:keyboardSize];
    [self setDoneButton:YES];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    [self insetViewTo:UIEdgeInsetsZero];
	[self.scrollView setContentOffset:CGPointZero animated:YES];
	
	self.scrollView.scrollEnabled = self.allowFreeScrolling;
    [self setDoneButton:NO];
}


-(void) insetViewTo:(UIEdgeInsets) contentInsets
{
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)killScroll
{
    [self.scrollView setContentOffset:self.scrollView.contentOffset animated:NO];
}

- (void)scrollToViewElementIfNeeded
{
	[self scrollToViewElementIfNeeded:keyboardSize];
}

- (void)scrollToViewElementIfNeeded:(CGRect)kbdSize
{
	if(activeViewElement==nil) return;
	
	CGPoint elementOrigin = activeViewElement.frame.origin;
	UIView * tmp = [activeViewElement superview];
	while (tmp && tmp != self.view)
	{
		elementOrigin.x += tmp.frame.origin.x;
		elementOrigin.y += tmp.frame.origin.y;
		tmp = [tmp superview];
	}
    
	// If active text field is hidden by keyboard, scroll it so it's visible
	CGRect viewRect  = self.view.frame;
	viewRect.size.height -= kbdSize.size.height;
    viewRect.origin.y = self.scrollView.contentOffset.y;

    if ([self respondsToSelector:@selector(keyboardAccesoryView)]) {
        viewRect.size.height -= ((UIView*) [self performSelector:@selector(keyboardAccesoryView)]).frame.size.height;
    }
    
	if (!CGRectContainsPoint(viewRect, elementOrigin)) 
	{
        CGPoint scrollPoint = CGPointMake(0.0, elementOrigin.y - keyboardSize.size.height);
		if(self.tabBarItem)
			scrollPoint.y += 49;		// tab bar size
		
        if(self.navigationItem)
			scrollPoint.y += 44;		//nav bar size
        
        if ([self respondsToSelector:@selector(keyboardAccesoryView)]) 
            scrollPoint.y += ((UIView*) [self performSelector:@selector(keyboardAccesoryView)]).frame.size.height;
        
		scrollPoint.y -= topScrollingRestriction_;
		scrollPoint.y += [activeViewElement frame].size.height * 0.5;
		
		[self killScroll];
        if (scrollPoint.y < 0) scrollPoint.y = 0;
        
        [self.scrollView setContentOffset:scrollPoint animated:YES];            
    }
}

#pragma mark -
#pragma mark ibactions

- (IBAction)editingDidBegin:(id)sender
{
	activeViewElement = sender;
}

- (IBAction)editingDidEnd:(id)sender
{
	activeViewElement = nil;
}

- (IBAction)resignSender:(id)sender
{
	[sender resignFirstResponder];
}

- (void)resignActiveViewElement
{
	[activeViewElement resignFirstResponder];
}


#pragma mark -
#pragma mark UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
	activeViewElement = textView;
	return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if (textView == activeViewElement)
      	activeViewElement = nil;

    return YES;
}

#pragma mark -
#pragma mark UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	activeViewElement = textField;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField == activeViewElement)
        activeViewElement = nil;

	return YES;
}

@end

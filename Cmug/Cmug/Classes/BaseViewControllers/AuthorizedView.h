//
//  AuthorizedView.h
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface AuthorizedView : UIViewController 

//authorization
@property(assign,nonatomic) BOOL requiresAuthorization;

-(void) showHud:(NSString*)message;
-(void) hideHud;

@end

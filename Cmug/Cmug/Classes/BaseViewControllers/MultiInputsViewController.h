//
//  MultiInputsViewController.h
//  Cmug
//
//  Created by Dmitry C on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FlexibleViewController.h"
#import "PrevNextDoneAccessoryView.h"


@interface MultiInputsViewController : FlexibleViewController <PrevNextDoneAccessoryViewDelegate>

@property (nonatomic, retain) PrevNextDoneAccessoryView* keyboardAccesoryView;

@property (nonatomic, assign) int tabIndex;
@property (nonatomic, readonly) int maxTabIndex;

@property (nonatomic, retain) UITextField* activeTextField;

@end

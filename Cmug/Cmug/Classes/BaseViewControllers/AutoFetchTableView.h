//
//  AutoFetchTableView.h
//  Cmug
//
//  Created by Dmitry C on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    AutoFetchTableViewStateNormal, 
    AutoFetchTableViewStateScrolledToBottom,
    AutoFetchTableViewStateLoading,
} AutoFetchTableViewState;

@class AutoFetchTableView;

@protocol AutoFetchTableViewDelegate <NSObject>
- (BOOL)autoFetchTableViewShouldStart:(AutoFetchTableView*)tableView;
- (void)autoFetchTableViewDidStart:(AutoFetchTableView*)tableView;
@end

@interface AutoFetchTableView : UITableView <UIScrollViewDelegate, UITableViewDelegate>
@property (nonatomic, assign) AutoFetchTableViewState autoFetchState;
@property (nonatomic, retain) id<AutoFetchTableViewDelegate> autoFetchDelegate;

- (void) autoFetchTriggerEndLoading:(id)responseData;
- (void) setState:(AutoFetchTableViewState)state;
@end
